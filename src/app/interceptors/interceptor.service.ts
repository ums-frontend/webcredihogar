import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from 'environments/environment';

const appSecret = `${environment.API_KEY}`;

@Injectable({ providedIn: 'root' })
export class InterceptorService implements HttpInterceptor{
    constructor() { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        request = request.clone({
          setHeaders: {
            // eslint-disable-next-line @typescript-eslint/naming-convention
            'Content-Type' : 'application/json',
            // eslint-disable-next-line @typescript-eslint/naming-convention
            'Accept': 'application/json',
            // eslint-disable-next-line @typescript-eslint/naming-convention
            'x-api-key': `${appSecret}`
          }
        });
        return next.handle( request ).pipe(catchError(error => this.errorHandler(error)));
    }

    errorHandler( error: HttpErrorResponse ): Observable<never> {
        if(error instanceof HttpErrorResponse){
            if(error.error instanceof ErrorEvent){
                console.log('Error de cliente');
            } else {
                if(error. status === 401){
                    console.log('Usted no cuenata con permisos para ingresar');
                }else {
                    console.log('Error de servidor');
                }
            }
        } else {
            console.log('Error desconocido');
        }
        return throwError(error);
    }

}
