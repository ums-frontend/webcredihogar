/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'home',
        title: 'Home',
        type: 'basic',
        icon: 'heroicons_outline:home',
        link: '/home',
        roles: [1, 2, 3]
    },
    {
        id: 'admin',
        title: 'Admin',
        type: 'basic',
        icon: 'heroicons_outline:key',
        link: '/administrators',
        roles: [1]
    },
    {
        id: 'dealership',
        title: 'Dealership',
        type: 'basic',
        icon: 'mat_outline:directions_car',
        link: '/dealership',
        roles: [1]
    },
    {
        id: 'dealer',
        title: 'Sales Rep',//dealer
        type: 'basic',
        icon: 'heroicons_outline:user',
        link: '/dealer',
        roles: [1, 2]
    },
    {
        id: 'costumers',
        title: 'Customers',
        type: 'basic',
        icon: 'heroicons_outline:identification',
        link: '/clients',
        roles: [1, 2, 3]
    },
    {
        id: 'catalogs',
        title: 'Catalogues',
        type: 'group',
        icon: 'heroicons_outline:home',
        children: [
            {
                id: 'catalogs.docs',
                title: 'Documents',
                type: 'basic',
                icon: 'heroicons_outline:archive',
                link: '/catalogs/documents',
                roles: [1]
            },
            {
                id: 'catalogs.config',
                title: 'Settings',
                type: 'basic',
                icon: 'heroicons_outline:cog',
                link: '/catalogs/settings',
                roles: [1]
            }
        ],
        roles: [1]
    },
    {
        id: 'credit',
        title: 'Credit application',
        type: 'group',
        icon: 'heroicons_outline:home',
        children: [
            {
                id: 'credit.admin',
                title: 'Request management',
                type: 'basic',
                icon: 'heroicons_outline:clipboard-check',
                link: '/credits/requests',
                roles: [1, 2, 3]
            },
            {
                id: 'credit.new',
                title: 'Loan',
                type: 'basic',
                icon: 'heroicons_outline:clipboard',
                link: '/credits/credit',
                roles: [3]
            }
        ],
        roles: [1, 2, 3]
    }

];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
