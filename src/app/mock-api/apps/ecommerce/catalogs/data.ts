/* eslint-disable */
export const documents = [
    {
        id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
        name: 'Comprobante de ingresos',
        minimum: '3',
        mandatory: true,
        active: true
    },
    {
        id: '00b0292f-3d50-4669-a0c4-7a9d85efc98d',
        name: 'Comprobante de domicilio',
        minimum: '1',
        mandatory: true,
        active: true
    }
];

export const settings = [
    {
        id: '00b0292f-3d50-4669-a0c4-7a9d85efc98d',
        name: 'Regional WEST',
        blocked: [{brand:"Ford", models:["Mustang","Raptor"]}],
        minimumDownPaymentPercentage: 15,
        minimumCashDownPayment: 5000,
        maximumLTVNADAPercentage: 15,
        maximumMonthlyPayment: 6000,
        minimumTermLoan: 12,
        maximumTermLoan: 60,
        maximumLoanValue: 5000,
        maximumAgeVehicle: 10,
        maximumMileageVehicle: 100000,
        maximumDaysFirstMonthlyPayment: 30,
        maximumDaysFirstBiWeeklyPayment: 15,
        maximumDaysFirstWeeklyPayment: 30,
        minimumReferencesCredit: 3,
        minimumEmploymentReferences: 4,
        active: true
    },
    {
        id: '0045b02f-3d50-4669-a0c4-7a9d85efc98d',
        name: 'Regional EAST',
        blocked: [{brand:"GMC", models:["Corvette"]}],
        minimumDownPaymentPercentage: 15,
        minimumCashDownPayment: 9000,
        maximumLTVNADAPercentage: 15,
        maximumMonthlyPayment: 10000,
        minimumTermLoan: 12,
        maximumTermLoan: 60,
        maximumLoanValue: 3000,
        maximumAgeVehicle: 12,
        maximumMileageVehicle: 120000,
        maximumDaysFirstMonthlyPayment: 60,
        maximumDaysFirstBiWeeklyPayment: 60,
        maximumDaysFirstWeeklyPayment: 60,
        minimumReferencesCredit: 3,
        minimumEmploymentReferences: 4,
        active: true
    }    
];