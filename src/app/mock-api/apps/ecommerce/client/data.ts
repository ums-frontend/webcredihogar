/* eslint-disable */
export const clients = [
    {
        id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
        account: 'CL-000001',
        email: 'isc.josue.nava@gmail.com',
        name: 'Bernabé Nava Cruz',
        address: 'Herlinda Garcia 38, centro Pedro Escobedo Querétaro',
        birthday: "1990-06-11",
        cellphone: '4481023355',
        homephone: '4482750034',
        ssn: '142334123',
        thumbnail  : 'assets/images/apps/ecommerce/products/watch-01-thumb.jpg',
        images     : [
            'assets/images/apps/ecommerce/products/watch-01-01.jpg',
            'assets/images/apps/ecommerce/products/watch-01-02.jpg',
            'assets/images/apps/ecommerce/products/watch-01-03.jpg'
        ],
        active     : true
    },
    {
        id: '00b0292f-3d50-4669-a0c4-7a9d85efc98d',
        account: 'CL-000002',
        email: 'josue.nava@itzama.com.mx',
        name: 'Josué Nava Cruz',
        address: 'Herlinda Garcia 38, centro Pedro Escobedo Querétaro',
        birthday: "1990-06-11",
        cellphone: '4481023355',
        homephone: '4482750034',
        ssn: '142312123',
        thumbnail: 'assets/images/apps/ecommerce/products/watch-01-thumb.jpg',
        images: [
            'assets/images/apps/ecommerce/products/watch-01-01.jpg',
            'assets/images/apps/ecommerce/products/watch-01-02.jpg',
            'assets/images/apps/ecommerce/products/watch-01-03.jpg'
        ],
        active: true
    }
];
