/* eslint-disable */
export const credits = [
    {
        id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
        requestId: 'RQ-000453',
        client: 'CL-000453',
        name: 'Pedro Dominguez',
        amount: 15000.00,
        date: '02/03/2020',
        vin: "1372231231245",
        brand: "Nissan",
        model: "Sentra",
        year: "2017",
        type: "Sedan",
        aval: "John Doe",
        active: true
    },
    {
        id: '7eb72w59-1347-4317-96b6-9yyh897eba3c',
        requestId: 'RQ-000454',
        client: 'CL-000454',
        name: 'Alex McGreegor',
        amount: 12000.00,
        date: '02/03/2020',
        vin: "1372231231245",
        brand: "Nissan",
        model: "Versa",
        year: "2021",
        type: "Sedan",
        aval: "Alicia Villa",
        active: true
    }
];
