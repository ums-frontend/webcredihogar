import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { take } from 'rxjs';
import { AvailableLangs, TranslocoService } from '@ngneat/transloco';
import { FuseNavigationService, FuseVerticalNavigationComponent } from '@fuse/components/navigation';

@Component({
    selector       : 'languages',
    templateUrl    : './languages.component.html',
    encapsulation  : ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    exportAs       : 'languages'
})
export class LanguagesComponent implements OnInit, OnDestroy
{
    availableLangs: AvailableLangs;
    activeLang: string;
    flagCodes: any;

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseNavigationService: FuseNavigationService,
        private _translocoService: TranslocoService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Get the available languages from transloco
        this.availableLangs = this._translocoService.getAvailableLangs();

        // Subscribe to language changes
        this._translocoService.langChanges$.subscribe((activeLang) => {

            // Get the active lang
            this.activeLang = activeLang;

            // Update the navigation
            this._updateNavigation(activeLang);
        });

        // Set the country iso codes for languages for flags
        this.flagCodes = {
            'en': 'us',
            'es': 'es'
        };
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Set the active lang
     *
     * @param lang
     */
    setActiveLang(lang: string): void
    {
        // Set the active lang
        this._translocoService.setActiveLang(lang);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.id || index;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Update the navigation
     *
     * @param lang
     * @private
     */
    private _updateNavigation(lang: string): void
    {
        // For the demonstration purposes, we will only update the Dashboard names
        // from the navigation but you can do a full swap and change the entire
        // navigation data.
        //
        // You can import the data from a file or request it from your backend,
        // it's up to you.

        // Get the component -> navigation data -> item
        const navComponent = this._fuseNavigationService.getComponent<FuseVerticalNavigationComponent>('mainNavigation');

        // Return if the navigation component does not exist
        if ( !navComponent )
        {
            return null;
        }

        // Get the flat navigation data
        const navigation = navComponent.navigation;

        // Get the menu item and update its title

        //Generals options home
        const mainItem = this._fuseNavigationService.getItem('home', navigation);
        if ( mainItem )
        {
            this._translocoService.selectTranslate('menu.home').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }

        const mainAdminItem = this._fuseNavigationService.getItem('admin', navigation);
        if ( mainAdminItem )
        {
            this._translocoService.selectTranslate('menu.administrator').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainAdminItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }

        const mainDealershipItem = this._fuseNavigationService.getItem('dealership', navigation);
        if ( mainDealershipItem )
        {
            this._translocoService.selectTranslate('menu.dealership').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainDealershipItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }

        const mainDealerItem = this._fuseNavigationService.getItem('dealer', navigation);
        if ( mainDealerItem )
        {
            this._translocoService.selectTranslate('menu.dealer').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainDealerItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }

        const mainCostumersItem = this._fuseNavigationService.getItem('costumers', navigation);
        if ( mainCostumersItem )
        {
            this._translocoService.selectTranslate('menu.costumers').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainCostumersItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }
        //Catalogs options
        const mainCatalogsItem = this._fuseNavigationService.getItem('catalogs', navigation);
        if ( mainCatalogsItem )
        {
            this._translocoService.selectTranslate('menu.catalogs.title').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainCatalogsItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }

        const mainCatalogsTypeVehicleItem = this._fuseNavigationService.getItem('catalogs.typeVehicle', navigation);
        if ( mainCatalogsTypeVehicleItem )
        {
            this._translocoService.selectTranslate('menu.catalogs.typeVehicle').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainCatalogsTypeVehicleItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }

        const mainCatalogsDocsItem = this._fuseNavigationService.getItem('catalogs.docs', navigation);
        if ( mainCatalogsDocsItem )
        {
            this._translocoService.selectTranslate('menu.catalogs.docs').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainCatalogsDocsItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }

        const mainCatalogsConfigItem = this._fuseNavigationService.getItem('catalogs.config', navigation);
        if ( mainCatalogsConfigItem )
        {
            this._translocoService.selectTranslate('menu.catalogs.config').pipe(take(1))
                .subscribe((translation) => {
                    // Set the title
                    mainCatalogsConfigItem.title = translation;
                    // Refresh the navigation component
                    navComponent.refresh();
                });
        }

         //Catalogs credit
         const mainCreditItem = this._fuseNavigationService.getItem('credit', navigation);
         if ( mainCreditItem )
         {
             this._translocoService.selectTranslate('menu.credit.title').pipe(take(1))
                 .subscribe((translation) => {
                     // Set the title
                     mainCreditItem.title = translation;
                     // Refresh the navigation component
                     navComponent.refresh();
                 });
         }

         const mainCreditAdminItem = this._fuseNavigationService.getItem('credit.admin', navigation);
         if ( mainCreditAdminItem )
         {
             this._translocoService.selectTranslate('menu.credit.admin').pipe(take(1))
                 .subscribe((translation) => {
                     // Set the title
                     mainCreditAdminItem.title = translation;
                     // Refresh the navigation component
                     navComponent.refresh();
                 });
         }

         const mainNewCreditItem = this._fuseNavigationService.getItem('credit.new', navigation);
         if ( mainNewCreditItem )
         {
             this._translocoService.selectTranslate('menu.credit.new').pipe(take(1))
                 .subscribe((translation) => {
                     // Set the title
                     mainNewCreditItem.title = translation;
                     // Refresh the navigation component
                     navComponent.refresh();
                 });
         }
    }
}
