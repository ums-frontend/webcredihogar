import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Document, RequestDocumentServices, ResponseDocumentServices } from 'app/entities/Document.types';
import { TranslocoService } from '@ngneat/transloco';
import { environment } from 'environments/environment';
import { ResponseCatalogServices, RequestCatalogServices } from 'app/entities/Catalog.types';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

const END_POINT = `${environment.SERVICES_URL}`;

@Injectable({
    providedIn: 'root'
})
export class CatalogService {
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient, private _translocoService: TranslocoService, private userService: UserService) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    getCatalog(typeId): Observable<ResponseCatalogServices> {
        const request: RequestCatalogServices = {
            type: typeId,
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseCatalogServices>(`${END_POINT}/catalog`, body, { 'headers': headers });
    }

    getModels(brandId): Observable<ResponseCatalogServices> {
        const request: RequestCatalogServices = {
            type: 4,
            id: brandId,
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseCatalogServices>(`${END_POINT}/catalog`, body, { 'headers': headers });
    }

    create(item: Document): Observable<ResponseDocumentServices> {
        const request: RequestDocumentServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued(),
            documentInfo: {
                name: item.name,
                min: item.min,
                mandatory: item.mandatory
            }
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseDocumentServices>(`${END_POINT}/newDocument`, body, { 'headers': headers });
    }

    update(item: Document): Observable<ResponseDocumentServices> {
        const request: RequestDocumentServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued(),
            documentInfo: {
                documentId: item.documentId,
                name: item.name,
                min: item.min,
                mandatory: item.mandatory
            }
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseDocumentServices>(`${END_POINT}/updateDocument`, body, { 'headers': headers });
    }

    delete(item: Document): Observable<ResponseDocumentServices> {
        const request: RequestDocumentServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued(),
            documentInfo: {
                documentId: item.documentId
            }
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseDocumentServices>(`${END_POINT}/deleteDocument`, body, { 'headers': headers });
    }

    /**
     * Function by get User logued
     */
    getUserLogued(): number {
        const userLogued: User = this.userService.userLogued;
        if(userLogued && userLogued.userId){
            return userLogued.userId;
        }else{
            return -1;
        }
    }
}
