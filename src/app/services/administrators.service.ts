import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { environment } from 'environments/environment';
import { AdministratorEntity } from 'app/entities/administrator.entity';

import { RequestGetUserListEntity, RequestNewUserEntity, RequestUpdateStatusUserEntity, RequestUpdateUserEntity } from 'app/entities/requestUser.entity';
import { ResponseGetUserListEntity, ResponseGeneralUserEntity } from 'app/entities/responseUser.entity';
import { TranslocoService } from '@ngneat/transloco';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

const END_POINT = `${environment.SERVICES_URL}`;

@Injectable({
  providedIn: 'root'
})

export class AdministratorsService {

    constructor(
        private http: HttpClient,
        private _translocoService: TranslocoService,
        private userService: UserService
        ) {}

    // eslint-disable-next-line @typescript-eslint/member-ordering
    rolAdmin: number = 1;

    getAdministrators(): Observable<ResponseGetUserListEntity> {
        const requestGetUser: RequestGetUserListEntity = {
            userId: this.getUserLogued(), //necessary for an administrator
            roleId: this.rolAdmin,
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json'};
        const body=JSON.stringify(requestGetUser);
        return this.http.post<ResponseGetUserListEntity>(`${END_POINT}/getUserList`, body,{'headers':headers});
    }

    createAdministrator(admin: AdministratorEntity): Observable<ResponseGeneralUserEntity> {
        const newAdmin: RequestNewUserEntity = {
            'name': admin.name,
            'email': admin.email,
            'address': '', //not necessary for an administrator
            'configId': 0, //not necessary for an administrator
            'roleId': this.rolAdmin,
            'createUserId' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase()
        };

        const body = JSON.stringify(newAdmin);
        console.log(body);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/newUser`, body);
    }

    updateAdministrator(admin: AdministratorEntity): Observable<ResponseGeneralUserEntity> {
        const updateAdmin: RequestUpdateUserEntity = {
            'userId': admin.userId,
            'name': admin.name,
            'address': '',//not necessary for an administrator
            'roleId': this.rolAdmin,
            'configId' : 0,//not necessary for an administrator
            'userIdUpdate' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase()
        };
        const body = JSON.stringify(updateAdmin);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/updateUser`, body);
    }


    updateStatusAdministrator(admin: AdministratorEntity): Observable<ResponseGeneralUserEntity> {
        const updateAdmin: RequestUpdateStatusUserEntity = {
            'userId': admin.userId,
            'status': admin.status,
            'userIdUpdate' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase()
        };
        const body = JSON.stringify(updateAdmin);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/updateUserStatus`, body);
    }

    /**
     * Function by get User logued
     */
    getUserLogued(): number {
        const userLogued: User = this.userService.userLogued;
        if(userLogued && userLogued.userId){
            return userLogued.userId;
        }else{
            return -1;
        }
    }

}
