import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'environments/environment';
import { DealershipEntity } from 'app/entities/dealership.entity';
import { SettingsEntity } from 'app/entities/settings.entity';
import { RequestGetUserListEntity, RequestNewUserEntity, RequestUpdateStatusUserEntity, RequestUpdateUserEntity } from 'app/entities/requestUser.entity';
import { ResponseGetUserListEntity, ResponseGeneralUserEntity, ResponseSettingsEntity } from 'app/entities/responseUser.entity';
import { TranslocoService } from '@ngneat/transloco';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

const END_POINT = `${environment.SERVICES_URL}`;

@Injectable({
  providedIn: 'root'
})
export class DealershipService {

    constructor(private http: HttpClient,
        private _translocoService: TranslocoService,
        private userService: UserService
        ) {}

    // eslint-disable-next-line @typescript-eslint/member-ordering
    rolDealership: number = 2;

    getDealership(): Observable<ResponseGetUserListEntity> {
        const requestGetUser: RequestGetUserListEntity = {
            userId: this.getUserLogued(),
            roleId: this.rolDealership,
            language: this._translocoService.getActiveLang().toUpperCase().toUpperCase()
        };
        const body=JSON.stringify(requestGetUser);
        return this.http.post<ResponseGetUserListEntity>(`${END_POINT}/getUserList`, body);
    }

    getSettings(): Observable<ResponseSettingsEntity> {
        const requestGetSettings: any = {
            type: 12,//Load settings
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const body=JSON.stringify(requestGetSettings);
        return this.http.post<ResponseSettingsEntity>(`${END_POINT}/catalog`, body);
    }

    createDealership(dealership: DealershipEntity): Observable<ResponseGeneralUserEntity> {
        const newUser: RequestNewUserEntity = {
            'name': dealership.name,
            'email': dealership.email,
            'address': dealership.address,
            'configId': dealership.configId,
            'roleId': this.rolDealership,
            'createUserId' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase(),
            'acronym': dealership.acronym,
            'cellphone':dealership.cellphone,
            'notes':dealership.notes
        };

        const body = JSON.stringify(newUser);
        console.log(body);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/newUser`, body);
    }

    updateDealership(dealership: DealershipEntity): Observable<ResponseGeneralUserEntity> {
        const updateUser: RequestUpdateUserEntity = {
            'userId': dealership.userId,
            'name': dealership.name,
            'address': dealership.address,
            'roleId': this.rolDealership,
            'configId' : dealership.configId,
            'userIdUpdate' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase(),
            'acronym': dealership.acronym,
            'cellphone':dealership.cellphone,
            'notes':dealership.notes
        };
        const body = JSON.stringify(updateUser);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/updateUser`, body);
    }

    updateStatusDealership(dealership: DealershipEntity): Observable<ResponseGeneralUserEntity> {
        const updateUser: RequestUpdateStatusUserEntity = {
            'userId': dealership.userId,
            'status': dealership.status,
            'userIdUpdate' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase()
        };
        const body = JSON.stringify(updateUser);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/updateUserStatus`, body);
    }

    /**
     * Function by get User logued
     */
    getUserLogued(): number {
        const userLogued: User = this.userService.userLogued;
        if(userLogued && userLogued.userId){
            return userLogued.userId;
        }else{
            return -1;
        }
    }
}
