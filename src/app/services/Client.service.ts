import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client, RequestClientServices, ResponseClientGeneralServices, ResponseClientServices } from 'app/entities/Client.types';
import { TranslocoService } from '@ngneat/transloco';
import { environment } from 'environments/environment';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

const END_POINT = `${environment.SERVICES_URL}`;

@Injectable({
    providedIn: 'root'
})
export class ClientService{

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient, private _translocoService: TranslocoService, private userService: UserService){
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    getObjects(): Observable<ResponseClientServices> {
        const request: RequestClientServices = {
            userId: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json'};
        const body=JSON.stringify(request);
        return this._httpClient.post<ResponseClientServices>(`${END_POINT}/getClientList`, body,{'headers':headers});
    }

    create(client: Client): Observable<ResponseClientServices> {
        const request: RequestClientServices = {
            createUserId: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase(),
            firstName: client.name,
            middleName: client.middleName,
            lastName: client.lastName,
            birthdate: client.dateBirth,
            cellphone: client.cellphone,
            homephone: client.homePhone,
            email: client.email,
            ssn: client.ssnItin
        };
        const headers = { 'content-type': 'application/json'};
        const body=JSON.stringify(request);
        console.log(`${END_POINT}/newClient`);
        console.log('==== body ====');
        console.log(body);
        console.log('==== body ====');
        return this._httpClient.post<ResponseClientServices>(`${END_POINT}/newClient`, body,{'headers':headers});
    }

    update(client: Client): Observable<ResponseClientServices> {
        const request: RequestClientServices = {
            userIdUpdate: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase(),
            clientId: client.id,
            firstName: client.name,
            middleName: client.middleName,
            lastName: client.lastName,
            birthdate: client.dateBirth,
            cellphone: client.cellphone,
            homephone: client.homePhone,
            email: client.email,
            ssn: client.ssnItin
        };
        const headers = { 'content-type': 'application/json'};
        const body=JSON.stringify(request);
        console.log(`${END_POINT}/updateClient`);
        console.log('==== body ====');
        console.log(body);
        console.log('==== body ====');
        return this._httpClient.post<ResponseClientServices>(`${END_POINT}/updateClient`, body,{'headers':headers});
    }

    /**
     * Delete the client
     *
     * @param client
     */
    delete(client: Client): Observable<ResponseClientServices>{
        const request: RequestClientServices = {
            userIdUpdate: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase(),
            clientId: client.id,
            status: client.status
        };
        const headers = { 'content-type': 'application/json'};
        const body=JSON.stringify(request);
        return this._httpClient.post<ResponseClientServices>(`${END_POINT}/updateClientStatus`, body,{'headers':headers});
    }
    /**
     * service by associate user by client
     *
     * @param client
     */
     associateUser(client: Client): Observable<ResponseClientGeneralServices>{
        const request: any = {
            clientId: client.id,
            createUserId: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json'};
        const body=JSON.stringify(request);
        return this._httpClient.post<ResponseClientGeneralServices>(`${END_POINT}/clientOriginator`, body,{'headers':headers});
    }

    search(data: any): Observable<ResponseClientServices>{
        const request: RequestClientServices = {
            userId: this.getUserLogued(),
            searchType: data.searchType,
            searchParam: data.searchParam,
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json'};
        const body=JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseClientServices>(`${END_POINT}/searchClient`, body,{'headers':headers});
    }

    /**
     * Function by get User logued
     */
    getUserLogued(): number {
        const userLogued: User = this.userService.userLogued;
        if(userLogued && userLogued.userId){
            return userLogued.userId;
        }else{
            return -1;
        }
    }
}
