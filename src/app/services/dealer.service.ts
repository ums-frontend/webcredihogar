import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'environments/environment';

import { DealerEntity } from 'app/entities/dealer.entity';
import { RequestGetUserListEntity, RequestNewUserEntity, RequestUpdateStatusUserEntity, RequestUpdateUserEntity } from 'app/entities/requestUser.entity';
import { ResponseGetUserListEntity, ResponseGeneralUserEntity } from 'app/entities/responseUser.entity';
import { TranslocoService } from '@ngneat/transloco';

import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

const END_POINT = `${environment.SERVICES_URL}`;

@Injectable({
  providedIn: 'root'
})
export class DealerService {

  constructor(
      private http: HttpClient,
      private _translocoService: TranslocoService,
      private userService: UserService
      ) { }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    rolDealer: number = 3;

    getDealers(): Observable<ResponseGetUserListEntity> {
        const requestGetUser: RequestGetUserListEntity = {
            userId: this.getUserLogued(),
            roleId: this.rolDealer,
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json'};
        const body=JSON.stringify(requestGetUser);
        return this.http.post<ResponseGetUserListEntity>(`${END_POINT}/getUserList`, body,{'headers':headers});
    }

    createDealer(dealer: DealerEntity): Observable<ResponseGeneralUserEntity> {
        const newUser: RequestNewUserEntity = {
            'name': dealer.name,
            'email': dealer.email,
            'address': '', //not necessary for an administrator
            'configId': 0, //not necessary for an administrator
            'roleId': this.rolDealer,
            'createUserId' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase(),
            'cellphone': dealer.cellphone
        };

        const body = JSON.stringify(newUser);
        console.log(body);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/newUser`, body);
    }

    updateDealer(dealer: DealerEntity): Observable<ResponseGeneralUserEntity> {
        const updateUser: RequestUpdateUserEntity = {
            'userId': dealer.userId,
            'name': dealer.name,
            'address': '',//not necessary for an administrator
            'roleId': this.rolDealer,
            'configId' : 0,//not necessary for an administrator
            'userIdUpdate' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase(),
            'cellphone': dealer.cellphone
        };
        const body = JSON.stringify(updateUser);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/updateUser`, body);
    }

    updateStatusDealer(dealer: DealerEntity): Observable<ResponseGeneralUserEntity> {
        const updateUser: RequestUpdateStatusUserEntity = {
            'userId': dealer.userId,
            'status': dealer.status,
            'userIdUpdate' : this.getUserLogued(),
            'language': this._translocoService.getActiveLang().toUpperCase()
        };
        const body = JSON.stringify(updateUser);
        return this.http.post<ResponseGeneralUserEntity>(`${END_POINT}/updateUserStatus`, body);
    }

    /**
     * Function by get User logued
     */
    getUserLogued(): number {
        const userLogued: User = this.userService.userLogued;
        if(userLogued && userLogued.userId){
            return userLogued.userId;
        }else{
            return -1;
        }
    }
}
