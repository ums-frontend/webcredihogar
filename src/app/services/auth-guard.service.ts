import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  // declare a variable 'routeURL'
  // to keep track of current active route
  routeURL: string;
  user: User;

  constructor(private _userService: UserService, private router: Router) {
    // initialize 'routeURL' with current route URL
    this.routeURL = this.router.url;
  }

  // the Router call canActivate() method,
  // if canActivate is registered in Routes[]
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise((resolve, reject) => {
        this.user = this._userService.userLogued;
        if(this.user.roleId === 1){
            if(state.url === '/administrators'
                || state.url === '/dealership'
                || state.url === '/dealer'
                || state.url === '/catalogs/documents'
                || state.url === '/catalogs/settings'){
                return resolve(true);
            }else{
                this.router.navigate(['/home']);
                return resolve(false);
            }
        }

        if(this.user.roleId === 2){
            if(state.url === '/administrators'
                || state.url === '/dealership'
                || state.url === '/catalogs/documents'
                || state.url === '/catalogs/settings'){
                this.router.navigate(['/home']);
                return resolve(false);
            }
        }

        if(this.user.roleId === 3){
            if(state.url === '/administrators'
                || state.url === '/dealership'
                || (state.url === '/dealer' && !this.user.managment)
                || state.url === '/catalogs/documents'
                || state.url === '/catalogs/settings'){
                this.router.navigate(['/home']);
                return resolve(false);
            }
        }

        return resolve(true);
    });
  }
}
