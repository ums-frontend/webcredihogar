import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AdditionalInfoDocument, Request, RequestRequestServices, ResponseRequestServices, ResponseSearchVINServices, Search, SearchRequestServices } from 'app/entities/Request.types';
import { TranslocoService } from '@ngneat/transloco';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';
import { environment } from 'environments/environment';

const END_POINT = `${environment.SERVICES_URL}`;

@Injectable({
    providedIn: 'root'
})
export class RequestService {
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient, private _translocoService: TranslocoService, private userService: UserService) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    updateStatusService(credit: Request, type: 1 | 2 | 3 | 4, id: number): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            userIdUpdate: this.getUserLogued(),
            creditAppId: credit.creditAppId,
            status: false,
            id: id,
            type: type
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/updateStatusService`, body, { 'headers': headers });
    }

    additionalInfo(credit: Request): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            userIdUpdate: this.getUserLogued(),
            creditAppId: credit.creditAppId,
            additionalInfo: credit.additionalInfo
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/updateAdditionalInfo`, body, { 'headers': headers });
    }

    validateVINVehicle(credit: any): Observable<ResponseSearchVINServices> {
        const request: any = {
            vin: credit.vin,
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        request.userId = this.getUserLogued();
        const service = 'searchVin';
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseSearchVINServices>(`${END_POINT}/${service}`, body, { 'headers': headers });
    }

    additionalInfoVehicle(credit: Request, update: boolean): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            vehicle: credit.vehicle
        };
        if(update){
            request.userIdUpdate = this.getUserLogued();
        }else{
            request.createUserId = this.getUserLogued();
        }
        const service = update ? 'updateVehicle' : 'vehicle';
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/${service}`, body, { 'headers': headers });
    }

    additionalInfoAddress(credit: Request): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            userIdUpdate: this.getUserLogued(),
            address: credit.address
        };

        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/updateAddress`, body, { 'headers': headers });
    }

     updateDocumentStatus(document: AdditionalInfoDocument): any {
          const request: RequestRequestServices = {
               documentId: document.id,
               status: false,
               userIdUpdate: this.getUserLogued(),
               language: this._translocoService.getActiveLang().toUpperCase()
          };
          const headers = { 'content-type': 'application/json' };
          const body = JSON.stringify(request);
          console.log(body);
          return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/updateDocumentStatus`, body, { 'headers': headers });
     }

    additionalInfoDocument(credit: Request, document: AdditionalInfoDocument[]): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            createUserId: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase(),
            document: document
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/document`, body, { 'headers': headers });
    }

    additionalInfoGetDocument(credit: Request): Observable<ResponseRequestServices>{
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            userId: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/getDocuments`, body, { 'headers': headers });
    }

    additionalInfoCommercial(credit: Request, update: boolean): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            clientId: credit.applicant.id,
            bank: credit.bank,
            branch: credit.branch,
            accountType: credit.accountType,
            accountNumber: credit.accountNumber
        };
        if(update){
            request.userIdUpdate = this.getUserLogued();
            request.id = credit.bankId;
        }else{
            request.createUserId = this.getUserLogued();
        }
        const service = update ? 'updateCommercialRef' : 'commercialRef';
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/${service}`, body, { 'headers': headers });
    }

    additionalInfoEmployment(credit: Request, update: boolean): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            employment: credit.employments
        };
        if(update){
            request.userIdUpdate = this.getUserLogued();
        }else{
            request.createUserId = this.getUserLogued();
        }
        const service = update ? 'updateEmployment' : 'employment';
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/${service}`, body, { 'headers': headers });
    }

    additionalInfoPersonals(credit: Request): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            userIdUpdate: this.getUserLogued(),
            personalReferences: credit.personals
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/updatePersonalRef`, body, { 'headers': headers });
    }

    additionalInfoCredit(credit: Request, update: boolean): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            requestedAmount: credit.requestedAmount,
            downPayment: credit.downPayment,
            payTermId: credit.payTermId,
            carPayment: credit.carPayment,
            firstPayment: credit.firstPayment,
            scheduledContract: credit.scheduledContract,
            loanTerm: credit.loanTerm,
            apr: credit.apr
        };
        if(update){
            request.userIdUpdate = this.getUserLogued();
        }else{
            request.createUserId = this.getUserLogued();
        }
        const service = update ? 'updateCreditApp' : 'creditApp';
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/${service}`, body, { 'headers': headers });
    }

    preApproval(credit: Request): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            id: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued()
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/preApproval`, body, { 'headers': headers });
    }

    acceptPreApproval(credit: Request): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            creditAppId: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued()
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/acceptPreApproval`, body, { 'headers': headers });
    }

    getPAL(credit: Request): Observable<ResponseRequestServices> {
        const request: RequestRequestServices = {
            id: credit.creditAppId,
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued()
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/getPAL`, body, { 'headers': headers });
    }

    searchCreditApp(search: Search): Observable<ResponseRequestServices> {
        const request: SearchRequestServices = {
            userId: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase(),
            initDate: search.initDate,
            endDate: search.endDate,
            status: search.status
        };

        if(search.originators != null){
            request.originators = search.originators;
        }else{
            request.originators = [];
        }
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(`${END_POINT}/searchCreditApp`);
        console.log('==== body ====');
        console.log(body);
        console.log('==== body ====');
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/searchCreditApp`, body, { 'headers': headers });
    }

    getCreditApp(creditAppId: any): Observable<ResponseRequestServices> {
        const request: SearchRequestServices = {
            userId: this.getUserLogued(),
            language: this._translocoService.getActiveLang().toUpperCase(),
            creditAppId: creditAppId
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseRequestServices>(`${END_POINT}/getCreditApp`, body, { 'headers': headers });
    }

    /**
     * Function by get User logued
     */
    getUserLogued(): number {
        const userLogued: User = this.userService.userLogued;
        if(userLogued && userLogued.userId){
            return userLogued.userId;
        }else{
            return -1;
        }
    }

}
