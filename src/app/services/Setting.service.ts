import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RequestSettingServices, ResponseSettingServices } from 'app/entities/Setting.types';
import { TranslocoService } from '@ngneat/transloco';
import { environment } from 'environments/environment';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

const END_POINT = `${environment.SERVICES_URL}`;

@Injectable({
    providedIn: 'root'
})
export class SettingService {
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient, private _translocoService: TranslocoService, private userService: UserService) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    getObjects(): Observable<ResponseSettingServices> {
        const request: RequestSettingServices = {
            type: 12,
            language: this._translocoService.getActiveLang().toUpperCase()
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseSettingServices>(`${END_POINT}/catalog`, body, { 'headers': headers });
    }

    /**
     * Get object by id
     */
    getById(id: string): Observable<ResponseSettingServices> {
        const request: RequestSettingServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            settingInfo: {
                id: id
            }
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseSettingServices>(`${END_POINT}/settingDetail`, body, { 'headers': headers });
    }

    saveBanned(bannedVehicle: any): Observable<ResponseSettingServices> {
        const request: RequestSettingServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: 1,
            bannedVehicle: bannedVehicle
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseSettingServices>(`${END_POINT}/newBannedVehicle`, body, { 'headers': headers });
    }

    /**
     * Create object
     */
    create(settingInfo: any): Observable<ResponseSettingServices> {
        const request: RequestSettingServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued(),
            settingInfo: settingInfo
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseSettingServices>(`${END_POINT}/newSetting`, body, { 'headers': headers });
    }

    /**
     * Create object
     */
     update(settingInfo: any): Observable<ResponseSettingServices> {
        const request: RequestSettingServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued(),
            settingInfo: settingInfo
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        console.log(body);
        return this._httpClient.post<ResponseSettingServices>(`${END_POINT}/updateSetting`, body, { 'headers': headers });
    }

    /**
     * Delete the object
     *
     * @param id
     */
    delete(id: string): Observable<ResponseSettingServices> {
        const request: RequestSettingServices = {
            language: this._translocoService.getActiveLang().toUpperCase(),
            author: this.getUserLogued(),
            settingInfo: {
                id: id
            }
        };
        const headers = { 'content-type': 'application/json' };
        const body = JSON.stringify(request);
        return this._httpClient.post<ResponseSettingServices>(`${END_POINT}/deleteSetting`, body, { 'headers': headers });
    }

    /**
     * Function by get User logued
     */
    getUserLogued(): number {
        const userLogued: User = this.userService.userLogued;
        if(userLogued && userLogued.userId){
            return userLogued.userId;
        }else{
            return -1;
        }
    }
}
