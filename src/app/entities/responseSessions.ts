import { User } from 'app/core/user/user.entity';

export class ResponseSessionsEntity {
    error: boolean;
    idError: number | string;
    msgError?: string;
    message?: string;
    user?: User;
}

export class ResponseChangePasswordEntity {
    error: boolean;
    idError?: string;
    msgError?: string;
    message?: string;
}
