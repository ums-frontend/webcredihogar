export interface Setting {
    id?: string;
    name: string;
    minDepositPercentage?: number;
    minDepositCash?: number;
    maxLtvNadaPercentage?: number;
    maxMonthPayPercentage?: number;
    loanTermInit?: number;
    loanTermEnd?: number;
    loanMaxValue?: number;
    vehicleMaxAge?: number;
    vehicleMaxMilage?: number;
    vehicleMaxMilageYear?: number;
    daysPayMonth?: number;
    daysPaySemiMonth?: number;
    daysPayWeek?: number;
    minReferences?: number;
    minWorkReferences?: number;
    bannedList?: any;
    status: boolean;
}

export interface BannedVehicle {
    makeId: number;
    make: string;
    modelId: number;
    model: string;
    full: boolean;
}

export class RequestSettingServices {
    userId?: number;
    author?: number;
    type?: number;
    language: string;
    settingInfo?: any;
    bannedVehicle?: any;
}

export class ResponseSettingServices {
    error: boolean;
    idError: number | string;
    msgError: string;
    catalog?: Setting[];
}
