export class RequestGetUserListEntity {
    userId: number;
    roleId: number;
    language: string;
}

export class RequestNewUserEntity {
    name: string;
    email: string;
    address: string;
    roleId: number;
    configId?: number;
    createUserId: number;
    language: string;
    acronym?: string;
    cellphone?: string;
    notes?: string;
}

export class RequestUpdateStatusUserEntity {
    userId: number;
    status: boolean;
    userIdUpdate: number;
    language: string;
}

export class RequestUpdateUserEntity {
    userId: number;
    name: string;
    address: string;
    roleId: number;
    configId: number;
    userIdUpdate: number;
    language: string;
    acronym?: string;
    cellphone?: string;
    notes?: string;
}
