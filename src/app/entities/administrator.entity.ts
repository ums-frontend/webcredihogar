export class AdministratorEntity {
    userId?: number;
    name: string;
    email: string;
    password?: string;
    roleId?: number;
    role?: string;
    status?: boolean;
  }
