export class DealershipEntity {
    userId?: number;
    name: string;
    email: string;
    address: string;
    password?: string;
    roleId?: number;
    role?: string;
    status?: boolean;
    // createUserId?: number;
    configId: number | undefined;
    acronym: string;
    cellphone?: string;
    notes?: string;
  }
