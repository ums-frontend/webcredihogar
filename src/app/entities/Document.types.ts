export interface Document{
    id?: number;
    documentId: string;
    name: string;
    min?: string;
    mandatory: boolean;
}

export class RequestDocumentServices{
    id?: number;
    userId?: number;
    type?: number;
    language: string;
    author?: number;
    documentInfo?: any;
    documentId?: string;
    name?: string;
    min?: string;
    mandatory?: boolean;
}

export class ResponseDocumentServices {
    error: boolean;
    idError: number | string;
    msgError: string;
    catalog?: Document[];
}
