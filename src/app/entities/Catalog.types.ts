export interface Catalog {
    id: number;
    name: string;
}

export class RequestCatalogServices {
    id?: string;
    type?: number;
    language: string;
    author?: number;
    name?: string;
}

export class ResponseCatalogServices {
    error: boolean;
    idError: number | string;
    msgError: string;
    catalog?: Catalog[];
}
