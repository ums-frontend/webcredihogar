export class DealerEntity {
    userId?: number;
    name: string;
    email: string;
    password?: string;
    roleId?: number;
    role?: string;
    status?: boolean;
    userNameCreate?: string;
    cellphone: string;
  }
