import { SettingsEntity } from 'app/entities/settings.entity';

export class ResponseGetUserListEntity {
    error: boolean;
    idError: number | string;
    msgError: string;
    userList?: any[];
}

export class ResponseGeneralUserEntity {
    error: boolean;
    idError: number | string;
    msgError?: string;
    message?: string;
}

export class ResponseSettingsEntity {
    error: boolean;
    idError: number | string;
    msgError?: string;
    catalog?: SettingsEntity[];
}
