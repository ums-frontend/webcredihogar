/* eslint-disable id-blacklist */
/* eslint-disable @typescript-eslint/naming-convention */
import { Client } from './Client.types';

export interface Request {
    id: string;
    creditAppId?: number;
    creditAppNumber?: number;
    applicant: Client;
    coApplicant?: Client;
    requestId: string;
    client: string;
    name: string;
    amount?: number;
    date: string;
    vin: string;
    make: string;
    model: string;
    year: string;
    type: string;
    aval: string;
    active: boolean;
    additionalInfo?: AdditionalInfo[];
    vehicle?: AdditionalInfoVehicle;
    address?: AdditionalInfoAddress[];
    documents?: AdditionalInfoDocument[];
    codocuments?: AdditionalInfoDocument[];
    bankId?: number;
    bank?: string;
    branch?: string;
    accountType?: string;
    accountNumber?: string;
    employments?: AdditionalInfoEmployment[];
    personals?: AdditionalInfoPersonal[];
    requestedAmount?: number;
    downPayment?: number;
    payTermId?: number;
    loanTerm?: number;
    carPayment?: number;
    firstPayment?: number;
    scheduledContract?: string;
    apr?: number;
    statusId?: number;
}

export interface AdditionalInfo {
    id?: number;
    clientId?: number;
    idType?: number;
    idNumber?: string;
    idExpDate?: string;
    residenceId?: number;
    yearsResidence?: number;
    fillingId?: number;
    dependantsNumber?: number;
    incomeFrecuencyId?: number;
    avgIncome?: number;
    avgRent?: number;
    avgUtilities?: number;
    bankrupcy?: boolean;
    bankrupcyIntend?: boolean;
    bankrupcyInfo?: string;
    vehicleRepossesion?: boolean;
    vehicleRepossesionInfo?: string;
    coApplicant?: boolean;
    bankrupcyDate?: string;
    vehicleRepossesionDate?: string;
}

export interface AdditionalInfoVehicle {
    id?: number;
    vin?: string;
    make?: string;
    model?: string;
    year?: number;
    mileage?: number;
    salesPrice?: number;
    stockNumber?: string;
    cleanTitle?: boolean;
    fuelId?: number;
    rideShare?: boolean;
    nadaTradeInAvg?: number;
    titleId?: number;
}

export interface AdditionalInfoAddress {
    id?: number;
    clientId: number;
    street?: string;
    number?: string;
    apt?: string;
    city?: string;
    stateId?: number;
    zip?: string;
    old?: boolean;
    years?: number;
    months?: number;
    coApplicant?: boolean;
}

export interface AdditionalInfoDocument {
    id?: number;
    documentId?: number;
    clientId?: number;
    url?: string;
    documentName?: string;
    coApplicant?: boolean;
}

export interface AdditionalInfoEmployment {
    id?: number;
    clientId?: number;
    companyName?: string;
    position?: string;
    telephone?: string;
    years?: string;
    months?: string;
    old?: boolean;
    coApplicant?: boolean;
}

export interface AdditionalInfoPersonal {
    id?: number;
    clientId?: number;
    name?: string;
    telephone?: string;
    relationship?: string;
    work?: boolean;
}

export class RequestRequestServices {
    createUserId?: number;
    userIdUpdate?: number;
    author?: number;
    id?: number;
    userId?: number;
    creditAppId?: number;
    language: string;
    additionalInfo?: any;
    address?: any;
    vehicle?: any;
    document?: AdditionalInfoDocument[];
    documentId?: number;
    bank?: string;
    branch?: string;
    accountType?: string;
    accountNumber?: string;
    employment?: AdditionalInfoEmployment[];
    personalReferences?: AdditionalInfoPersonal[];
    requestedAmount?: number;
    downPayment?: number;
    payTermId?: number;
    loanTerm?: number;
    carPayment?: number;
    clientId?: number;
    firstPayment?: number;
    scheduledContract?: string;
    apr?: number;
    status?: boolean;
    type?: number;
}

export class Search {
    initDate: string;
    endDate: string;
    status?: number;
    originators?: any;
    creditAppId?: number;
}

export class SearchRequestServices {
    userId?: number;
    language: string;
    initDate?: string;
    endDate?: string;
    status?: number;
    originators?: any;
    creditAppId?: number;
}

export class CreditApp {
    ca_status_name: string;
    client_id: number;
    client_last_name: string;
    client_middle_name: string;
    client_name: string;
    credit_app_approval_letter: string;
    credit_app_create_date: string;
    credit_app_id: number;
    credit_app_number: string;
    credit_app_requested_amount: number;
    originator: string;
    vehicle_make: string;
    vehicle_model: string;
    vehicle_vin: string;
    vehicle_year: number;
    dealer: string;
    documents: AdditionalInfoDocument[];
}

export class ResponseRequestServices {
    error: boolean;
    idError: number | string;
    msgError: string;
    clientList?: Client[];
}


export class ResponseSearchVINServices {
    error: boolean;
    idError: number | string;
    msgError?: string;
    vin?: vinResponse;
}

export class vinResponse {
    make?: string;
    manufacturer?: string;
    model?: string;
    modelYear?: string;
    type?: string;
    driveType?: string;
    fuelType?: string;
}
