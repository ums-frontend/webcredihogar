export interface Client {
    id?: number;
    account?: string;
    email?: string;
    name: string;
    middleName?: string;
    lastName?: string | null;
    dateBirth?: string | null;
    cellphone?: string | null;
    homePhone: string | null;
    ssnItin?: number;
    status: boolean;
}

export class GeneralInfoClient {
    requestNumber?: number;
    dealer?: string;
    dealership?: string;
    nameClient?: string;
}

export class RequestClientServices {
    userId?: number;
    language: string;
    createUserId?: number;
    userIdUpdate?: number;
    clientId?: number;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    birthdate?: string;
    cellphone?: string;
    homephone?: string;
    email?: string;
    ssn?: number;
    status?: boolean;
    searchType?: number;
    searchParam?: string;
}

export class ResponseClientServices {
    error: boolean;
    idError: number | string;
    msgError: string;
    clientList?: Client[];
}

export class ResponseClientGeneralServices {
    error: boolean;
    idError?: number | string;
    msgError?: string;
    message?: string;
}
