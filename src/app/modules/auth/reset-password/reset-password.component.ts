import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { finalize, switchMap } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { FuseValidators } from '@fuse/validators';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
    selector     : 'auth-reset-password',
    templateUrl  : './reset-password.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
    styles       : [
        `
        .bg-gray-800{
            background-color: #1A1A1A !important;
        }
        .text-primary-500 {
            color: #3CB4E5;
        }
        mat-label, .mat-placeholder-required {
            color: #64748b;
        }
        `
    ]
})
export class AuthResetPasswordComponent implements OnInit
{
    @ViewChild('resetPasswordNgForm') resetPasswordNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    resetPasswordForm: FormGroup;
    showAlert: boolean = false;
    userIdSelected: number | null;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private translocoService: TranslocoService
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.resetPasswordForm = this._formBuilder.group({
                password       : ['', Validators.required],
                passwordConfirm: ['', Validators.required]
            },
            {
                validators: FuseValidators.mustMatch('password', 'passwordConfirm')
            }
        );
        //Get params router
        this._activatedRoute.paramMap.subscribe((params) => {
              this.userIdSelected = (params.get('user')) ? Number(params.get('user')): null;
            }
        );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Reset password
     */
    resetPassword(): void
    {
        // Return if the form is invalid
        if ( this.resetPasswordForm.invalid )
        {
            return;
        }

        if(this.userIdSelected == null){
             // Set the alert
             //Get text error
             this.translocoService.selectTranslate('resetPass.errorParamUser').pipe()
            .subscribe((textErrorForm) => {
                    this.alert = {
                       type   : 'error',
                       message: textErrorForm
                   };
                   // Show the alert
                   this.showAlert = true;
            });
            setTimeout(() => {
                this._router.navigateByUrl('/sign-in');
            }, 5000);
            return;
        }

        // Disable the form
        this.resetPasswordForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Send the request to the server
        this._authService.resetPassword(this.resetPasswordForm.get('password').value, this.userIdSelected)
            .pipe(
                finalize(() => {

                    // Re-enable the form
                    this.resetPasswordForm.enable();

                    // Reset the form
                    this.resetPasswordNgForm.resetForm();

                    // Show the alert
                    this.showAlert = true;
                })
            )
            .subscribe(
                (response) => {

                    // Set the alert
                    this.translocoService.selectTranslate('resetPass.successMessage').pipe()
                        .subscribe((textsuccess) => {
                            this.alert = {
                                type   : 'success',
                                message: textsuccess
                            };
                    });
                    setTimeout(() => {
                        this._router.navigateByUrl('/sign-in');
                    }, 5000);
                },
                (response) => {
                    this.translocoService.selectTranslate('resetPass.errorTryCatch').pipe()
                        .subscribe((textErrorForm) => {
                            this.alert = {
                                type   : 'error',
                                message: textErrorForm
                            };
                    });
                }
            );
    }
}
