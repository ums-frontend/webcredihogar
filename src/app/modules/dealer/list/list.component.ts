import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Subject, takeUntil } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { TablePagination } from 'app/entities/InterfacePagination.interface';
import { cloneDeep } from 'lodash-es';

import { DealerEntity } from 'app/entities/dealer.entity';
import { DealerService } from 'app/services/dealer.service';
import { FuseAlertType } from '@fuse/components/alert';
import { TranslocoService } from '@ngneat/transloco';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: fuseAnimations
})
export class ListComponent implements OnInit {
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    rowListAll: DealerEntity[] = [];
    rowList: DealerEntity[] = [];
    selectedDealer: DealerEntity | null = null;
    selectedDealerForm: FormGroup;
    isLoading: boolean = false;
    // Load the gray layer
    screenLoading: boolean = false;
    pagination: TablePagination;
    flashMessage: 'success' | 'error' | null = null;
    private unsubscribeAll: Subject<any> = new Subject<any>();
    // eslint-disable-next-line @typescript-eslint/member-ordering
    searchInputControl: FormControl = new FormControl();
    // eslint-disable-next-line @typescript-eslint/member-ordering
    proccessNewDealer: boolean = false;
    // eslint-disable-next-line @typescript-eslint/member-ordering
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    // eslint-disable-next-line @typescript-eslint/member-ordering
    showAlert: boolean = false;

    // flag to show btn add new
    // eslint-disable-next-line @typescript-eslint/member-ordering
    showBtnNew: boolean = false;

    //Cargar el usuario logueado
    // eslint-disable-next-line @typescript-eslint/member-ordering
    user: User;

    /**
     * Constructor
     */
    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private dealerService: DealerService,
        private formBuilder: FormBuilder,
        private translocoService: TranslocoService,
        private _userService: UserService,
    ) { }
    inicializeTable(): void {
        if ( this._sort && this._paginator )
        {
             // Set the initial sort
             this._sort.sort({
                 id          : 'userId',
                 start       : 'asc',
                 disableClear: true
             });

             // Mark for check
             this.changeDetectorRef.markForCheck();

             // If the user changes the sort order...
             this._sort.sortChange
                 .pipe(takeUntil(this.unsubscribeAll))
                 .subscribe(() => {
                     // Reset back to the first page
                     this._paginator.pageIndex = 0;

                     // Close the details
                     this.closeDetails();
                 });

             // Get dealers if sort or page changes
             merge(this._sort.sortChange, this._paginator.page).subscribe(
                 () => {
                     this.closeDetails();
                     this.isLoading = true;
                     // eslint-disable-next-line max-len
                     let objResult: any;
                     if(this._sort && this._paginator){
                        objResult  = this.calculatePagination(this.rowListAll ,null, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize );
                     }else {
                        objResult  = this.calculatePagination(this.rowListAll);
                     }
                     this.rowList            = objResult.list;
                     this.pagination         = objResult.pagination;
                     this.isLoading = false;
                     this.changeDetectorRef.markForCheck();
                 }
             );
        }
    }

    ngOnInit(): void {
        //Validate show Btn New
        this.validateBtnAddDealer();

        //load list dealer
        this.loadDealer(true);

        // Create the selected dealer form
        this.selectedDealerForm = this.formBuilder.group({
            userId          : [''],
            name            : ['', [Validators.required]],
            email           : ['', [Validators.required, Validators.email]],
            status          : [true],
            userNameCreate  : [''],
            cellphone   : ['', [Validators.required]]
        });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
        .subscribe(
            (query) => {
                this.closeDetails();
                this.isLoading = true;
                let objResult: any;
                if(this._sort && this._paginator){
                    objResult  = this.calculatePagination(this.rowListAll ,query, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize );
                }else {
                    objResult  = this.calculatePagination(this.rowListAll, query);
                    setTimeout(() => {
                        this.inicializeTable();
                    }, 100);
                }
                this.rowList            = objResult.list;
                this.pagination         = objResult.pagination;
                this.isLoading = false;
                this.changeDetectorRef.markForCheck();
            }
        );
    }

    /**
     * validate show btn add
     */
    validateBtnAddDealer(): void {
        //Carga de usuario loguado
        this.user = this._userService.userLogued;
        if(this.user.roleId === 2 || (this.user.roleId === 3 && this.user.managment)){
            this.showBtnNew = true;
        }
    }


    /**
     * Load dealer
     */
    loadDealer(inicializeTable: boolean = false): void {
        //Ability button add
        this.proccessNewDealer = false;
        this.screenLoading = true;
        this.dealerService.getDealers().subscribe(
                (res: any) => {
                    if(res.error){
                         // Set the alert
                        this.showMessage('warn', 'Error '+res.idError+'!. '+res.msgError);
                        this.rowList = [];
                    }else{
                        this.rowListAll         = res.userList;
                        const objResult: any    = this.calculatePagination(res.userList);
                        this.rowList            = objResult.list;
                        this.pagination         = objResult.pagination;
                    }
                    if(inicializeTable){
                        setTimeout(() => {
                            this.inicializeTable();
                        }, 500);
                    }
                    this.changeDetectorRef.markForCheck();
                    this.screenLoading = false;
                },
                (err) => {
                    console.log(err);
                     // Set the alert
                    this.showMessage('warn', 'Error! '+ JSON.stringify(err));
                    this.screenLoading = false;
                }
        );
    }

    /**
     * Calculate pagination by table
     */
    calculatePagination(list: DealerEntity[], search: string = '', sort: any = 'userId', order: string = 'asc', page: number = 0, size: number = 10 ): any {

        // // Clone the dealers
        let dealerListClone: DealerEntity[] = cloneDeep(list);

        // Sort the dealerListClone
        if ( sort === 'userId' || sort === 'name' || sort === 'status' || sort === 'userNameCreate'  )
        {
            dealerListClone.sort((a, b) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldA: any;
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldB: any;
                if(sort === 'userId'){
                    fieldA = parseInt(a[sort], 10);
                    fieldB = parseInt(b[sort], 10);
                    // eslint-disable-next-line max-len
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB, undefined, {numeric:'true'}) : fieldB.toString().localeCompare(fieldA, undefined, {numeric:'true'});
                }else{
                    fieldA = a[sort].toString().toUpperCase();
                    fieldB = b[sort].toString().toUpperCase();
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB) : fieldB.toString().localeCompare(fieldA);
                }
            });
        }
        else
        {
            dealerListClone.sort((a, b) => order === 'asc' ? a[sort] - b[sort] : b[sort] - a[sort]);
        }

        // If search exists...
        if ( search )
        {
            // Filter the dealerListClone
            dealerListClone = dealerListClone.filter(contact => contact.name && contact.name.toLowerCase().includes(search.toLowerCase()));
        }

        // Paginate - Start
        const dealerListCloneLength = dealerListClone.length;

        // Calculate pagination details
        const begin = page * size;
        const end = Math.min((size * (page + 1)), dealerListCloneLength);
        const lastPage = Math.max(Math.ceil(dealerListCloneLength / size), 1);

        // Prepare the pagination object
        let pagination = {};

        // If the requested page number is bigger than
        // the last possible page number, return null for
        // dealerListClone but also send the last possible page so
        // the app can navigate to there
        if ( page > lastPage )
        {
            dealerListClone = null;
            pagination = {
                lastPage
            };
        }
        else
        {
            // Paginate the results by size
            dealerListClone = dealerListClone.slice(begin, end);

            // Prepare the pagination mock-api
            pagination = {
                length    : dealerListCloneLength,
                size      : size,
                page      : page,
                lastPage  : lastPage,
                startIndex: begin,
                endIndex  : end - 1
            };
        }
        return {
                list: dealerListClone,
                pagination: pagination
        };
    }

    /*
    * New row by new dealer
    */
    newDealer(): void {
        this.proccessNewDealer = true;

        const newDealerEnty: DealerEntity = {
            status: true,
            email: '',
            name: '',
            cellphone: undefined
        };

        let elementScroll: any = document.getElementsByClassName('elementScroll');

        //Add row
        this.rowList.unshift(newDealerEnty);

        // Go to new dealer
        this.selectedDealer = newDealerEnty;

        // Fill the form
        this.selectedDealerForm.patchValue(newDealerEnty);

        // Mark for check
        this.changeDetectorRef.markForCheck();

        // Scroll to top
        elementScroll[0].scrollTop = 0;
    }

    cancelnewDealer(): void {
        if(!this.rowList[0].userId){
            this.rowList.splice(0, 1);
        }
        this.proccessNewDealer = false;
    }

    /*
    * Create dealer
    */
     saveDealer(): void
     {
        if (this.selectedDealerForm.valid) {
            // Get the dealer object
           const dealer: DealerEntity = this.selectedDealerForm.getRawValue();
           this.isLoading = true;
            // Create the dealer
            this.dealerService.createDealer(dealer).subscribe((res: any) => {
                if(res.error){
                    // Alert error
                    if(res.message && res.message !== ''){
                        this.showMessage('warning', 'Error '+res.idError+'!. '+res.message);
                        //Cargar dealeristradores
                        this.loadDealer();
                    }else{
                        this.showMessage('warn', 'Error '+res.idError+'!. '+res.msgError);
                    }
                }else{
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Cargar administradores
                    this.loadDealer();
                }
                this.isLoading = false;
                this.changeDetectorRef.markForCheck();
            });
        } else {
            //set text second by language
            this.translocoService.selectTranslate('forms.errorData').pipe()
            .subscribe((textErrorForm) => {
                    this.showMessage('warn', textErrorForm);
            });
        }
     }

    /**
     * Update the selected dealer using the form data
     */
    updateSelectedDealer(): void
    {
        if (this.selectedDealerForm.dirty && this.selectedDealerForm.valid) {
            // Get the dealer object
            const dealer: DealerEntity = this.selectedDealerForm.getRawValue();
            this.isLoading = true;
            this.screenLoading = true;
            // Update the dealer on the server
            this.dealerService.updateDealer(dealer).subscribe((res: any) => {
                if(res.error){
                    // Alert error
                    this.showMessage('warn', 'Error '+res.idError+'!. '+res.msgError);
                }else{
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Update Lists general
                    this.reloadGeneralLists(dealer);
                    //Reload list commented
                    // const objResult: any    = this.calculatePagination(this.rowListAll);
                    // this.rowList            = objResult.list;
                    // this.pagination         = objResult.pagination;
                }
                this.isLoading = false;
                this.screenLoading = false;
                this.changeDetectorRef.markForCheck();
            },
            (err) => {
                console.log(err);
                 // Set the alert
                this.showMessage('warn', 'Error! '+ JSON.stringify(err));
                this.screenLoading = false;
            });

        } else {
            //set text second by language
            const messageError = (!this.selectedDealerForm.dirty) ? 'forms.errorDirty' : 'forms.errorData';
            this.translocoService.selectTranslate(messageError).pipe()
            .subscribe((textErrorForm) => {
                    this.showMessage('warn', textErrorForm);
            });
        }
    }

    /**
     * Update status of selected dealer using the form data
     */
     updateStatusSelectedDealer(event: MatSlideToggleChange): void
     {

        console.log('toggle', event.checked);
         if (this.selectedDealerForm.dirty && this.selectedDealerForm.valid) {
             // Get the admin object
            const dealer: DealerEntity = this.selectedDealerForm.getRawValue();
            this.isLoading = true;
             // Update status admin on the server
             this.dealerService.updateStatusDealer(dealer).subscribe((res: any) => {
                 if(res.error){
                     //Return value
                     this.selectedDealerForm.controls['status'].setValue(!event.checked);
                     // Alert error
                     this.showMessage('warn', 'Error '+res.idError+'!. '+res.msgError);
                 }else{
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Update Lists general
                    this.reloadGeneralLists(dealer);
                    //Reload list commented
                    //  const objResult: any    = this.calculatePagination(this.rowListAll);
                    //  this.rowList            = objResult.list;
                    //  this.pagination         = objResult.pagination;
                 }
                 this.isLoading = false;
                 this.changeDetectorRef.markForCheck();
             });
         } else {
             //set text second by language
             const messageError = (!this.selectedDealerForm.dirty) ? 'forms.errorDirty' : 'forms.errorData';
             this.translocoService.selectTranslate(messageError).pipe()
             .subscribe((textErrorForm) => {
                     this.showMessage('warn', textErrorForm);
             });
         }
    }

    /**
     * Function to reload changes in general lists
     */
    reloadGeneralLists(dealer: DealerEntity): void {
        //Reload List selected
        const indexDealerListSelected: number = this.rowList.findIndex(item => item.userId && item.userId === dealer.userId);
        if(indexDealerListSelected>=0){
            this.rowList[indexDealerListSelected] = dealer;
        }

        //Reload General List
        const indexDeralerListGeneral: number = this.rowListAll.findIndex(item => item.userId && item.userId === dealer.userId);
        if(indexDeralerListGeneral>=0){
            this.rowListAll[indexDeralerListGeneral] = dealer;
        }
    }


    /*** Toggle dealer details
      * @param dealerId
      */
    toggleDetails(dealer: DealerEntity): void {

        //Cancel new dealer
        if(this.proccessNewDealer && dealer?.userId){
            this.cancelnewDealer();
        }

        // If the dealer is already selected...
        if (this.selectedDealer && this.selectedDealer.userId === dealer.userId) {
            // Close the details
            this.closeDetails();
            return;
        }

        // Go to load dealer
        this.selectedDealer = dealer;

        // Fill the form
        this.selectedDealerForm.reset();
        this.selectedDealerForm.patchValue(dealer);

        // Mark for check
        this.changeDetectorRef.markForCheck();
    }

    /**
     * Close the details
     */
    closeDetails(): void {
        this.selectedDealer = null;
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this.changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            this.changeDetectorRef.markForCheck();
        }, 3000);
    }

    showMessage(type: FuseAlertType, message: string, _timer: number = 3500): void {
        setTimeout(() => {
            this.showAlert = false;
            this.changeDetectorRef.markForCheck();
        }, _timer);
        this.alert = {
            type   : type,
            message: message
        };
        this.showAlert = true;
    }
}
