import { Route } from '@angular/router';
import { AuthGuardService } from 'app/services/auth-guard.service';
import { DealerComponent } from './dealer.component';
import { ListComponent } from './list/list.component';

export const dealerRoutingModule: Route[] = [
    {
        path     : '',
        component: DealerComponent,
        canActivate:[AuthGuardService],
        children : [
            {
                path     : '',
                component: ListComponent,
                canActivate:[AuthGuardService]
            }
        ]
    }
];
