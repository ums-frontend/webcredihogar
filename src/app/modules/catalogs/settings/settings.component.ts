import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatDrawer } from '@angular/material/sidenav';
import { MatSort } from '@angular/material/sort';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { SettingService } from 'app/services/Setting.service';
import { Pagination } from 'app/entities/Pagination.types';
import { BannedVehicle, Setting } from 'app/entities/Setting.types';
import { Observable, Subject, takeUntil, merge } from 'rxjs';
import { cloneDeep } from 'lodash-es';
import { CatalogService } from 'app/services/Catalog.service';
import { Catalog } from 'app/entities/Catalog.types';
import { map, startWith } from 'rxjs/operators';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;
    @ViewChild('drawer') private drawer: MatDrawer;

    rowListAll: Setting[] = [];
    rowList: Setting[] = [];

    flashMessage: 'success' | 'error' | 'deleted' | null = null;
    flashMessageMsg: string = '';
    isLoading: boolean = false;
    // Load the gray layer
    screenLoading: boolean = false;
    pagination: Pagination;
    searchInputControl: FormControl = new FormControl();
    selectedItem: Setting | null = null;
    selectedItemForm: FormGroup;
    tagsEditMode: boolean = false;
    editMode: boolean = false;
    addNewChip: boolean = false;

    //  autocomplete brand difinition
    brandList: any[] = [];
    selectedBrand: Catalog = null;
    controlBrandForm = new FormControl();
    filteredBrandList: Observable<Catalog[]>;
    //  autocomplete model difinition
    modelList: any[] = [];
    selectedModel: Catalog = null;
    controlModelForm = new FormControl();
    filteredModelList: Observable<Catalog[]>;
    //error banned vehicles
    isErrorBannedVehicles = false;
    strErrorBannedVehicles = '';

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        private _changeDetectorRef: ChangeDetectorRef
        , private _fuseConfirmationService: FuseConfirmationService
        , private _formBuilder: FormBuilder
        , private _dataService: SettingService
        , private _catalogService: CatalogService
    ) {
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.load(true);

        // Create the selected object form
        this.selectedItemForm = this._formBuilder.group({
            id: [''],
            name: ['', [Validators.required]],
            minDepositPercentage: ['', [Validators.required]],
            minDepositCash: ['', [Validators.required]],
            maxLtvNadaPercentage: ['', [Validators.required]],
            maxMonthPayPercentage: ['', [Validators.required]],
            loanTermInit: ['', [Validators.required]],
            loanTermEnd: ['', [Validators.required]],
            loanMaxValue: ['', [Validators.required]],
            vehicleMaxAge: ['', [Validators.required]],
            vehicleMaxMilage: ['', [Validators.required]],
            vehicleMaxMilageYear: ['', [Validators.required]],
            daysPayMonth: ['', [Validators.required]],
            daysPaySemiMonth: ['', [Validators.required]],
            daysPayWeek: ['', [Validators.required]],
            minReferences: ['', [Validators.required]],
            minWorkReferences: ['', [Validators.required]],
            status: [false]
        });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .subscribe(
                (query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    let objResult: any;
                    if (this._sort && this._paginator) {
                        objResult = this.calculatePagination(this.rowListAll, query, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize);
                    } else {
                        objResult = this.calculatePagination(this.rowListAll, query);
                        setTimeout(() => {
                            this.inicializeTable();
                        }, 100);
                    }
                    this.rowList = objResult.list;
                    this.pagination = objResult.pagination;
                    this.isLoading = false;
                }
            );

    }


    /**
     * After view init
     */
    ngAfterViewInit(): void {

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    inicializeTable(): void {
        if (this._sort && this._paginator) {
            // Set the initial sort
            this._sort.sort({
                id: 'name',
                start: 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;
                    // Close the details
                    this.closeDetails();
                });

            // Get admins if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).subscribe(() => {
                this.closeDetails();
                // eslint-disable-next-line max-len
                let objResult: any;
                if (this._sort && this._paginator) {
                    objResult = this.calculatePagination(this.rowListAll, null, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize);
                } else {
                    objResult = this.calculatePagination(this.rowListAll);
                }
                this.rowList = objResult.list;
                this.pagination = objResult.pagination;
                this._changeDetectorRef.markForCheck();
            });
        }
    }

    /**
     * Load
     */
    load(inicializeTable: boolean = false): void {
        //Load Brand
        this.loadBrand();

        this.screenLoading = true;
        this._dataService.getObjects().subscribe(
            (res: any) => {
                if (res.error) {
                    this.rowList = [];
                } else {
                    this.rowListAll = res.catalog;
                    const objResult: any = this.calculatePagination(res.catalog);
                    this.rowList = objResult.list;
                    this.pagination = objResult.pagination;
                }
                if (inicializeTable) {
                    setTimeout(() => {
                        this.inicializeTable();
                    }, 500);
                }
                this._changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                console.log(err);
                this.screenLoading = false;
            }
        );
    }

    /**
     * Load Brand Catolog
     */
    loadBrand(): any {
        this.screenLoading = true;
        this._catalogService.getCatalog(3).subscribe(
            (res: any) => {
                if (res.error) {
                    this.brandList = [];
                } else {
                    this.brandList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                console.log(err);
                this.screenLoading = false;
            }
        );
    }

    /**
     * Calculate pagination by table
     */
    calculatePagination(list: Setting[], search: string = '', sort: any = 'id', order: string = 'asc', page: number = 0, size: number = 10): any {
        // // Clone the admins
        let adminListClone: Setting[] = cloneDeep(list);
        // Sort the adminListClone
        if (sort === 'id' || sort === 'name' || sort === 'status') {
            adminListClone.sort((a, b) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldA: any;
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldB: any;
                if (sort === 'id') {
                    fieldA = parseInt(a[sort], 10);
                    fieldB = parseInt(b[sort], 10);
                    // eslint-disable-next-line max-len
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB, undefined, { numeric: 'true' }) : fieldB.toString().localeCompare(fieldA, undefined, { numeric: 'true' });
                } else {
                    fieldA = a[sort].toString().toUpperCase();
                    fieldB = b[sort].toString().toUpperCase();
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB) : fieldB.toString().localeCompare(fieldA);
                }
            });
        }
        else {
            adminListClone.sort((a, b) => order === 'asc' ? a[sort] - b[sort] : b[sort] - a[sort]);
        }

        // If search exists...
        if (search) {
            // Filter the adminListClone
            adminListClone = adminListClone.filter(contact => contact.name && contact.name.toLowerCase().includes(search.toLowerCase()));
        }

        // Paginate - Start
        const adminListCloneLength = adminListClone.length;

        // Calculate pagination details
        const begin = page * size;
        const end = Math.min((size * (page + 1)), adminListCloneLength);
        const lastPage = Math.max(Math.ceil(adminListCloneLength / size), 1);

        // Prepare the pagination object
        let pagination = {};

        // If the requested page number is bigger than
        // the last possible page number, return null for
        // adminListClone but also send the last possible page so
        // the app can navigate to there
        if (page > lastPage) {
            adminListClone = null;
            pagination = {
                lastPage
            };
        }
        else {
            // Paginate the results by size
            adminListClone = adminListClone.slice(begin, end);

            // Prepare the pagination mock-api
            pagination = {
                length: adminListCloneLength,
                size: size,
                page: page,
                lastPage: lastPage,
                startIndex: begin,
                endIndex: end - 1
            };
        }
        return {
            list: adminListClone,
            pagination: pagination
        };
    }

    /**
     * Toggle object details
     *
     * @param item
     */
    toggleDetails(item: Setting): void {
        this.screenLoading = true;
        // If the object is already selected...
        if (this.selectedItem && this.selectedItem.id === item.id) {
            // Close the details
            this.closeDetails();
            return;
        }

        // Get the object by id
        this._dataService.getById(item.id)
            .subscribe(
                (res: any) => {
                    if (res.error) {
                        this.selectedItem = null;
                    } else {
                        this.editMode = true;
                        // Set the selected object
                        this.selectedItem = res.settingInfo;
                        // Fill the form
                        this.selectedItemForm.patchValue(res.settingInfo);
                        //Open side nav
                        this.drawer.toggle();
                        // Mark for check
                        this._changeDetectorRef.markForCheck();
                    }
                    this.screenLoading = false;
                },
                (err) => {
                    console.log(err);
                    this.screenLoading = false;
                });

        //  autocomplete brand
        this.filteredBrandList = this.controlBrandForm.valueChanges.pipe(
            startWith(''),
            map(value => (typeof value === 'string' ? value : null)),
            map(name => (name ? this._filterBrand(name) : this.brandList.slice())),
        );
        //  autocomplete model
        this.filteredModelList = this.controlModelForm.valueChanges.pipe(
            startWith(''),
            map(value => (typeof value === 'string' ? value : null)),
            map(name => (name ? this._filterModel(name) : this.modelList.slice())),
        );
    }

    /**
     * load Brand vehicle
     */
    loadBrandVehicle(): void {
        if(this.controlBrandForm.value){
            if(!this.controlBrandForm.value.name){
                console.log('Load Brand');
                const objSelected: any[] = this.brandList.filter(option => option.name.toLowerCase() === this.controlBrandForm.value.toLowerCase());
                if(objSelected && objSelected.length > 0){
                    this.displayDataBrand(objSelected[0]);
                }else{
                    this.selectedBrand = null;
                    this.selectedModel = null;
                    this.modelList = [];
                    this.controlModelForm.setValue('');
                    this._changeDetectorRef.markForCheck();
                }
            }
        }else{
            this.selectedBrand = null;
        }
    }
    /**
     * load Model vehicle
     */
     loadModelVehicle(): void {
        if(this.controlModelForm.value){
            if(!this.controlModelForm.value.name){
                console.log('Load Model');
                const objSelected: any[] = this.modelList.filter(option => option.name.toLowerCase() === this.controlModelForm.value.toLowerCase());
                if(objSelected && objSelected.length > 0){
                    this.displayDataModel(objSelected[0]);
                }
            }
        }else{
            this.selectedModel = null;
        }
    }

    /**
     * Remove item brand or model
     */
    removeItemVehicle(item): void{
        const index: number = this.selectedItem?.bannedList.indexOf(item);
        this.selectedItem?.bannedList.splice(index, 1);
    }


    /**
     * Close the details
     */
    closeDetails(): void {
        this.drawer.close();
        this.selectedItem = null;
    }


    /**
     * Create object
     */
    create(): void {
        this.selectedItemForm.reset();
        this._catalogService.getCatalog(2).subscribe(
            (res: any) => {
                if (!res.error) {
                    const item: Setting = {
                        name: ''
                        , status: true
                        , bannedList: res.catalog
                    };
                    this.editMode = false;
                    // Go to new object
                    this.selectedItem = item;
                    // Fill the form
                    this.selectedItemForm.patchValue(item);
                    //Open side nav
                    this.drawer.toggle();
                    // Mark for check
                    this._changeDetectorRef.markForCheck();
                }
                this._changeDetectorRef.markForCheck();
            }
        );
    }

    /**
     * Update the selected object using the form data
     */
    update(): void {
        // Get the object object
        if (this.selectedItemForm.invalid) {
            return;
        }

        this.screenLoading = true;
        if (!this.editMode) {
            const item = this.selectedItemForm.getRawValue();
            item.bannedList = this.selectedItem?.bannedList;
            this._dataService.create(item).subscribe(
                (res: any) => {
                    if (res.error) {
                        const msg = res.idError + '-' + res.msgError;
                        this.showFlashMessage('error', msg);
                    } else {
                        this.load(false);
                        this.showFlashMessage('success');
                        this.selectedItemForm.reset();
                        setTimeout(() => {
                            this.closeDetails();
                        }, 3000);
                    }
                    this.screenLoading = false;
                },
                (err) => {
                    this.showFlashMessage('error', 'Error! ' + JSON.stringify(err));
                    this.screenLoading = false;
                }
            );
        } else {
            const item = this.selectedItemForm.getRawValue();
            item.bannedList = this.selectedItem?.bannedList;
            this._dataService.update(item).subscribe(
                (res: any) => {
                    if (res.error) {
                        const msg = res.idError + '-' + res.msgError;
                        this.showFlashMessage('error', msg);
                    } else {
                        this.load(false);
                        this.showFlashMessage('success');
                        this.selectedItemForm.reset();
                        setTimeout(() => {
                            this.closeDetails();
                        }, 3000);
                        this.screenLoading = false;
                    }
                },
                (err) => {
                    this.showFlashMessage('error', 'Error! ' + JSON.stringify(err));
                    this.screenLoading = false;
                }
            );
        }
    }

    /**
     * Delete the selected object using the form data
     */
    delete(): void {
        this._dataService.delete(this.selectedItem?.id).subscribe(
            (res: any) => {
                if (res.error) {
                    const msg = res.idError + '-' + res.descripcionError;
                    this.showFlashMessage('error', msg);
                } else {
                    this.load(false);
                    this.showFlashMessage('deleted');
                    setTimeout(() => {
                        this.closeDetails();
                    }, 3000);
                }
            },
            (err) => {
                this.showFlashMessage('error', 'Error! ' + JSON.stringify(err));
            }
        );
    }

    addBannedVehicle(): void {
        if (this.controlBrandForm.value !== null) {
            const full = this.controlModelForm.value === null;
            const brandId = (this.selectedBrand && this.selectedBrand.id) ? this.selectedBrand.id : 0;
            const brand = (this.selectedBrand && this.selectedBrand.name) ? this.selectedBrand.name : this.controlBrandForm.value;
            const modelId = (this.selectedModel && this.selectedModel.id) ? this.selectedModel.id : 0;
            const model = (this.selectedModel && this.selectedModel.name) ? this.selectedModel.name : this.controlModelForm.value === null ? '' : this.controlModelForm.value;

            const bannedVehicle: BannedVehicle = {
                makeId: brandId,
                modelId: modelId,
                full: full,
                make: brand,
                model: model,
            };
            this.screenLoading = true;
            this._dataService.saveBanned(bannedVehicle).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isErrorBannedVehicles = true;
                        this.strErrorBannedVehicles = res.descripcionError;
                    } else {
                        this.isErrorBannedVehicles = false;
                        this.strErrorBannedVehicles = '';
                        this.selectedItem.bannedList.push(res.vehicle);
                        this.controlBrandForm.reset();
                        this.controlModelForm.reset();
                        if(brandId === 0){
                            this.loadBrand();
                        }
                    }
                    this._changeDetectorRef.markForCheck();
                    this.screenLoading = false;
                },
                (err) => {
                    this.screenLoading = false;
                }
            );
        }
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error' | 'deleted', msg: string = ''): void {
        // Show the message
        this.flashMessage = type;
        this.flashMessageMsg = msg;
        // Mark for check
        this._changeDetectorRef.markForCheck();
        // Hide it after 3 seconds
        setTimeout(() => {
            this.flashMessage = null;
            this.flashMessageMsg = '';
            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    //  function display data autocomplete brand
    displayDataBrand = (catalog): string => {
        console.log('Proccess displayDataBrand');
        if (catalog != null) {
            this.selectedBrand = catalog;
            this.downloadModels(catalog);
        }
        return catalog && catalog.name ? catalog.name : '';
    };

    //  function display data autocomplete model
    displayDataModel = (catalog): string => {
        if (catalog != null) {
            this.selectedModel = catalog;
        }
        return catalog && catalog.name ? catalog.name : '';
    };

    downloadModels(brand: Catalog): void {
        this.screenLoading = true;
        this.selectedModel = null;
        this.modelList = [];
        this._catalogService.getModels(brand.id).subscribe(
            (res: any) => {
                if (res.error) {
                    this.modelList = [];
                } else {
                    this.modelList = res.catalog;
                    console.log(res.catalog);
                }
                this.controlModelForm.setValue('');
                this._changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                console.log(err);
            }
        );
    }

    minZero(event: Event, key: string): void{
        if(Number((event.target as HTMLInputElement).value) < 0){
            this.selectedItemForm.get(key).patchValue('0');
            this._changeDetectorRef.markForCheck();
        }
    }

    maxValue(event: Event, key: string, limit: number): void{
        this.minZero(event, key);
        if(Number((event.target as HTMLInputElement).value) > limit){
            this.selectedItemForm.get(key).patchValue(limit);
            this._changeDetectorRef.markForCheck();
        }
    }

    //  filter input autocomplete brand
    private _filterBrand(name: string): Catalog[] {
        const filterValue = name.toLowerCase();
        const cts = this.brandList.filter(option => option.name.toLowerCase().includes(filterValue));
        return cts;
    }

    //  filter input autocomplete model
    private _filterModel(name: string): Catalog[] {
        const filterValue = name.toLowerCase();
        const cts = this.modelList.filter(option => option.name.toLowerCase().includes(filterValue));
        return cts;
    }

}
