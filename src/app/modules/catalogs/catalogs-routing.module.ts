import { Route } from '@angular/router';
import { AuthGuardService } from 'app/services/auth-guard.service';
import { DocumentsComponent } from './documents/documents.component';
import { SettingsComponent } from './settings/settings.component';

export const catalogsRouting: Route[] = [
  {
      path: 'documents',
      component: DocumentsComponent,
      canActivate:[AuthGuardService]
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate:[AuthGuardService]
  }
];
