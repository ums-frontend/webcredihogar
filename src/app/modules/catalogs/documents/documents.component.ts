import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatDrawer } from '@angular/material/sidenav';
import { MatSort } from '@angular/material/sort';
import { DocumentService } from 'app/services/Document.service';
import { Document } from 'app/entities/Document.types';
import { Pagination } from 'app/entities/Pagination.types';
import { Subject, takeUntil, merge } from 'rxjs';
import { cloneDeep } from 'lodash-es';

@Component({
    selector: 'app-documents',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;
    @ViewChild('drawer') private drawer: MatDrawer;

    rowListAll: Document[] = [];
    rowList: Document[] = [];

    flashMessage: 'success' | 'created' | 'deleted' | 'error' | null = null;
    flashMessageMsg: string = '';
    isLoading: boolean = false;
    // Load the gray layer
    screenLoading: boolean = false;
    pagination: Pagination;
    searchInputControl: FormControl = new FormControl();
    selectedItem: Document | null = null;
    selectedItemForm: FormGroup;
    editMode: boolean = false;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(private _changeDetectorRef: ChangeDetectorRef, private _formBuilder: FormBuilder, private _dataService: DocumentService) { }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.load(true);
        // Create the selected object form
        this.selectedItemForm = this._formBuilder.group({
            id: [''],
            name: ['', [Validators.required]],
            min: ['1'],
            mandatory: [false]
        });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
            .subscribe(
                (query) => {
                    this.closeDetails();
                    this.isLoading = true;
                    let objResult: any;
                    if (this._sort && this._paginator) {
                        objResult = this.calculatePagination(this.rowListAll, query, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize);
                    } else {
                        objResult = this.calculatePagination(this.rowListAll, query);
                        setTimeout(() => {
                            this.inicializeTable();
                        }, 100);
                    }
                    this.rowList = objResult.list;
                    this.pagination = objResult.pagination;
                    this.isLoading = false;
                }
            );
    }


    /**
     * After view init
     */
    ngAfterViewInit(): void {
        if (this._sort && this._paginator) {
            // Set the initial sort
            this._sort.sort({
                id: 'id',
                start: 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;
                    // Close the details
                    this.closeDetails();
                });
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    inicializeTable(): void {
        if (this._sort && this._paginator) {
            // Set the initial sort
            this._sort.sort({
                id: 'name',
                start: 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;
                    // Close the details
                    this.closeDetails();
                });

            // Get admins if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).subscribe(() => {
                this.closeDetails();
                this.isLoading = true;
                // eslint-disable-next-line max-len
                let objResult: any;
                if (this._sort && this._paginator) {
                    objResult = this.calculatePagination(this.rowListAll, null, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize);
                } else {
                    objResult = this.calculatePagination(this.rowListAll);
                }
                this.rowList = objResult.list;
                this.pagination = objResult.pagination;
                this.isLoading = false;
                this._changeDetectorRef.markForCheck();
            });
        }
    }

    /**
     * Load
     */
    load(inicializeTable: boolean = false): void {
        this.screenLoading = true;
        this._dataService.getObjects().subscribe(
            (res: any) => {
                if (res.error) {
                    this.rowList = [];
                } else {
                    this.rowListAll = res.catalog;
                    const objResult: any = this.calculatePagination(res.catalog);
                    this.rowList = objResult.list;
                    this.pagination = objResult.pagination;
                }
                if (inicializeTable) {
                    setTimeout(() => {
                        this.inicializeTable();
                    }, 500);
                }
                this._changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                console.log(err);
                this.screenLoading = false;
            }
        );
    }

    /**
     * Calculate pagination by table
     */
    calculatePagination(list: Document[], search: string = '', sort: any = 'id', order: string = 'asc', page: number = 0, size: number = 10): any {
        // // Clone the admins
        let adminListClone: Document[] = cloneDeep(list);
        // Sort the adminListClone
        if (sort === 'id' || sort === 'name' || sort === 'status') {
            adminListClone.sort((a, b) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldA: any;
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldB: any;
                if (sort === 'id') {
                    fieldA = parseInt(a[sort], 10);
                    fieldB = parseInt(b[sort], 10);
                    // eslint-disable-next-line max-len
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB, undefined, { numeric: 'true' }) : fieldB.toString().localeCompare(fieldA, undefined, { numeric: 'true' });
                } else {
                    fieldA = a[sort].toString().toUpperCase();
                    fieldB = b[sort].toString().toUpperCase();
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB) : fieldB.toString().localeCompare(fieldA);
                }
            });
        }
        else {
            adminListClone.sort((a, b) => order === 'asc' ? a[sort] - b[sort] : b[sort] - a[sort]);
        }

        // If search exists...
        if (search) {
            // Filter the adminListClone
            adminListClone = adminListClone.filter(contact => contact.name && contact.name.toLowerCase().includes(search.toLowerCase()));
        }

        // Paginate - Start
        const adminListCloneLength = adminListClone.length;

        // Calculate pagination details
        const begin = page * size;
        const end = Math.min((size * (page + 1)), adminListCloneLength);
        const lastPage = Math.max(Math.ceil(adminListCloneLength / size), 1);

        // Prepare the pagination object
        let pagination = {};

        // If the requested page number is bigger than
        // the last possible page number, return null for
        // adminListClone but also send the last possible page so
        // the app can navigate to there
        if (page > lastPage) {
            adminListClone = null;
            pagination = {
                lastPage
            };
        }
        else {
            // Paginate the results by size
            adminListClone = adminListClone.slice(begin, end);

            // Prepare the pagination mock-api
            pagination = {
                length: adminListCloneLength,
                size: size,
                page: page,
                lastPage: lastPage,
                startIndex: begin,
                endIndex: end - 1
            };
        }
        return {
            list: adminListClone,
            pagination: pagination
        };
    }

    /**
     * Toggle object details
     *
     * @param item
     */
    toggleDetails(item: Document): void {
        // If the object is already selected...
        if (this.selectedItem && this.selectedItem.documentId === item.documentId) {
            // Close the details
            this.closeDetails();
            return;
        }
        this.editMode = true;
        // Set the selected object
        this.selectedItem = item;
        // Fill the form
        this.selectedItemForm.patchValue(item);
        //Open side nav
        this.drawer.toggle();
        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Close the details
     */
    closeDetails(): void {
        this.drawer.close();
        this.selectedItem = null;
    }

    create(): void {
        const item: Document = {
            documentId: '_new'
            , name: ''
            , min: ''
            , mandatory: false
        };
        this.editMode = false;
        this.rowList.unshift(item);
        this.selectedItem = item;
        this.selectedItemForm.patchValue(item);
        //Open side nav
        this.drawer.toggle();
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Update the selected item using the form data
     */
    update(): void {
        const item = this.selectedItemForm.getRawValue();
        if (!this.editMode) {
            this._dataService.create(item).subscribe(
                (res: any) => {
                    if (res.error) {
                        const msg = res.idError + '-' + res.msgError;
                        this.showFlashMessage('error', msg);
                    } else {
                        this.load(false);
                        this.showFlashMessage('created');
                        setTimeout(() => {
                            this.closeDetails();
                        }, 3000);
                    }
                },
                (err) => {
                    this.showFlashMessage('error', 'Error! '+ JSON.stringify(err));
                }
            );
        } else {
            this._dataService.update(item).subscribe(
                (res: any) => {
                    if (res.error) {
                        const msg = res.idError + '-' + res.msgError;
                        this.showFlashMessage('error', msg);
                    } else {
                        this.load(false);
                        this.showFlashMessage('success');
                        setTimeout(() => {
                            this.closeDetails();
                        }, 3000);
                    }
                },
                (err) => {
                    this.showFlashMessage('error', 'Error! '+ JSON.stringify(err));
                }
            );
        }
    }

    /**
     * Delete the selected object using the form data
     */
    delete(): void {
        const item = this.selectedItemForm.getRawValue();
        this._dataService.delete(item).subscribe(
            (res: any) => {
                if (res.error) {
                    const msg = res.idError + '-' + res.msgError;
                    this.showFlashMessage('error', msg);
                } else {
                    this.load(false);
                    this.showFlashMessage('deleted');
                    setTimeout(() => {
                        this.closeDetails();
                    }, 3000);
                }
            },
            (err) => {
                this.showFlashMessage('error', 'Error! '+ JSON.stringify(err));
            }
        );
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'created' | 'deleted' | 'error', msg: string = ''): void {
        // Show the message
        this.flashMessage = type;
        this.flashMessageMsg = msg;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {
            this.flashMessage = null;
            this.flashMessageMsg = '';
            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 3000);
    }

}
