/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { Catalog } from 'app/entities/Catalog.types';
import { DealershipEntity } from 'app/entities/dealership.entity';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import {
    FormControl,
    FormGroup,
    FormBuilder,
    Validators,
} from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RequestService } from 'app/services/Request.service';
import { CreditApp, Search } from 'app/entities/Request.types';
import { Subject, takeUntil, merge } from 'rxjs';
import { DealershipService } from 'app/services/dealership.service';
import { cloneDeep } from 'lodash-es';
import { TablePagination } from 'app/entities/InterfacePagination.interface';
import { CatalogService } from 'app/services/Catalog.service';
import { NavigationExtras, Router } from '@angular/router';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';
import { Storage } from 'aws-amplify';
import { AdditionalInfoDocument } from '../../../entities/Request.types';
import { AngularCsv } from 'angular-csv-ext/dist/Angular-csv';
import moment from 'moment';


@Component({
    selector: 'app-requests',
    templateUrl: './requests.component.html',
    styleUrls: ['./requests.component.scss'],
})
export class RequestsComponent implements OnInit {
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    // Load the gray layer
    screenLoading: boolean = false;
    pagination: TablePagination;
    searchInputControl: FormControl = new FormControl();
    selectedItem: CreditApp | null = null;
    selectedItemForm: FormGroup;
    tagsEditMode: boolean = false;
    openfilters: boolean = false;

    delearshipList: DealershipEntity[] = [];
    statusList: Catalog[] = [];
    creditAppList: CreditApp[] = [];

    rowListAll: CreditApp[] = [];
    rowList: CreditApp[] = [];

    /* General error variables */
    isError: boolean = false;
    errorMsg: string = '';

    /* filter variables */
    statusFilter: any = null;
    dealershipFilter: any = null;
    startDateFilter: any = null;
    endDateFilter: any = null;

    user: User;
    roleSales = 3;

    firstDateView = true;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _formBuilder: FormBuilder,
        private _userService: UserService,
        private _dataService: RequestService,
        private _catalogService: CatalogService,
        private _dealershipService: DealershipService,
        private _router: Router
    ) {
        const date = new Date();
        const y = date.getFullYear();
        const m = date.getMonth();

        this.startDateFilter = this.dateToYMD(new Date(y, m, 1));
        this.endDateFilter = this.dateToYMD(new Date(y, m + 1, 0));
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.user = this._userService.userLogued;

        //Test service
        this.loadStatus();
        this.loadDealership();
        this.searchCredits(true);

        // Create the selected object form
        this.selectedItemForm = this._formBuilder.group({
            id: [''],
            name: ['', [Validators.required]],
            brands: [''],
            models: [''],
            minimumDownPaymentPercentage: [''],
            minimumCashDownPayment: [''],
            maximumLTVNADAPercentage: [''],
            maximumMonthlyPayment: [''],
            minimumTermLoan: [''],
            maximumTermLoan: [''],
            maximumLoanValue: [''],
            maximumAgeVehicle: [''],
            maximumMileageVehicle: [''],
            maximumDaysFirstMonthlyPayment: [''],
            maximumDaysFirstBiWeeklyPayment: [''],
            maximumDaysFirstWeeklyPayment: [''],
            minimumReferencesCredit: [''],
            minimumEmploymentReferences: [''],
            active: [false],
        });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges.subscribe((query) => {
            this.closeDetails();
            this.isLoading = true;
            let objResult: any;
            if (this._sort && this._paginator) {
                objResult = this.calculatePagination(
                    this.rowListAll,
                    query,
                    this._sort.active,
                    this._sort.direction,
                    this._paginator.pageIndex,
                    this._paginator.pageSize
                );
            } else {
                objResult = this.calculatePagination(this.rowListAll, query);
                setTimeout(() => {
                    this.inicializeTable();
                }, 100);
            }
            this.rowList = objResult.list;
            this.pagination = objResult.pagination;
            this.isLoading = false;
            this._changeDetectorRef.markForCheck();
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    loadDealership(): void {
        this.isError = false;
        this.errorMsg = '';

        this._dealershipService.getDealership().subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                    this.delearshipList = [];
                } else {
                    this.delearshipList = res.userList;
                }
                this._changeDetectorRef.markForCheck();
            },
            (err) => {
                this.isError = true;
                this.errorMsg = JSON.stringify(err);
                this.delearshipList = [];
            }
        );
    }

    loadStatus(): void {
        this.screenLoading = true;
        this._catalogService.getCatalog(13).subscribe((res: any) => {
            if (res.error) {
                this.statusList = [];
            } else {
                this.statusList = res.catalog;
            }
            this._changeDetectorRef.markForCheck();
        });
    }

    searchCredits(inicializeTable: boolean = false): void {
        this.isError = false;
        this.errorMsg = '';

        if(!this.firstDateView){
            this.startDateFilter = moment(this.startDateFilter).toDate();
            this.endDateFilter = moment(this.endDateFilter).toDate();
            this.startDateFilter = this.dateToYMD (new Date (this.startDateFilter));
            this.endDateFilter = this.dateToYMD (new Date (this.endDateFilter));
        }

        const request: Search = {
            initDate: this.startDateFilter,
            endDate: this.endDateFilter,
        };

        if (this.statusFilter != null) {
            request.status = this.statusFilter;
        }
        if (this.dealershipFilter != null && this.dealershipFilter.length > 0) {
            request.originators = this.dealershipFilter;
        }
        this.firstDateView = false;
        this._dataService.searchCreditApp(request).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                    this.rowListAll = [];
                } else {
                    if (res.creditAppList !== undefined) {
                        this.rowListAll = res.creditAppList;
                        const objResult: any = this.calculatePagination(
                            res.creditAppList
                        );
                        this.rowList = objResult.list;
                        this.pagination = objResult.pagination;
                    }
                }
                if (inicializeTable) {
                    setTimeout(() => {
                        this.inicializeTable();
                    }, 500);
                }
                this._changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                this.isError = true;
                this.errorMsg = JSON.stringify(err);
                this.rowListAll = [];
                this.screenLoading = false;
            }
        );
    }

    inicializeTable(): void {
        if (this._sort && this._paginator) {
            // Set the initial sort
            this._sort.sort({
                id: 'userId',
                start: 'asc',
                disableClear: true,
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get dealers if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).subscribe(() => {
                this.closeDetails();
                this.isLoading = true;
                // eslint-disable-next-line max-len
                let objResult: any;
                if (this._sort && this._paginator) {
                    objResult = this.calculatePagination(
                        this.rowListAll,
                        null,
                        this._sort.active,
                        this._sort.direction,
                        this._paginator.pageIndex,
                        this._paginator.pageSize
                    );
                } else {
                    objResult = this.calculatePagination(this.rowListAll);
                }
                this.rowList = objResult.list;
                this.pagination = objResult.pagination;
                this.isLoading = false;
                this._changeDetectorRef.markForCheck();
            });
        }
    }

    /**
     * Calculate pagination by table
     */
    calculatePagination(
        list: CreditApp[],
        search: string = '',
        sort: any = 'userId',
        order: string = 'asc',
        page: number = 0,
        size: number = 10
    ): any {
        // // Clone the dealers
        let dealerListClone: CreditApp[] = cloneDeep(list);

        // Sort the dealerListClone
        if (sort === 'userId') {
            dealerListClone.sort((a, b) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldA: any;
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldB: any;
                if (sort === 'userId') {
                    fieldA = parseInt(a[sort], 10);
                    fieldB = parseInt(b[sort], 10);
                    // eslint-disable-next-line max-len
                    return order === 'asc'
                        ? fieldA
                              .toString()
                              .localeCompare(fieldB, undefined, {
                                  numeric: 'true',
                              })
                        : fieldB
                              .toString()
                              .localeCompare(fieldA, undefined, {
                                  numeric: 'true',
                              });
                } else {
                    fieldA = a[sort].toString().toUpperCase();
                    fieldB = b[sort].toString().toUpperCase();
                    return order === 'asc'
                        ? fieldA.toString().localeCompare(fieldB)
                        : fieldB.toString().localeCompare(fieldA);
                }
            });
        } else {
            dealerListClone.sort((a, b) =>
                order === 'asc' ? a[sort] - b[sort] : b[sort] - a[sort]
            );
        }

        // If search exists...
        if (search) {
            // Filter the dealerListClone
            dealerListClone = dealerListClone.filter(
                contact =>
                    contact.client_name &&
                    contact.client_name
                        .toLowerCase()
                        .includes(search.toLowerCase())
            );
        }

        // Paginate - Start
        const dealerListCloneLength = dealerListClone.length;

        // Calculate pagination details
        const begin = page * size;
        const end = Math.min(size * (page + 1), dealerListCloneLength);
        const lastPage = Math.max(Math.ceil(dealerListCloneLength / size), 1);

        // Prepare the pagination object
        let pagination = {};

        // If the requested page number is bigger than
        // the last possible page number, return null for
        // dealerListClone but also send the last possible page so
        // the app can navigate to there
        if (page > lastPage) {
            dealerListClone = null;
            pagination = {
                lastPage,
            };
        } else {
            // Paginate the results by size
            dealerListClone = dealerListClone.slice(begin, end);

            // Prepare the pagination mock-api
            pagination = {
                length: dealerListCloneLength,
                size: size,
                page: page,
                lastPage: lastPage,
                startIndex: begin,
                endIndex: end - 1,
            };
        }
        return {
            list: dealerListClone,
            pagination: pagination,
        };
    }

    /**
     * Toggle object details
     *
     * @param objectId
     */
    toggleDetails(item: CreditApp): void {
        // If the object is already selected...
        if (
            this.selectedItem &&
            this.selectedItem.credit_app_id === item.credit_app_id
        ) {
            // Close the details
            this.closeDetails();
            return;
        }

        this.selectedItem = item;
        // Fill the form
        this.selectedItemForm.patchValue(item);
        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Close the details
     */
    closeDetails(): void {
        this.selectedItem = null;
    }

    dateToYMD(date): string {
        const d = date.getDate();
        const m = date.getMonth() + 1;
        const y = date.getFullYear();
        return `${y}-${m <= 9 ? '0' + m : m}-${d <= 9 ? '0' + d : d}`;
    }

    search(): void {
        this.searchCredits(false);
    }

    cleanFilters(): void {
        this.statusFilter = null;
        this.dealershipFilter = null;
    }

    downloadCSV(): void {
        if(this.rowList && this.rowList.length > 0){
            console.log(this.rowList);
            new AngularCsv(this.formatRowsByCSV(Object.assign([], this.rowList)), 'Request_Management',{
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: false,
                showTitle: false,
                title: '',
                useBom: false,
                noDownload: false,
                // eslint-disable-next-line max-len
                headers: ['ca_status_name', 'client_id', 'client_last_name', 'client_middle_name', 'client_name', 'credit_app_approval_letter', 'credit_app_create_date', 'credit_app_id', 'credit_app_number', 'credit_app_requested_amount', 'dealer', 'documents', 'originator', 'vehicle_make', 'vehicle_model', 'vehicle_stock_number',  'vehicle_vin', 'vehicle_year'],
                useHeader: false,
                nullToEmptyString: true
            });
        }
    }

    formatRowsByCSV(rowList: CreditApp[]): any[]{
        const arrayCSV: any[] = [];
        // eslint-disable-next-line @typescript-eslint/prefer-for-of
        for (let i = 0; i < rowList.length; i++) {
            const item: any = rowList[i];
            let stringDocs = '';
            if(item.documents && item.documents.length > 0){
                // eslint-disable-next-line @typescript-eslint/prefer-for-of
                stringDocs = '';
                // eslint-disable-next-line @typescript-eslint/prefer-for-of
                for (let a = 0; a < item.documents.length; a++) {
                    stringDocs = stringDocs + item.documents[a].url + ' | ';
                }
            }

            const objItem: any = {
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'ca_status_name':(item.ca_status_name) ? item.ca_status_name : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'client_id':(item.client_id) ? item.client_id : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'client_last_name':(item.client_last_name) ? item.client_last_name : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'client_middle_name':(item.client_middle_name) ? item.client_middle_name : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'client_name':(item.client_name) ? item.client_name : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'credit_app_approval_letter':(item.credit_app_approval_letter) ? item.credit_app_approval_letter : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'credit_app_create_date':(item.credit_app_create_date) ? item.credit_app_create_date : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'credit_app_id':(item.credit_app_id) ? item.credit_app_id : null ,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'credit_app_number':(item.credit_app_number) ? item.credit_app_number : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'credit_app_requested_amount':(item.credit_app_requested_amount) ? item.credit_app_requested_amount : null,
                'dealer':(item.dealer) ? item.dealer : null,
                'documents':(stringDocs) ? stringDocs : null,
                'originator':(item.originator) ? item.originator : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'vehicle_make':(item.vehicle_make) ? item.vehicle_make : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'vehicle_model':(item.vehicle_model) ? item.vehicle_model : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'vehicle_stock_number':(item.vehicle_stock_number) ? item.vehicle_stock_number : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'vehicle_vin':(item.vehicle_vin) ? item.vehicle_vin : null,
                // eslint-disable-next-line @typescript-eslint/naming-convention
                'vehicle_year':(item.vehicle_year) ? item.vehicle_year : null
            };

            arrayCSV.push(objItem);
        }
        return arrayCSV;
    }

    openDetails(credit): void {
        let permision = 'read';
        if (this.user.roleId === this.roleSales) {
            if (credit.ca_status_name === 'Pending') {
                permision = 'write';
            } else {
                permision = 'read';
            }
        } else {
            permision = 'read';
        }
        this._router.navigate(['credits/credit'], { queryParams: { id: credit.credit_app_id, permision: permision }, skipLocationChange: true},);
    }

    async downloadFormat(): Promise<void> {
        const result = await Storage.get(
            'downloadables/CreditApplication.pdf',
            { download: true }
        );
        this.downloadBlob(result.Body, 'CreditApplication.pdf');
    }

    async downloadFile(document: AdditionalInfoDocument): Promise<void> {
        const result = await Storage.get('documents/' + document.url, {
            download: true,
        });
        this.downloadBlob(result.Body, document.url);
    }

    async downloadPreapprovalLetter(document: string): Promise<void> {
        const result = await Storage.get(document, { download: true });
        this.downloadBlob(result.Body, document.split('/')[1]);
    }

    downloadBlob(blob, filename): HTMLAnchorElement {
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = filename || 'download';
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        const clickHandler = () => {
            setTimeout(() => {
                URL.revokeObjectURL(url);
                a.removeEventListener('click', clickHandler);
            }, 150);
        };
        a.addEventListener('click', clickHandler, false);
        a.click();
        return a;
    }
}
