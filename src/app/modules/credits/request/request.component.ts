/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable max-len */
/* eslint-disable guard-for-in */
/* eslint-disable id-blacklist */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { Subject, takeUntil } from 'rxjs';
import { FuseMediaWatcherService } from '@fuse/services/media-watcher';
import { AdditionalInfo, AdditionalInfoVehicle, AdditionalInfoAddress, AdditionalInfoDocument, Request, AdditionalInfoEmployment, AdditionalInfoPersonal } from 'app/entities/Request.types';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Client, GeneralInfoClient } from 'app/entities/Client.types';
import { ClientService } from 'app/services/Client.service';
import { CatalogService } from 'app/services/Catalog.service';
import { RequestService } from 'app/services/Request.service';
import { Storage } from 'aws-amplify';
import { cloneDeep } from 'lodash-es';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import moment, { Moment } from 'moment';
import { TranslocoService } from '@ngneat/transloco';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.entity';
import { truncate } from 'fs';

@Component({
    selector: 'app-request',
    templateUrl: './request.component.html',
    styleUrls: ['./request.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestComponent implements OnInit, OnDestroy {

    @ViewChild('drawerDocument') drawerDocument: MatDrawer;
    @ViewChild('itzFileUpload', { read: ElementRef }) itzFileUpload: ElementRef;
    @ViewChild('drawer') drawer: MatDrawer;

    state: any;

    drawerMode: 'over' | 'side' = 'side';
    drawerOpened: boolean = true;

    // Load the gray layer
    screenLoading: boolean = false;

    // navigation variables
    panels: any[] = [];
    selectedPanel: string = 'search';
    nextPanel: string = 'account';

    selectedItem: Request | null = null;
    showCoApplicant: boolean = false;

    requestCreditForm: FormGroup;
    documentForm: FormGroup;
    personalReferencesForm: FormGroup;

    searchInputControl: FormControl = new FormControl();

    //show form vehicle by VIN valid
    vinIsValid: boolean = false;

    //Configuration person reference
    minPersonalReferences = 0;
    minWorkPersonalReferences = 0;

    //catalogs
    clientsList: Client[] = [];
    clientsListAll: Client[] = [];
    clientsListBackup: Client[] = [];
    statesList: any[] = [];
    residencesList: any[] = [];
    identificationsList: any[] = [];
    incomeFrequenciesList: any[] = [];
    maritialList: any[] = [];
    fuelTypesList: any[] = [];
    typeTitleList: any[] = [];
    accountTypeList: any[] = [];
    vehicleTypeList: any[] = [];
    documentsList: any[] = [];

    selectedItemClient: Client = null;
    selectedItemCoApplicant: Client = null;

    /* Upload Files variables */
    isEditFile: boolean = false;
    isFileApplicant: boolean = false;
    typeFile: boolean = false;
    fileToUpload: File | null = null;
    selectedFileApplicant: any = null;
    selectedFileCoApplicant: any = null;

    /* General error variables */
    isError: boolean = false;
    errorMsg: string = '';

    documentUploadList: any[] = [];
    isUpdateCreditApp = false;
    preApprovedResult = null;
    isAllowUpdatePermission = true;

    needShowBackButton: boolean = false;
    needShowNextButton: boolean = false;

    hasSecondApplicantAddress: boolean = false;
    hasSecondcoApplicantAddress: boolean = false;

    hasSecondApplicantEmployment: boolean = false;
    hasSecondCoApplicantEmployment: boolean = false;

    /** General info client */
    generalInfoClient: GeneralInfoClient = {};

    userLogued: User;
    mandatoryPersonalReferences = 1;
    arrayFormPersonalRederences: number[] = [1];
    minmandatoryPersonalReferences = 1;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(private _changeDetectorRef: ChangeDetectorRef
        , private _fuseMediaWatcherService: FuseMediaWatcherService
        , private _formBuilder: FormBuilder
        , private _route: ActivatedRoute
        , private _translocoService: TranslocoService
        , private _clientService: ClientService
        , private _catalogService: CatalogService
        , private _requestService: RequestService
        , private _userService: UserService
        , private _router: Router) { }
    /**
     * On init
     */
    ngOnInit(): void {
        // Setup available panels
        this.setupPanels();
        this.controlNavigationButton('search');
        // Create the form
        this.requestCreditForm = this._formBuilder.group({
            identification: ['', Validators.required],
            expiration: ['', Validators.required],
            identificationtype: ['', Validators.required],
            residencetype: ['', Validators.required],
            howyears: [''],
            maritialstatus: [''],
            dependants: [''],
            incomefrequency: ['', Validators.required],
            monthlyincome: ['', Validators.required],
            monthlyutilities: [''],
            monthlyrentmortgage: [''],
            bankruptcyintend: [false, Validators.required],
            bankrupt: [false, Validators.required],
            bankruptcydate: [''],
            bankruptreason: [''],
            repossession: [false, Validators.required],
            repossessiondate: [''],
            repossessionreason: [''],
            coidentification: [''],
            coexpiration: [''],
            coidentificationtype: [''],
            coresidencetype: [''],
            cohowyears: [''],
            comaritialstatus: [''],
            codependants: [''],
            coincomefrequency: [''],
            comonthlyincome: [''],
            comonthlyutilities: [''],
            comonthlyrentmortgage: [''],
            cobankruptcyintend: [false],
            cobankruptcydate: [''],
            cobankruptreason: [''],
            cobankrupt: [false],
            corepossession: [false],
            corepossessiondate: [''],
            corepossessionreason: [''],
            vin: ['', Validators.required],
            make: ['', Validators.required],
            model: ['', Validators.required],
            year: ['', Validators.required],
            mileage: ['', Validators.required],
            sale: ['', Validators.required],
            stock: [''],
            cleantitle: ['', Validators.required],
            fuel: ['', Validators.required],
            rideShare: [false],
            nadaTradeInAvg: ['', Validators.required],
            addressId: ['0', Validators.required],
            street: ['', Validators.required],
            extnumber: [''],
            intnumber: [''],
            state: ['', Validators.required],
            city: ['', Validators.required],
            zipcode: ['', Validators.required],
            old: [false],
            yearliving: ['', Validators.required],
            monthliving: ['', Validators.required],
            secondaddressId: ['0'],
            secondstreet: ['', Validators.required],
            secondextnumber: [''],
            secondintnumber: [''],
            secondstate: ['', Validators.required],
            secondcity: ['', Validators.required],
            secondzipcode: ['', Validators.required],
            secondold: [true],
            secondyearliving: ['', Validators.required],
            secondmonthliving: ['', Validators.required],
            coaddressId: ['0'],
            costreet: ['', Validators.required],
            coextnumber: [''],
            cointnumber: [''],
            costate: ['', Validators.required],
            cocity: ['', Validators.required],
            cozipcode: ['', Validators.required],
            coold: [false],
            coyearliving: ['', Validators.required],
            comonthliving: ['', Validators.required],
            secondcoaddressId: ['0'],
            secondcostreet: ['', Validators.required],
            secondcoextnumber: [''],
            secondcointnumber: [''],
            secondcostate: ['', Validators.required],
            secondcocity: ['', Validators.required],
            secondcozipcode: ['', Validators.required],
            secondcoold: [true],
            secondcoyearliving: ['', Validators.required],
            secondcomonthliving: ['', Validators.required],
            bank: ['', Validators.required],
            branch: [''],
            accountType: [0],
            accountNumber: [''],
            employmentId: ['0'],
            employmentName: ['', Validators.required],
            employmentPosition: ['', Validators.required],
            employmentPhone: ['', [Validators.required]],
            employmentYear: ['', [Validators.required, Validators.min(0), Validators.max(99)]],
            employmentMonth: ['', [Validators.required, Validators.min(0), Validators.max(11)]],
            employmentold: [false],
            secondemploymentId: ['0'],
            secondemploymentName: ['', Validators.required],
            secondemploymentPosition: ['', Validators.required],
            secondemploymentPhone: ['', [Validators.required]],
            secondemploymentYear: ['', [Validators.required, Validators.min(0), Validators.max(99)]],
            secondemploymentMonth: ['', [Validators.required, Validators.min(0), Validators.max(11)]],
            secondemploymentold: [true],
            coemploymentId: ['0'],
            coemploymentName: ['', Validators.required],
            coemploymentPosition: ['', Validators.required],
            coemploymentPhone: ['', [Validators.required]],
            coemploymentYear: ['', [Validators.required, Validators.min(0), Validators.max(99)]],
            coemploymentMonth: ['', [Validators.required, Validators.min(0), Validators.max(11)]],
            coemploymentold: [false],
            secondcoemploymentId: ['0'],
            secondcoemploymentName: ['', Validators.required],
            secondcoemploymentPosition: ['', Validators.required],
            secondcoemploymentPhone: ['',[Validators.required]],
            secondcoemploymentYear: ['', [Validators.required, Validators.min(0), Validators.max(99)]],
            secondcoemploymentMonth: ['', [Validators.required, Validators.min(0), Validators.max(11)]],
            secondcoemploymentold: [true],
            requestAmount: ['', Validators.required],
            requestDownpayment: ['', Validators.required],
            requestPeriods: ['', Validators.required],
            requestLoan: ['', Validators.required],
            requestCar: ['', Validators.required],
            requestAPR: ['', [Validators.required, Validators.max(100), Validators.min(0)]],
            firstpayment: ['', Validators.required],
            scheduledContract: ['', Validators.required]
        });

        this.documentForm = this._formBuilder.group({
            path: [''],
            documentId: [''],
            document: ['']
        });

        //Dinamyc form personal reference
        this.personalReferencesForm = this._formBuilder.group({
        });

        this.userLogued = this._userService.userLogued;
        this.minPersonalReferences = Number((this.userLogued.minPersonalReferences)?this.userLogued.minPersonalReferences : 0);
        this.minWorkPersonalReferences = Number((this.userLogued.minWorkReferences) ? this.userLogued.minWorkReferences : 0);

        this.manageMandatoryFormReference();

        // Subscribe to media changes
        this._fuseMediaWatcherService.onMediaChange$.pipe(takeUntil(this._unsubscribeAll)).subscribe(({ matchingAliases }) => {
            // Set the drawerMode and drawerOpened
            if (matchingAliases.includes('lg')) {
                this.drawerMode = 'side';
                this.drawerOpened = true;
            }
            else {
                this.drawerMode = 'over';
                this.drawerOpened = false;
            }
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges.subscribe((query) => {
            const listClone: Client[] = cloneDeep(this.clientsListAll);
            this.clientsList = listClone.filter(contact => ((contact.name && contact.name.toLowerCase().includes(query.toLowerCase()))
                || (contact.id && ('' + contact.id).includes(query.toLowerCase()))));
            // this.clientsList.sort((a, b) => a.id.toString().localeCompare(b.id.toString()));
            this.clientsList.sort((a, b) => b.id - a.id);

            this._changeDetectorRef.markForCheck();
        }
        );

        this._route.queryParams.subscribe((params) => {
            if (params.permision !== 'read' || params.permision == null) {
                this.preApprovedResult = null; // reset results
            }
            if (params.permision != null) {
                this.isAllowUpdatePermission = (params.permision !== 'read');
            } else {
                this.isAllowUpdatePermission = true;
            }
            if (params.id != null) {
                this.getCreditAppData(params.id);
                this.loadCatalogs(false);
                this.requestCreditForm.controls['cleantitle'].setValue(1);
            } else {
                this.loadCatalogs(true);
                this.isUpdateCreditApp = false;
                this.setupPanels();
                this.selectedItem = null;
                this.selectedItemClient = null;
                this.selectedItemCoApplicant = null;
                this.generalInfoClient = null;
                this.showCoApplicant = false;
                this.needShowBackButton = false;
                this.needShowNextButton = false;
                this.hasSecondApplicantAddress = false;
                this.hasSecondcoApplicantAddress = false;
                this.hasSecondApplicantEmployment = false;
                this.hasSecondCoApplicantEmployment = false;
                this.selectedPanel = 'search';
                this.nextPanel = 'account';
                this.requestCreditForm.reset();
                this.requestCreditForm.controls['bankruptcyintend'].setValue(false);
                this.requestCreditForm.controls['bankrupt'].setValue(false);
                this.requestCreditForm.controls['repossession'].setValue(false);
                this.requestCreditForm.controls['cobankruptcyintend'].setValue(false);
                this.requestCreditForm.controls['cobankrupt'].setValue(false);
                this.requestCreditForm.controls['corepossession'].setValue(false);
                this.requestCreditForm.controls['cleantitle'].setValue(1);
                this.requestCreditForm.controls['rideShare'].setValue(false);
                this.requestCreditForm.controls['old'].setValue(false);
                this.requestCreditForm.controls['secondold'].setValue(true);
                this.requestCreditForm.controls['coold'].setValue(false);
                this.requestCreditForm.controls['secondcoold'].setValue(true);
                this.requestCreditForm.controls['employmentold'].setValue(false);
                this.requestCreditForm.controls['secondemploymentold'].setValue(true);
                this.requestCreditForm.controls['coemploymentold'].setValue(false);
                this.requestCreditForm.controls['secondcoemploymentold'].setValue(true);
                this.requestCreditForm.controls['accountTypeId'].setValue(0);
                this.vinIsValid = false;
                this.personalReferencesForm.reset();
                this.manageMandatoryFormReference();
            }

            for (const field in this.requestCreditForm.controls) {
                if (!this.isAllowUpdatePermission) {
                    this.requestCreditForm.get(field).disable();
                } else {
                    this.requestCreditForm.get(field).enable();
                }
            }
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Navigate to the panel
     *
     * @param panel
     */
    goToPanel(panel: string): void {
        if (!this.isAllowUpdatePermission && panel !== 'preapproval') {
            this.selectedPanel = panel;
        }
    }

    /**
     * Get the details of the panel
     *
     * @param id
     */
    getPanelInfo(id: string): any {
        return this.panels.find(panel => panel.id === id);
    }

    loadCatalogs(greyLayer: boolean): void {
        if (greyLayer) {
             this.screenLoading = true;
        }
        this._clientService.getObjects().subscribe(
            (res: any) => {
                if (res.error) {
                    this.clientsList = [];
                    this.clientsListAll = [];
                } else {
                    this.clientsList = res.clientList.sort((a, b) => b.id - a.id);
                    this.clientsListAll = res.clientList;
                    for (let i = 0; i < res.clientList.length; i++){
                        const client = res.clientList[i];
                        this.clientsListBackup.push(client);
                    }
                    // this.clientsList.sort((a, b) => a.id.toString().localeCompare(b.id.toString()));
                }
                this._changeDetectorRef.markForCheck();
                if (greyLayer) {
                    this.screenLoading = false;
               }
            },
            (err) => {
                this.isError = true;
                this.errorMsg = this._translocoService.translate('common.error');
                if (greyLayer) {
                    this.screenLoading = false;
               }
            }
        );

        this._catalogService.getCatalog(1).subscribe(
            (res: any) => {
                if (res.error) {
                    this.statesList = [];
                } else {
                    this.statesList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
            }
        );

        this._catalogService.getCatalog(5).subscribe(
            (res: any) => {
                if (res.error) {
                    this.documentsList = [];
                } else {
                    this.documentsList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
            }
        );

        this._catalogService.getCatalog(6).subscribe(
            (res: any) => {
                if (res.error) {
                    this.residencesList = [];
                } else {
                    this.residencesList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
            }
        );

        this._catalogService.getCatalog(8).subscribe(
            (res: any) => {
                if (res.error) {
                    this.incomeFrequenciesList = [];
                } else {
                    this.incomeFrequenciesList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
            }
        );

        this._catalogService.getCatalog(9).subscribe(
            (res: any) => {
                if (res.error) {
                    this.identificationsList = [];
                } else {
                    this.identificationsList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
            }
        );

        this._catalogService.getCatalog(10).subscribe(
            (res: any) => {
                if (res.error) {
                    this.fuelTypesList = [];
                } else {
                    this.fuelTypesList = res.catalog;
                    console.log(this.fuelTypesList);
                }
                this._changeDetectorRef.markForCheck();
            }
        );

        this._catalogService.getCatalog(15).subscribe(
            (res: any) => {
                if (res.error) {
                    this.typeTitleList = [];
                } else {
                    this.typeTitleList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
            }
        );

        this._catalogService.getCatalog(14).subscribe(
            (res: any) => {
                if (res.error) {
                    this.accountTypeList = [];
                } else {
                    this.accountTypeList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
            }
        );

        this._catalogService.getCatalog(11).subscribe(
            (res: any) => {
                if (res.error) {
                    this.maritialList = [];
                } else {
                    this.maritialList = res.catalog;
                }
                this._changeDetectorRef.markForCheck();
            }
        );
    }

    clearFormCoApplicant(): void {
        this.requestCreditForm.get('coidentification').patchValue(null);
        this.requestCreditForm.get('coexpiration').patchValue(null);
        this.requestCreditForm.get('coidentificationtype').patchValue(null);
        this.requestCreditForm.get('coresidencetype').patchValue(null);
        this.requestCreditForm.get('coincomefrequency').patchValue(null);
        this.requestCreditForm.get('comonthlyincome').patchValue(null);
        this.requestCreditForm.get('comonthlyutilities').patchValue(null);
        this.requestCreditForm.get('comonthlyrentmortgage').patchValue(null);
        this.requestCreditForm.get('costreet').patchValue(null);
        this.requestCreditForm.get('coextnumber').patchValue(null);
        this.requestCreditForm.get('costate').patchValue(null);
        this.requestCreditForm.get('cocity').patchValue(null);
        this.requestCreditForm.get('cozipcode').patchValue(null);
        this.requestCreditForm.get('coold').patchValue(null);
        this.requestCreditForm.get('coyearliving').patchValue(null);
        this.requestCreditForm.get('comonthliving').patchValue(null);
    }

    addMandatoryFormCoApplicant(): void {
        this.requestCreditForm.controls['coidentification'].setValidators([Validators.required]);
        this.requestCreditForm.controls['coexpiration'].setValidators([Validators.required]);
        this.requestCreditForm.controls['coidentificationtype'].setValidators([Validators.required]);
        this.requestCreditForm.controls['coresidencetype'].setValidators([Validators.required]);
        this.requestCreditForm.controls['coincomefrequency'].setValidators([Validators.required]);
        this.requestCreditForm.controls['comonthlyincome'].setValidators([Validators.required]);
    }

    deleteMandatoryFormCoApplicant(): void {
        this.requestCreditForm.controls['coidentification'].clearValidators();
        this.requestCreditForm.controls['coexpiration'].clearValidators();
        this.requestCreditForm.controls['coidentificationtype'].clearValidators();
        this.requestCreditForm.controls['coresidencetype'].clearValidators();
        this.requestCreditForm.controls['coincomefrequency'].clearValidators();
        this.requestCreditForm.controls['comonthlyincome'].clearValidators();
        this.requestCreditForm.controls['comonthlyutilities'].clearValidators();
        this.requestCreditForm.controls['comonthlyrentmortgage'].clearValidators();
    }

    updateValdatorsCoApplicant(): void {
        this.requestCreditForm.controls['coidentification'].updateValueAndValidity();
        this.requestCreditForm.controls['coexpiration'].updateValueAndValidity();
        this.requestCreditForm.controls['coidentificationtype'].updateValueAndValidity();
        this.requestCreditForm.controls['coresidencetype'].updateValueAndValidity();
        this.requestCreditForm.controls['coincomefrequency'].updateValueAndValidity();
        this.requestCreditForm.controls['comonthlyincome'].updateValueAndValidity();
        this.requestCreditForm.controls['comonthlyutilities'].updateValueAndValidity();
        this.requestCreditForm.controls['comonthlyrentmortgage'].updateValueAndValidity();
    }

    addcoapplicant(): void {
        this.clientsList = [];
        for (let i = 0; i < this.clientsListBackup.length; i++) {
            const client = this.clientsListBackup[i];
            if (this.selectedItemClient.id !== client.id) {
                this.clientsList.push(client);
            }
        }

        this.showCoApplicant = !this.showCoApplicant;
        this.clearFormCoApplicant();
        this.addMandatoryFormCoApplicant();
        this.updateValdatorsCoApplicant();
    }

    deletecoapplicant(): void {
        this.selectedItemCoApplicant = null;
        this.showCoApplicant = !this.showCoApplicant;
        this.clearFormCoApplicant();
        this.deleteMandatoryFormCoApplicant();
        this.updateValdatorsCoApplicant();

        let coapplicantId = 0;
        this.selectedItem.additionalInfo.forEach((additional: AdditionalInfo) => {
            if (additional.coApplicant) {
                coapplicantId = additional.id;
            }
        });
        if(coapplicantId !== 0) {
            this.screenLoading = true;
            this._requestService.updateStatusService(this.selectedItem, 1, coapplicantId).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isError = true;
                        this.errorMsg = res.idError + '-' + res.msgError;
                    }
                    this._changeDetectorRef.markForCheck();
                    this.screenLoading = false;
                },
                (err) => {
                    this.isError = true;
                    this.errorMsg = this._translocoService.translate('common.error');
                    this.screenLoading = false;
                }
            );
        }
    }

    uploadAplicantDocument(isApplicant: boolean): void {
        this.isFileApplicant = isApplicant;
        this.isEditFile = false;
        this.drawerDocument.toggle();
        this._changeDetectorRef.markForCheck();
        this.documentForm.get('documentId').patchValue('');
    }

    updateAplicantDocument(item: any, isApplicant: boolean): void {
        this.selectedFileApplicant = item;
        this.isFileApplicant = isApplicant;
        this.isEditFile = true;
        this.documentForm.get('document').patchValue(item.documentId);
        this.documentForm.get('path').patchValue(item.url);
        //this.drawerDocument.toggle();
        this._changeDetectorRef.markForCheck();
    }

    closeDocumentUpload(): void {
        this.fileToUpload = null;
        this.typeFile = false;
        this.documentForm.get('document').patchValue('');
        this.documentForm.get('path').patchValue('');
        this.drawerDocument.close();
        this._changeDetectorRef.markForCheck();
    }

    searchDocumentUpload(): void {
        this.itzFileUpload.nativeElement.click();
    }

    handleDocumentUpload(event: any): void {
        const files: FileList = event.target.files;
        this.fileToUpload = files.item(0);
        this.documentForm.get('path').patchValue(this.fileToUpload.name);
        event.target.value = ''; // Reset the event to upload files with the same name
    }

    async downloadFile(item): Promise<void> {
        const result = await Storage.get('documents/'+item.url, { download: true });
        this.downloadBlob(result.Body, item.url);
    }

    async deleteFile(item, type: number): Promise<void> {
        if (type === 1) {
            this.selectedItem.documents = this.selectedItem.documents.filter((document: any) => document.id !== item.id);
        } else {
            this.selectedItem.codocuments = this.selectedItem.codocuments.filter((document: any) => document.id !== item.id);
        }
        this.screenLoading = true;
        this._requestService.updateDocumentStatus(item).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                }
                this._changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                this.isError = true;
                this.errorMsg = this._translocoService.translate('common.error');
                this.screenLoading = false;
            }
        );
    }

    downloadBlob(blob, filename): HTMLAnchorElement {
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = filename || 'download';
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        const clickHandler = () => {
            setTimeout(() => {
                URL.revokeObjectURL(url);
                a.removeEventListener('click', clickHandler);
            }, 150);
        };
        a.addEventListener('click', clickHandler, false);
        a.click();
        return a;
    }

    dataDocument(item: any): void {
        this.documentForm.get('document').patchValue(item.name); // Get the document type name
        this.typeFile = true;

    }

    async uploadDocument(): Promise<void> {
        if (this.documentForm.invalid) {
            return;
        }
        const ext = this.fileToUpload.name.split('.').pop();
        const clientId = this.isFileApplicant ? this.selectedItem.applicant.id : this.selectedItem.coApplicant.id;
        const documentId = this.documentForm.value.documentId;
        const document = this.documentForm.value.document;
        const keyFileName = this.selectedItem.creditAppNumber + '_'+ document +'_' + moment().valueOf()  + '.' + ext;
        this.screenLoading = true;
        Storage.put('documents/'+keyFileName, this.fileToUpload, {
            resumable: true,
            completeCallback: (event) => {
                const documentInt: AdditionalInfoDocument = {
                    documentId: documentId,
                    clientId: clientId,
                    url: keyFileName
                };
                this.checkAdditionalInfoDocument(documentInt);
            },
            progressCallback: (progress) => {
                console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
                this.screenLoading = false;
            },
            errorCallback: (err) => {
                console.error('Unexpected error while uploading', err);
                this.screenLoading = false;
            }
        });
    }

    checkInitiatedRequest(): void {
        if (this.selectedItem == null) {
            this.selectedItem = {
                id: '',
                creditAppId: 0,
                creditAppNumber: 0,
                applicant: null,
                coApplicant: null,
                requestId: '',
                client: '',
                name: '',
                date: '',
                vin: '',
                make: '',
                model: '',
                year: '',
                type: '',
                aval: '',
                additionalInfo: [],
                vehicle: null,
                address: [],
                documents: [],
                codocuments: [],
                personals: [],
                active: true,
                statusId: 1
            };
        }
    }

    /*
    * function checkInitiatedRequest
    * This method is used to validate that the data in the additional information section is complete.
    */
    checkAdditionalInfo(): boolean {

        let validForms = false;
        if(this.requestCreditForm.value.bankrupt
            && (this.requestCreditForm.value.bankruptcydate === null || this.requestCreditForm.value.bankruptcydate === ''
            && this.requestCreditForm.value.bankruptreason === null || this.requestCreditForm.value.bankruptreason === '')
            ){
            validForms = false;
            return validForms;
        }
        if(this.requestCreditForm.value.bankruptcyintend
            && (this.requestCreditForm.value.bankruptcydate === null || this.requestCreditForm.value.bankruptcydate === ''
            && this.requestCreditForm.value.bankruptreason === null || this.requestCreditForm.value.bankruptreason === '')
            ){
            validForms = false;
            return validForms;
        }
        if(this.requestCreditForm.value.repossession
            && (this.requestCreditForm.value.repossessiondate === null || this.requestCreditForm.value.repossessiondate === ''
            && this.requestCreditForm.value.repossessionreason === null || this.requestCreditForm.value.repossessionreason === '')
            ){
            validForms = false;
            return validForms;
        }

        if (this.selectedItem.applicant != null
            && this.requestCreditForm.value.identificationtype !== null && this.requestCreditForm.value.identificationtype !== ''
            && this.requestCreditForm.value.identification !== null && this.requestCreditForm.value.identification !== ''
            && this.requestCreditForm.value.expiration !== null && this.requestCreditForm.value.expiration !== ''
            && this.requestCreditForm.value.residencetype !== null && this.requestCreditForm.value.residencetype !== ''
            && this.requestCreditForm.value.incomefrequency !== null && this.requestCreditForm.value.incomefrequency !== ''
            && this.requestCreditForm.value.monthlyincome !== null && this.requestCreditForm.value.monthlyincome !== ''
            ) {
            validForms = true;
        } else {
            validForms = false;
            return validForms;
        }
        if (validForms) {
            if (this.selectedItemCoApplicant != null) {

                if(this.requestCreditForm.value.cobankrupt
                    && ( this.requestCreditForm.value.cobankruptcydate === null || this.requestCreditForm.value.cobankruptcydate === ''
                    && this.requestCreditForm.value.cobankruptreason === null || this.requestCreditForm.value.cobankruptreason === '')
                    ){
                    validForms = false;
                    return validForms;
                }
                if(this.requestCreditForm.value.cobankruptcyintend
                    && ( this.requestCreditForm.value.cobankruptcydate === null || this.requestCreditForm.value.cobankruptcydate === ''
                    && this.requestCreditForm.value.cobankruptreason === null || this.requestCreditForm.value.cobankruptreason === '' )
                    ){
                    validForms = false;
                    return validForms;
                }
                if(this.requestCreditForm.value.corepossession
                    && ( this.requestCreditForm.value.corepossessiondate === null || this.requestCreditForm.value.corepossessiondate === ''
                    && this.requestCreditForm.value.corepossessionreason === null || this.requestCreditForm.value.corepossessionreason === '' )
                    ){
                    validForms = false;
                    return validForms;
                }

                if (this.requestCreditForm.value.coidentificationtype !== null && this.requestCreditForm.value.coidentificationtype !== ''
                    && this.requestCreditForm.value.coidentification !== null && this.requestCreditForm.value.coidentification !== ''
                    && this.requestCreditForm.value.coexpiration !== null && this.requestCreditForm.value.coexpiration !== ''
                    && this.requestCreditForm.value.coresidencetype !== null && this.requestCreditForm.value.coresidencetype !== ''
                    && this.requestCreditForm.value.coincomefrequency !== null && this.requestCreditForm.value.coincomefrequency !== ''
                    && this.requestCreditForm.value.comonthlyincome !== null && this.requestCreditForm.value.comonthlyincome !== ''
                    ) {
                    validForms = true;
                } else {
                    validForms = false;
                    return validForms;
                }
            } else {
                validForms = true;
            }
        }
        return validForms;
    }

    /**
     * Function name selectCustomer
     * function to select a client and continue with the process
     */
    selectCustomer(client: Client, index: number): void {
        this.selectedItemClient = client;
        this.clientsList.splice(index, 1);
        this.onClickNext(this.nextPanel);
    }
    selectCoCustomer(coClient: Client): void {
        this.selectedItemCoApplicant = coClient;
        //this.onClickNext(this.nextPanel);
    }

    /*
    * function updateAdditionalInfo
    * This method is used to update the data from the additional information section for the current credit application.
    */
    updateAdditionalInfo(): void {

        this.isError = false;
        this.errorMsg = '';

        let aiaId = 0;
        let aicId = 0;
        this.selectedItem.additionalInfo.forEach((additional: AdditionalInfo) => {
            if (additional.coApplicant) {
                aicId = additional.id;
            } else {
                aiaId = additional.id;
            }
        });

        const validForms = this.checkAdditionalInfo();
        if (validForms) {
            const adds: AdditionalInfo[] = [];
            const additionalInfoApplicant: AdditionalInfo = {
                id: aiaId,
                clientId: this.selectedItemClient.id,
                idType: this.requestCreditForm.value.identificationtype,
                idNumber: this.requestCreditForm.value.identification,
                idExpDate: this.requestCreditForm.value.expiration?.format('YYYY-MM-DD'),
                residenceId: this.requestCreditForm.value.residencetype,
                yearsResidence: this.requestCreditForm.value.howyears,
                fillingId: this.requestCreditForm.value.maritialstatus,
                dependantsNumber: this.requestCreditForm.value.dependants,
                incomeFrecuencyId: this.requestCreditForm.value.incomefrequency,
                avgIncome: this.requestCreditForm.value.monthlyincome,
                avgRent: this.requestCreditForm.value.monthlyrentmortgage,
                avgUtilities: this.requestCreditForm.value.monthlyutilities,
                bankrupcy: this.requestCreditForm.value.bankrupt,
                bankrupcyIntend: this.requestCreditForm.value.bankruptcyintend,
                bankrupcyDate: this.requestCreditForm.value.bankruptcydate?.format('YYYY-MM-DD'),
                bankrupcyInfo: this.requestCreditForm.value.bankruptreason,
                vehicleRepossesion: this.requestCreditForm.value.repossession,
                vehicleRepossesionDate: this.requestCreditForm.value.repossessiondate?.format('YYYY-MM-DD'),
                vehicleRepossesionInfo: this.requestCreditForm.value.repossessionreason,
                coApplicant: false
            };
            adds.push(additionalInfoApplicant);

            if (this.selectedItemCoApplicant != null && aicId != null && validForms) {
                this.selectedItem.coApplicant = this.selectedItemCoApplicant;
                const additionalInfoCoApplicant: AdditionalInfo = {
                    id: aicId,
                    clientId: this.selectedItem.coApplicant.id,
                    idType: this.requestCreditForm.value.coidentificationtype,
                    idNumber: this.requestCreditForm.value.coidentification,
                    idExpDate: this.requestCreditForm.value.coexpiration?.format('YYYY-MM-DD'),
                    residenceId: this.requestCreditForm.value.coresidencetype,
                    yearsResidence: this.requestCreditForm.value.cohowyears,
                    fillingId: this.requestCreditForm.value.comaritialstatus,
                    dependantsNumber: this.requestCreditForm.value.codependants,
                    incomeFrecuencyId: this.requestCreditForm.value.coincomefrequency,
                    avgIncome: this.requestCreditForm.value.comonthlyincome,
                    avgRent: this.requestCreditForm.value.comonthlyrentmortgage,
                    avgUtilities: this.requestCreditForm.value.comonthlyutilities,
                    bankrupcy: this.requestCreditForm.value.cobankrupt,
                    bankrupcyIntend: this.requestCreditForm.value.cobankruptcyintend,
                    bankrupcyDate: this.requestCreditForm.value.cobankruptcydate?.format('YYYY-MM-DD'),
                    bankrupcyInfo: this.requestCreditForm.value.cobankruptreason,
                    vehicleRepossesion: this.requestCreditForm.value.corepossession,
                    vehicleRepossesionDate: this.requestCreditForm.value.corepossessiondate?.format('YYYY-MM-DD'),
                    vehicleRepossesionInfo: this.requestCreditForm.value.corepossessionreason,
                    coApplicant: true
                };
                adds.push(additionalInfoCoApplicant);

            } else {
                this.selectedItem.coApplicant = null;
            }
            this.selectedItem.additionalInfo = adds;

            this.serviceAdditionalInfo();
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function updateAdditionalInfo
    * This method is used to communicate the data of the additional information section to the server.
    */
    serviceAdditionalInfo(): void {
		this.screenLoading = true;
        this.isError = false;
        this._requestService.additionalInfo(this.selectedItem).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                } else {
                    this.selectedItem.creditAppId = res.creditAppId;
                    this.selectedItem.additionalInfo = res.additionalInfo;
                    this.selectedItem.creditAppNumber = res.creditAppNumber;
                    this.controlNavigationButton('vehicle');
                }
                this._changeDetectorRef.markForCheck();
				this.screenLoading = false;
            },
            (err) => {
                this.isError = true;
                this.errorMsg = this._translocoService.translate('common.error');
                this.screenLoading = false;
            }
        );
    }

    /*
    * function checkAdditionalInfoVehicle
    * This method is used to validate that the data in the vehicle section is complete.
    */
    checkAdditionalInfoVehicle(): boolean {
        let validForms = false;
        if (this.selectedItem.applicant != null && this.selectedItem.creditAppId != null
            && this.requestCreditForm.value.vin !== null && this.requestCreditForm.value.vin !== ''
            && this.requestCreditForm.value.make !== null && this.requestCreditForm.value.make !== ''
            && this.requestCreditForm.value.model !== null && this.requestCreditForm.value.model !== ''
            && this.requestCreditForm.value.year !== null && this.requestCreditForm.value.year !== ''
            && this.requestCreditForm.value.mileage !== null && this.requestCreditForm.value.mileage !== ''
            && this.requestCreditForm.value.sale !== null && this.requestCreditForm.value.sale !== ''
            && this.requestCreditForm.value.fuel !== null && this.requestCreditForm.value.fuel !== ''
            && this.requestCreditForm.value.nadaTradeInAvg !== null && this.requestCreditForm.value.nadaTradeInAvg !== ''
            && this.requestCreditForm.value.cleantitle !== null && this.requestCreditForm.value.cleantitle !== ''
            && this.vinIsValid === true
            ) {
            validForms = true;
        } else {
            validForms = false;
        }
        return validForms;
    }

    /*
    * Fuction to validate VIN
    */
    validateVIN(): void {
        this.isError = false;
        this.errorMsg = '';
        if(this.requestCreditForm.value.vin !== null && this.requestCreditForm.value.vin !== ''){
            const vehicle: any = {
                vin: this.requestCreditForm.value.vin
            };
            this.serviceValidateVINVehicle(vehicle);
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function by clean vehicle by VIN
    */
    otherVIN(): void {
        this.vinIsValid = false;
        this.requestCreditForm.value.vin = '';
        this.requestCreditForm.controls['vin'].setValue('');
        this.requestCreditForm.controls['make'].setValue('');
        this.requestCreditForm.controls['model'].setValue('');
        this.requestCreditForm.controls['year'].setValue('');
        // this.requestCreditForm.controls['fuel'].setValue(vehicle.fuelId);
    }

    /*
    * function serviceValidateVINVehicle
    * This method is used to validate the VIN of the vehicle section to the server.
    */
    serviceValidateVINVehicle(request: any): void {
		this.screenLoading = true;
        this.isError = false;
        this._requestService.validateVINVehicle(request).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                } else {
                    this.vinIsValid = true;
                    if(res && res.vin){
                        this.requestCreditForm.controls['make'].setValue(res.vin.make);
                        this.requestCreditForm.controls['model'].setValue(res.vin.model);
                        this.requestCreditForm.controls['year'].setValue(res.vin.modelYear);

                        const idFuel = this.fuelTypesList.filter((item: any) => this.mergeFuel(res.vin.fuelType) === item.name);
                        if(idFuel && idFuel.length > 0){
                            this.requestCreditForm.controls['fuel'].setValue(idFuel[0].id);
                        }
                    }
                }
                this._changeDetectorRef.markForCheck();
				this.screenLoading = false;
            },
            (err) => {
                this.isError = true;
                this.errorMsg = this._translocoService.translate('common.error');
                this.screenLoading = false;
            }
        );
    }
    /**
     * function by merge values fuel
     */
    mergeFuel(valueFuel: string): string {
        switch (valueFuel) {
            case 'Gasoline':
                return 'Gas';
                break;
            case 'Diesel':
                return 'Diesel';
                break;
            default:
                return '';
                break;
        }
    }

    /*
    * function createAdditionalInfoVehicle
    * This method is used to record the data from the vehicle section for the current credit application.
    */
    createAdditionalInfoVehicle(): void {
        this.isError = false;
        this.errorMsg = '';

        const validForms = this.checkAdditionalInfoVehicle();
        if (validForms) {
            const vehicle: AdditionalInfoVehicle = {
                vin: this.requestCreditForm.value.vin,
                make: this.requestCreditForm.value.make,
                model: this.requestCreditForm.value.model,
                year: this.requestCreditForm.value.year,
                mileage: this.requestCreditForm.value.mileage,
                salesPrice: this.requestCreditForm.value.sale,
                stockNumber: this.requestCreditForm.value.stock,
                cleanTitle: this.requestCreditForm.value.cleantitle,
                fuelId: this.requestCreditForm.value.fuel,
                rideShare: this.requestCreditForm.value.rideShare,
                nadaTradeInAvg: this.requestCreditForm.value.nadaTradeInAvg
            };

            this.selectedItem.vehicle = vehicle;
            this.serviceAdditionalInfoVehicle(false);
        } else {
            this.isError = true;
            if(this.vinIsValid){
                this.errorMsg = this._translocoService.translate('common.allRequired');
            }else{
                this.errorMsg = this._translocoService.translate('credit.car.errorValidate');
            }
        }
    }

    /*
    * function updateAdditionalInfoVehicle
    * This method is used to update the data from the vehicle section for the current credit application.
    */
    updateAdditionalInfoVehicle(): void {
        this.isError = false;
        this.errorMsg = '';

        const validForms = this.checkAdditionalInfoVehicle();
        if (validForms && this.selectedItem.vehicle.id != null) {
            const vehicle: AdditionalInfoVehicle = {
                id: this.selectedItem.vehicle.id,
                vin: this.requestCreditForm.value.vin,
                make: this.requestCreditForm.value.make,
                model: this.requestCreditForm.value.model,
                year: this.requestCreditForm.value.year,
                mileage: this.requestCreditForm.value.mileage,
                salesPrice: this.requestCreditForm.value.sale,
                stockNumber: this.requestCreditForm.value.stock,
                cleanTitle: this.requestCreditForm.value.cleantitle,
                fuelId: this.requestCreditForm.value.fuel,
                rideShare: this.requestCreditForm.value.rideShare,
                nadaTradeInAvg: this.requestCreditForm.value.nadaTradeInAvg
            };

            this.selectedItem.vehicle = vehicle;
            this.serviceAdditionalInfoVehicle(true);
        } else {
            this.isError = true;
            if(this.vinIsValid){
                this.errorMsg = this._translocoService.translate('common.allRequired');
            }else{
                this.errorMsg = this._translocoService.translate('credit.car.errorValidate');
            }
        }
    }

    /*
    * function serviceAdditionalInfoVehicle
    * This method is used to communicate the data of the vehicle section to the server.
    */
    serviceAdditionalInfoVehicle(isUpdate: boolean): void {
		this.screenLoading = true;
        this.isError = false;
        this._requestService.additionalInfoVehicle(this.selectedItem, isUpdate).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                } else {
                    if(!isUpdate){
                        this.selectedItem.vehicle.id = res.vehicleId;
                    }
                    this.controlNavigationButton('address');
                }
                this._changeDetectorRef.markForCheck();
				this.screenLoading = false;
            },
            (err) => {
                this.isError = true;
                this.errorMsg = this._translocoService.translate('common.error');
                this.screenLoading = false;
            }
        );
    }

    /*
    * function checkInitiatedRequest
    * This method is used to validate that the data in the additional information section is complete.
    */
    checkAdditionalInfoAddress(): boolean {

        let validForms = false;

        if (this.selectedItem.applicant != null && this.selectedItem.creditAppId != null
            && this.requestCreditForm.controls.street.valid
            && this.requestCreditForm.controls.state.valid
            && this.requestCreditForm.controls.city.valid
            && this.requestCreditForm.controls.zipcode.valid
            && this.requestCreditForm.controls.yearliving.valid
            && this.requestCreditForm.controls.monthliving.valid) {
                if(this.requestCreditForm.value.yearliving < 1 || this.hasSecondApplicantAddress) {
                    if (this.requestCreditForm.controls.secondstreet.valid
                        && this.requestCreditForm.controls.secondstate.valid
                        && this.requestCreditForm.controls.secondcity.valid
                        && this.requestCreditForm.controls.secondzipcode.valid
                        && this.requestCreditForm.controls.secondyearliving.valid
                        && this.requestCreditForm.controls.secondmonthliving.valid) {
                            validForms = true;
                        }else{
                            //Mark Errors form
                            this.requestCreditForm.get('secondstreet').markAsTouched();
                            this.requestCreditForm.get('secondstate').markAsTouched();
                            this.requestCreditForm.get('secondcity').markAsTouched();
                            this.requestCreditForm.get('secondzipcode').markAsTouched();
                            this.requestCreditForm.get('secondyearliving').markAsTouched();
                            this.requestCreditForm.get('secondmonthliving').markAsTouched();
                            validForms = false;
                        }
                }else{
                    validForms = true;
                }
        } else {
            //Mark Errors form
            this.requestCreditForm.get('street').markAsTouched();
            this.requestCreditForm.get('state').markAsTouched();
            this.requestCreditForm.get('city').markAsTouched();
            this.requestCreditForm.get('zipcode').markAsTouched();
            this.requestCreditForm.get('yearliving').markAsTouched();
            this.requestCreditForm.get('monthliving').markAsTouched();
            validForms = false;
        }
        if (validForms) {
            if (this.selectedItemCoApplicant != null) {
                if (this.selectedItem.creditAppId != null
                    && this.requestCreditForm.controls.costreet.valid
                    && this.requestCreditForm.controls.costate.valid
                    && this.requestCreditForm.controls.cocity.valid
                    && this.requestCreditForm.controls.cozipcode.valid
                    && this.requestCreditForm.controls.coyearliving.valid
                    && this.requestCreditForm.controls.comonthliving.valid) {
                        if(this.requestCreditForm.value.coyearliving < 1 || this.hasSecondcoApplicantAddress) {
                            if (this.requestCreditForm.controls.secondcostreet.valid
                                && this.requestCreditForm.controls.secondcostate.valid
                                && this.requestCreditForm.controls.secondcocity.valid
                                && this.requestCreditForm.controls.secondcozipcode.valid
                                && this.requestCreditForm.controls.secondcoyearliving.valid
                                && this.requestCreditForm.controls.secondcomonthliving.valid) {
                                    validForms = true;
                                }else{
                                    //Mark Errors form
                                    this.requestCreditForm.get('secondcostreet').markAsTouched();
                                    this.requestCreditForm.get('secondcostate').markAsTouched();
                                    this.requestCreditForm.get('secondcocity').markAsTouched();
                                    this.requestCreditForm.get('secondcozipcode').markAsTouched();
                                    this.requestCreditForm.get('secondcoyearliving').markAsTouched();
                                    this.requestCreditForm.get('secondcomonthliving').markAsTouched();
                                    validForms = false;
                                }
                        }else{
                            validForms = true;
                        }
                } else {
                    //Mark Errors form
                    this.requestCreditForm.get('costreet').markAsTouched();
                    this.requestCreditForm.get('costate').markAsTouched();
                    this.requestCreditForm.get('cocity').markAsTouched();
                    this.requestCreditForm.get('cozipcode').markAsTouched();
                    this.requestCreditForm.get('coyearliving').markAsTouched();
                    this.requestCreditForm.get('comonthliving').markAsTouched();
                    validForms = false;
                }
            } else {
                validForms = true;
            }
        }

        return validForms;
    }

    /*
    * function updateAdditionalInfoAddress
    * This method is used to update the data from the address section for the current credit application.
    */
    updateAdditionalInfoAddress(): void {
        this.isError = false;
        this.errorMsg = '';

        const validForms = this.checkAdditionalInfoAddress();
        if (validForms) {
            const adds: AdditionalInfoAddress[] = [];
            const addressApplicant: AdditionalInfoAddress = {
                id: this.requestCreditForm.value.addressId,
                clientId: this.selectedItem.applicant.id,
                street: this.requestCreditForm.value.street,
                number: (this.requestCreditForm.value.extnumber) ? this.requestCreditForm.value.extnumber : '',
                apt: this.requestCreditForm.value.intnumber,
                stateId: this.requestCreditForm.value.state,
                city: this.requestCreditForm.value.city,
                zip: this.requestCreditForm.value.zipcode,
                years: this.requestCreditForm.value.yearliving,
                months: this.requestCreditForm.value.monthliving,
                old: this.requestCreditForm.value.old,
            };
            adds.push(addressApplicant);

            if(this.requestCreditForm.value.yearliving < 1 || this.hasSecondApplicantAddress){
                const secondaddressApplicant: AdditionalInfoAddress = {
                    id: this.requestCreditForm.value.secondaddressId,
                    clientId: this.selectedItem.applicant.id,
                    street: this.requestCreditForm.value.secondstreet,
                    number: (this.requestCreditForm.value.secondextnumber) ? this.requestCreditForm.value.secondextnumber : '',
                    apt: this.requestCreditForm.value.secondintnumber,
                    stateId: this.requestCreditForm.value.secondstate,
                    city: this.requestCreditForm.value.secondcity,
                    zip: this.requestCreditForm.value.secondzipcode,
                    years: this.requestCreditForm.value.secondyearliving,
                    months: this.requestCreditForm.value.secondmonthliving,
                    old: this.requestCreditForm.value.secondold,
                };
                adds.push(secondaddressApplicant);
            }

            if (this.selectedItemCoApplicant != null) {
                const addressCoApplicant: AdditionalInfoAddress = {
                    id: this.requestCreditForm.value.coaddressId,
                    clientId: this.selectedItem.coApplicant.id,
                    street: this.requestCreditForm.value.costreet,
                    number: (this.requestCreditForm.value.coextnumber) ? this.requestCreditForm.value.coextnumber : '',
                    apt: this.requestCreditForm.value.cointnumber,
                    stateId: this.requestCreditForm.value.costate,
                    city: this.requestCreditForm.value.cocity,
                    zip: this.requestCreditForm.value.cozipcode,
                    years: this.requestCreditForm.value.coyearliving,
                    months: this.requestCreditForm.value.comonthliving,
                    old: this.requestCreditForm.value.coold,
                };
                adds.push(addressCoApplicant);

                if(this.requestCreditForm.value.coyearliving < 1 || this.hasSecondcoApplicantAddress){
                    const secondaddressCoApplicant: AdditionalInfoAddress = {
                        id: this.requestCreditForm.value.secondcoaddressId,
                        clientId: this.selectedItem.coApplicant.id,
                        street: this.requestCreditForm.value.secondcostreet,
                        number: (this.requestCreditForm.value.secondcoextnumber) ? this.requestCreditForm.value.secondcoextnumber : '',
                        apt: this.requestCreditForm.value.secondcointnumber,
                        stateId: this.requestCreditForm.value.secondcostate,
                        city: this.requestCreditForm.value.secondcocity,
                        zip: this.requestCreditForm.value.secondcozipcode,
                        years: this.requestCreditForm.value.secondcoyearliving,
                        months: this.requestCreditForm.value.secondcomonthliving,
                        old: this.requestCreditForm.value.secondcoold,
                    };
                    adds.push(secondaddressCoApplicant);
                }
            }
            this.selectedItem.address = adds;
            this.serviceAdditionalInfoAddress();
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
            if(((this.requestCreditForm.controls.costreet.valid
                && this.requestCreditForm.controls.costate.valid
                && this.requestCreditForm.controls.cocity.valid
                && this.requestCreditForm.controls.cozipcode.valid
                && this.requestCreditForm.controls.coyearliving.valid
                && this.requestCreditForm.controls.comonthliving.valid) && this.requestCreditForm.value.coyearliving < 1) || this.hasSecondcoApplicantAddress){
                this.hasSecondcoApplicantAddress = true;
                this.errorMsg = this._translocoService.translate('credit.direction.coapplicant_second_address_required');
            }
            if(((this.requestCreditForm.controls.street.valid
                && this.requestCreditForm.controls.state.valid
                && this.requestCreditForm.controls.city.valid
                && this.requestCreditForm.controls.zipcode.valid
                && this.requestCreditForm.controls.yearliving.valid
                && this.requestCreditForm.controls.monthliving.valid) && this.requestCreditForm.value.yearliving < 1) || this.hasSecondApplicantAddress){
                this.hasSecondApplicantAddress = true;
                this.errorMsg = this._translocoService.translate('credit.direction.applicant_second_address_required');
            }
        }
    }

    /*
    * function serviceAdditionalInfoAddress
    * This method is used to communicate the data of the address section to the server.
    */
    serviceAdditionalInfoAddress(): void {
		this.screenLoading = true;
        this._requestService.additionalInfoAddress(this.selectedItem).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                } else {
                    this.selectedItem.address = res.address;
                    res.address.forEach((add: AdditionalInfoAddress) => {
                        if(!add.coApplicant) {
                            if(add.street === this.requestCreditForm.value.street){
                                this.requestCreditForm.controls['addressId'].setValue(add.id);
                            }
                            if(add.street === this.requestCreditForm.value.secondstreet){
                                this.requestCreditForm.controls['secondaddressId'].setValue(add.id);
                            }
                        }
                        if(add.coApplicant) {
                            if(add.street === this.requestCreditForm.value.costreet){
                                this.requestCreditForm.controls['coaddressId'].setValue(add.id);
                            }
                            if(add.street === this.requestCreditForm.value.secondcostreet){
                                this.requestCreditForm.controls['secondcoaddressId'].setValue(add.id);
                            }
                        }
                    });
                    this.controlNavigationButton('documentation');
                }
                this._changeDetectorRef.markForCheck();
				this.screenLoading = false;
            },
            (err) => {
                this.isError = true;
                this.errorMsg = this._translocoService.translate('common.error');
                this.screenLoading = false;
            }
        );
    }

    checkAdditionalInfoDocument(document: AdditionalInfoDocument): void {
        this.isError = false;

        this.errorMsg = '';
        if (this.selectedItem.applicant != null
            && this.selectedItem.creditAppId != null
            && document != null
        ) {
            const documents: AdditionalInfoDocument[] = [];
            documents.push(document);
            this._requestService.additionalInfoDocument(this.selectedItem, documents).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isError = true;
                        this.errorMsg = res.idError + '-' + res.msgError;
                    } else {
                        this.getAdditionalInfoDocument();
                    }
                    this._changeDetectorRef.markForCheck();
                },
                (err) => {
                    this.isError = true;
                    this.errorMsg = this._translocoService.translate('common.error');
                }
            );
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    getAdditionalInfoDocument(): void {
        this.isError = false;
        this.errorMsg = '';
        if (this.selectedItem.applicant != null
            && this.selectedItem.creditAppId != null
        ) {
            this.screenLoading = true;
            this._requestService.additionalInfoGetDocument(this.selectedItem).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isError = true;
                        this.errorMsg = res.idError + '-' + res.msgError;
                    } else {
                        const docs = res.document;
                        const docsCoApplicant = [];
                        const docsApplicant = [];
                        docs.forEach((doc: any) => {
                            if (doc.coApplicant) {
                                docsCoApplicant.push(doc);
                            } else {
                                docsApplicant.push(doc);
                            }
                        });
                        this.selectedItem.documents = docsApplicant;
                        this.selectedItem.codocuments = docsCoApplicant;
                        this.closeDocumentUpload();
                    }
                    this._changeDetectorRef.markForCheck();
                    this.screenLoading = false;
                },
                (err) => {
                    this.isError = true;
                    this.errorMsg = this._translocoService.translate('common.error');
                    this.screenLoading = false;
                }
            );
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function createAdditionalInfoCommercial
    * This method is used to create the data from the commercial references section for the current credit application.
    */
    createAdditionalInfoCommercial(): void {
        this.isError = false;
        this.errorMsg = '';
        if (this.selectedItem.applicant != null && this.selectedItem.creditAppId != null
            && this.requestCreditForm.value.bank !== '') {
            this.selectedItem.bank = this.requestCreditForm.value.bank;
            this.selectedItem.branch = this.requestCreditForm.value.branch;
            this.selectedItem.accountType = this.requestCreditForm.value.accountType;
            this.selectedItem.accountNumber = this.requestCreditForm.value.accountNumber;
            this.serviceAdditionalInfoCommercial(false);
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function updateAdditionalInfoCommercial
    * This method is used to update the data from the commercial references section for the current credit application.
    */
    updateAdditionalInfoCommercial(): void {
        this.isError = false;
        this.errorMsg = '';
        if (this.selectedItem.applicant != null && this.selectedItem.creditAppId != null
            && this.requestCreditForm.value.bank !== '' && this.selectedItem.bankId != null) {
            this.selectedItem.bank = this.requestCreditForm.value.bank;
            this.selectedItem.branch = this.requestCreditForm.value.branch;
            this.selectedItem.accountType = this.requestCreditForm.value.accountType;
            this.selectedItem.accountNumber = this.requestCreditForm.value.accountNumber;
            this.serviceAdditionalInfoCommercial(true);
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function serviceAdditionalInfoCommercial
    * This method is used to communicate the data of the commercial references section to the server.
    */
    serviceAdditionalInfoCommercial(isUpdate: boolean): void {
		this.screenLoading = true;
        this._requestService.additionalInfoCommercial(this.selectedItem, isUpdate).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                } else {
                    if(!isUpdate){
                        this.selectedItem.bankId = res.id;
                    }
                    this.controlNavigationButton('employment');
                }
                this._changeDetectorRef.markForCheck();
				this.screenLoading = false;
            },
            (err) => {
                this.isError = true;
                this.errorMsg = this._translocoService.translate('common.error');
                this.screenLoading = false;
            }
        );
    }

    /*
    * function checkAdditionalInfoEmployment
    * This method is used to validate that the data in the employment references section is complete.
    */
    checkAdditionalInfoEmployment(): boolean {

        let validForms = false;

        if (this.selectedItem.applicant != null && this.selectedItem.creditAppId != null
            && this.requestCreditForm.controls.employmentName.valid
            && this.requestCreditForm.controls.employmentPhone.valid
            && this.requestCreditForm.controls.employmentPosition.valid
            && this.requestCreditForm.controls.employmentYear.valid
            && this.requestCreditForm.controls.employmentMonth.valid) {
                if(this.requestCreditForm.value.employmentYear < 1 || this.hasSecondApplicantEmployment) {
                    if (this.requestCreditForm.controls.secondemploymentName.valid
                        && this.requestCreditForm.controls.secondemploymentPhone.valid
                        && this.requestCreditForm.controls.secondemploymentPosition.valid
                        && this.requestCreditForm.controls.secondemploymentYear.valid
                        && this.requestCreditForm.controls.secondemploymentMonth.valid) {
                            validForms = true;
                        }else{
                            //Mark Errors form
                            this.requestCreditForm.get('secondemploymentName').markAsTouched();
                            this.requestCreditForm.get('secondemploymentPhone').markAsTouched();
                            this.requestCreditForm.get('secondemploymentPosition').markAsTouched();
                            this.requestCreditForm.get('secondemploymentYear').markAsTouched();
                            this.requestCreditForm.get('secondemploymentMonth').markAsTouched();
                            validForms = false;
                        }
                }else{
                    validForms = true;
                }
        } else {
            //Mark Errors form
            this.requestCreditForm.get('employmentName').markAsTouched();
            this.requestCreditForm.get('employmentPhone').markAsTouched();
            this.requestCreditForm.get('employmentPosition').markAsTouched();
            this.requestCreditForm.get('employmentYear').markAsTouched();
            this.requestCreditForm.get('employmentMonth').markAsTouched();
            validForms = false;
        }
        if (validForms) {
            if (this.selectedItemCoApplicant != null) {
                if (this.selectedItem.creditAppId != null
                    && this.requestCreditForm.controls.coemploymentName.valid
                    && this.requestCreditForm.controls.coemploymentPhone.valid
                    && this.requestCreditForm.controls.coemploymentPosition.valid
                    && this.requestCreditForm.controls.coemploymentYear.valid
                    && this.requestCreditForm.controls.coemploymentMonth.valid) {
                        if(this.requestCreditForm.value.coemploymentYear < 1 || this.hasSecondCoApplicantEmployment) {
                            if (this.requestCreditForm.controls.secondcoemploymentName.valid
                                && this.requestCreditForm.controls.secondcoemploymentPhone.valid
                                && this.requestCreditForm.controls.secondcoemploymentPosition.valid
                                && this.requestCreditForm.controls.secondcoemploymentYear.valid
                                && this.requestCreditForm.controls.secondcoemploymentMonth.valid) {
                                    validForms = true;
                                }else{
                                    //Mark Errors form
                                    this.requestCreditForm.get('secondcoemploymentName').markAsTouched();
                                    this.requestCreditForm.get('secondcoemploymentPhone').markAsTouched();
                                    this.requestCreditForm.get('secondcoemploymentPosition').markAsTouched();
                                    this.requestCreditForm.get('secondcoemploymentYear').markAsTouched();
                                    this.requestCreditForm.get('secondcoemploymentMonth').markAsTouched();
                                    validForms = false;
                                }
                        }else{
                            validForms = true;
                        }
                } else {
                    //Mark Errors form
                    this.requestCreditForm.get('coemploymentName').markAsTouched();
                    this.requestCreditForm.get('coemploymentPhone').markAsTouched();
                    this.requestCreditForm.get('coemploymentPosition').markAsTouched();
                    this.requestCreditForm.get('coemploymentYear').markAsTouched();
                    this.requestCreditForm.get('coemploymentMonth').markAsTouched();
                    validForms = false;
                }
            } else {
                validForms = true;
            }
        }
        return validForms;
    }

    /*
    * function updateAdditionalInfoEmployment
    * This method is used to update the data from the employment references section for the current credit application.
    */
    updateAdditionalInfoEmployment(): void {
        this.isError = false;
        this.errorMsg = '';

        const validForms = this.checkAdditionalInfoEmployment();
        if (validForms) {
            const employments: AdditionalInfoEmployment[] = [];
            const employmentapplicant: AdditionalInfoEmployment = {
                id: this.requestCreditForm.value.employmentId,
                clientId: this.selectedItem.applicant.id,
                companyName: this.requestCreditForm.value.employmentName,
                position: this.requestCreditForm.value.employmentPosition,
                telephone: this.requestCreditForm.value.employmentPhone,
                years: this.requestCreditForm.value.employmentYear,
                months: this.requestCreditForm.value.employmentMonth,
                old: this.requestCreditForm.value.employmentold
            };
            employments.push(employmentapplicant);

            if(this.requestCreditForm.value.employmentYear < 1 || this.hasSecondApplicantEmployment){
                const secondemploymentapplicant: AdditionalInfoEmployment = {
                    id: this.requestCreditForm.value.secondemploymentId,
                    clientId: this.selectedItem.applicant.id,
                    companyName: this.requestCreditForm.value.secondemploymentName,
                    position: this.requestCreditForm.value.secondemploymentPosition,
                    telephone: this.requestCreditForm.value.secondemploymentPhone,
                    years: this.requestCreditForm.value.secondemploymentYear,
                    months: this.requestCreditForm.value.secondemploymentMonth,
                    old: this.requestCreditForm.value.secondemploymentold
                };
                employments.push(secondemploymentapplicant);
            }

            if (validForms && this.selectedItem.coApplicant != null) {
                const employmentcoapplicant: AdditionalInfoEmployment = {
                    id: this.requestCreditForm.value.coemploymentId,
                    clientId: this.selectedItem.coApplicant.id,
                    companyName: this.requestCreditForm.value.coemploymentName,
                    position: this.requestCreditForm.value.coemploymentPosition,
                    telephone: this.requestCreditForm.value.coemploymentPhone,
                    years: this.requestCreditForm.value.coemploymentYear,
                    months: this.requestCreditForm.value.coemploymentMonth,
                    old: this.requestCreditForm.value.coemploymentold
                };
                employments.push(employmentcoapplicant);

                if(this.requestCreditForm.value.coemploymentYear < 1 || this.hasSecondCoApplicantEmployment){
                    const secondemploymentcoapplicant: AdditionalInfoEmployment = {
                        id: this.requestCreditForm.value.secondcoemploymentId,
                        clientId: this.selectedItem.coApplicant.id,
                        companyName: this.requestCreditForm.value.secondcoemploymentName,
                        position: this.requestCreditForm.value.secondcoemploymentPosition,
                        telephone: this.requestCreditForm.value.secondcoemploymentPhone,
                        years: this.requestCreditForm.value.secondcoemploymentYear,
                        months: this.requestCreditForm.value.secondcoemploymentMonth,
                        old: this.requestCreditForm.value.secondcoemploymentold
                    };
                    employments.push(secondemploymentcoapplicant);
                }
            }
            this.selectedItem.employments = employments;
            this.serviceAdditionalInfoEmployment(true);
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');

            if((this.requestCreditForm.controls.coemploymentName.valid
                && this.requestCreditForm.controls.coemploymentPhone.valid
                && this.requestCreditForm.controls.coemploymentPosition.valid
                && this.requestCreditForm.controls.coemploymentYear.valid
                && this.requestCreditForm.controls.coemploymentMonth.valid) && this.requestCreditForm.value.coemploymentYear < 1){
                this.hasSecondCoApplicantEmployment = true;
                this.errorMsg = this._translocoService.translate('credit.employments.coapplicant_second_required');
            }
            if((this.requestCreditForm.controls.employmentName.valid
                && this.requestCreditForm.controls.employmentPhone.valid
                && this.requestCreditForm.controls.employmentPosition.valid
                && this.requestCreditForm.controls.employmentYear.valid
                && this.requestCreditForm.controls.employmentMonth.valid) && this.requestCreditForm.value.employmentYear < 1){
                this.hasSecondApplicantEmployment = true;
                this.errorMsg = this._translocoService.translate('credit.employments.applicant_second_required');
            }
        }
    }

    /*
    * function serviceAdditionalInfoEmployment
    * This method is used to communicate the data of the employment references section to the server.
    */
    serviceAdditionalInfoEmployment(isUpdate: boolean): void {
		this.screenLoading = true;
        this._requestService.additionalInfoEmployment(this.selectedItem, isUpdate).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                } else {
                    this.selectedItem.employments = res.employment;
                    res.employment.forEach((add: AdditionalInfoEmployment) => {
                        if(!add.coApplicant) {
                            if(add.companyName === this.requestCreditForm.value.employmentName){
                                this.requestCreditForm.controls['employmentId'].setValue(add.id);
                            }
                            if(add.companyName === this.requestCreditForm.value.secondemploymentName){
                                this.requestCreditForm.controls['secondemploymentId'].setValue(add.id);
                            }
                        }
                        if(add.coApplicant) {
                            if(add.companyName === this.requestCreditForm.value.coemploymentName){
                                this.requestCreditForm.controls['coemploymentId'].setValue(add.id);
                            }
                            if(add.companyName === this.requestCreditForm.value.secondcoemploymentName){
                                this.requestCreditForm.controls['secondcoemploymentId'].setValue(add.id);
                            }
                        }
                    });
                    this.controlNavigationButton('personal');
                }
                this._changeDetectorRef.markForCheck();
				this.screenLoading = false;
            },
            (err) => {
                this.isError = true;
                this.errorMsg = this._translocoService.translate('common.error');
                this.screenLoading = false;
            }
        );
    }

    /*
    * function createAdditionalInfoPersonal
    * This method is used to create and update the data from the personal references section for the current credit application.
    */
    checkAdditionalInfoPersonal(): void {
        this.isError = false;
        this.errorMsg = '';

        if (this.selectedItem.applicant != null && this.selectedItem.creditAppId != null) {
            const personals: AdditionalInfoPersonal[] = [];

            let worksReferences = 0;
            let personalReferences = 0;

            //Validate Forms dynamics
            for (let iPersonal = 0; iPersonal < this.arrayFormPersonalRederences.length; iPersonal++) {
                const personalNumberForm = this.arrayFormPersonalRederences[iPersonal];


                if (this.personalReferencesForm.controls[`personalName${personalNumberForm}`].valid
                    && this.personalReferencesForm.controls[`personalPhone${personalNumberForm}`].valid
                    && this.personalReferencesForm.controls[`personalRelationship${personalNumberForm}`].valid) {
                        const personal: AdditionalInfoPersonal = {
                            clientId: this.selectedItemClient.id,
                            id: (this.personalReferencesForm.value[`personalId${personalNumberForm}`] ? this.personalReferencesForm.value[`personalId${personalNumberForm}`] : ''),
                            name: this.personalReferencesForm.value[`personalName${personalNumberForm}`],
                            telephone: this.personalReferencesForm.value[`personalPhone${personalNumberForm}`],
                            relationship: this.personalReferencesForm.value[`personalRelationship${personalNumberForm}`],
                            work: this.personalReferencesForm.value[`personalWork${personalNumberForm}`]
                        };
                        personals.push(personal);
                        if(this.personalReferencesForm.value[`personalWork${personalNumberForm}`]){
                            worksReferences = worksReferences+1;
                        }else{
                            personalReferences = personalReferences+1;
                        }
                }else{
                    //Mark Errors form
                    this.personalReferencesForm.get(`personalName${personalNumberForm}`).markAsTouched();
                    this.personalReferencesForm.get(`personalPhone${personalNumberForm}`).markAsTouched();
                    this.personalReferencesForm.get(`personalRelationship${personalNumberForm}`).markAsTouched();
                }
            }


                if(this.mandatoryPersonalReferences === (personalReferences + worksReferences)){
                    if(personalReferences >= this.minPersonalReferences
                        && worksReferences >= this.minWorkPersonalReferences){
                        this.selectedItem.personals = personals;
                        this.screenLoading = true;
                        this._requestService.additionalInfoPersonals(this.selectedItem).subscribe(
                            (res: any) => {
                                if (res.error) {
                                    this.isError = true;
                                    this.errorMsg = res.idError + '-' + res.msgError;
                                } else {
                                    this.selectedItem.personals = res.personalRef;
                                    res.personalRef.forEach((add: AdditionalInfoPersonal) => {
                                        for (let iPersonalForm = 0; iPersonalForm < this.arrayFormPersonalRederences.length; iPersonalForm++) {
                                            const key = this.arrayFormPersonalRederences[iPersonalForm];
                                            if(String(this.personalReferencesForm.value[`personalPhone${key}`]) === String(add.telephone) && !this.personalReferencesForm.value[`personalId${key}`]){
                                                this.personalReferencesForm.controls[`personalId${key}`].setValue(add.id);
                                                break;
                                            }
                                        }
                                    });
                                    this.controlNavigationButton('credit');
                                }
                                this._changeDetectorRef.markForCheck();
                                this.screenLoading = false;
                            },(err) => {
                                this.isError = true;
                                this.errorMsg = this._translocoService.translate('common.error');
                                this.screenLoading = false;
                            }
                        );
                    }else{
                        this.isError = true;
                        this.errorMsg = this._translocoService.translate('credit.personals.missing_reference', { works: this.userLogued.minWorkReferences, personals: this.userLogued.minPersonalReferences });
                    }
                }else{
                    this.isError = true;
                    this.errorMsg = this._translocoService.translate('common.allRequired');
                }
        }else{
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function checkAdditionalInfoCredit
    * This method is used to create and update the data from the credit section for the current credit application.
    */
    checkAdditionalInfoCredit(): void {
        this.isError = false;
        this.errorMsg = '';
        if (this.selectedItem.applicant != null && this.selectedItem.creditAppId != null
            && this.requestCreditForm.value.requestAmount !== '' && this.requestCreditForm.value.requestDownpayment !== ''
            && this.requestCreditForm.value.requestPeriods !== '' && this.requestCreditForm.value.requestCar !== ''
            && this.requestCreditForm.value.firstpayment !== '' && this.requestCreditForm.value.requestLoan !== ''
            && this.requestCreditForm.value.scheduledContract !== '' && this.requestCreditForm.value.requestAPR !== ''
            && this.requestCreditForm.value.requestAPR !== null && this.requestCreditForm.value.requestAPR <= 100 && this.requestCreditForm.value.requestAPR >= 0 ) {

            this.selectedItem.requestedAmount = this.requestCreditForm.value.requestAmount;
            this.selectedItem.downPayment = this.requestCreditForm.value.requestDownpayment;
            this.selectedItem.apr = this.requestCreditForm.value.requestAPR;
            this.selectedItem.payTermId = this.requestCreditForm.value.requestPeriods;
            this.selectedItem.loanTerm = this.requestCreditForm.value.requestLoan;
            this.selectedItem.carPayment = this.requestCreditForm.value.requestCar;
            this.selectedItem.firstPayment = this.requestCreditForm.value.firstpayment?.format('YYYY-MM-DD');
            this.selectedItem.scheduledContract = this.requestCreditForm.value.scheduledContract?.format('YYYY-MM-DD');

            this.screenLoading = true;
            this._requestService.additionalInfoCredit(this.selectedItem, this.isUpdateCreditApp).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isError = true;
                        this.errorMsg = res.idError + '-' + res.msgError;
                    } else {
                        this.checkPreApproval();
                    }
                    this._changeDetectorRef.markForCheck();
					this.screenLoading = false;
                },
                (err) => {
                    this.isError = true;
                    this.errorMsg = this._translocoService.translate('common.error');
                    this.screenLoading = false;
                }
            );
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function checkPreApproval
    * This method is used to get data from the pre approval section for the current credit application.
    */
    checkPreApproval(): void {
        this.isError = false;
        this.errorMsg = '';
        if (this.selectedItem.creditAppId != null) {

            this._requestService.preApproval(this.selectedItem).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isError = true;
                        this.errorMsg = res.idError + '-' + res.msgError;
                    } else {
                        this.preApprovedResult = res.result;
                        this.controlNavigationButton('preapproval');
                    }
                    this._changeDetectorRef.markForCheck();
                }
            );
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function checkAcceptPreApproval
    * This method is used to get data from the pre approval section for the current credit application.
    */
    checkAcceptPreApproval(): void {
        this.isError = false;
        this.errorMsg = '';
        if (this.selectedItem.creditAppId != null) {
            this.screenLoading = true;
            this._requestService.acceptPreApproval(this.selectedItem).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isError = true;
                        this.errorMsg = res.idError + '-' + res.msgError;
                    } else {
                        this.setupPanels();
                        this._router.navigate(['credits/credit'], { queryParams: { id: this.selectedItem.creditAppId, permision:'read' }, skipLocationChange: true });
                    }
                    this._changeDetectorRef.markForCheck();
                    this.screenLoading = false;
                },
                (err) => {
                    this.screenLoading = false;
                }
            );
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    /*
    * function checkRejectPreApproval
    * This method is used to get data from the pre approval section for the current credit application.
    */
    checkRejectPreApproval(): void {
        this._router.navigate(['credits/requests']);
    }

    getPAL(): void {
        this.isError = false;
        this.errorMsg = '';
        if (this.selectedItem.creditAppId != null) {
            this._requestService.getPAL(this.selectedItem).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isError = true;
                        this.errorMsg = res.idError + '-' + res.msgError;
                    } else {
                        const keyPAL = res.keyPAL;
                        this.downloadFileKey(keyPAL);
                    }
                    this._changeDetectorRef.markForCheck();
                }
            );
        } else {
            this.isError = true;
            this.errorMsg = this._translocoService.translate('common.allRequired');
        }
    }

    async downloadFileKey(keyPAL: string): Promise<void>{
        const creditAppId = this.selectedItem.creditAppId;
        const keyFileName = moment().valueOf() + '_' + creditAppId + '_PAL.pdf';
        const result = await Storage.get(keyPAL, { download: true });
        this.downloadBlob(result.Body, keyFileName);
    }

    /*
    * function getCreditAppData
    * This method is used to get all data for the current credit application.
    */
    getCreditAppData(id: any): void {
        this.screenLoading = true;
        this._requestService.getCreditApp(id).subscribe(
            (res: any) => {
                if (res.error) {
                    this.isError = true;
                    this.errorMsg = res.idError + '-' + res.msgError;
                    this.screenLoading = false;
                } else {
                    this.checkInitiatedRequest();
                    const clients = res.clientInfo;
                    const vehicles = res.vehicle;
                    const addresses = res.address;
                    const additionals = res.additionalInfo;
                    const documents = res.document;
                    const commercials = res.commercialRef;
                    const employments = res.employment;
                    const personals = res.personalRef;
                    const credit = res.creditApp;

                    //Cean general info client
                    this.generalInfoClient = {};
                    this.showCoApplicant = false;
                    clients.forEach((client: any) => {
                        if (client.coApplicant) {
                            this.selectedItemCoApplicant = client;
                            this.selectedItem.coApplicant = client;
                            this.selectedItem.coApplicant.id = client.clientId;
                            this.addMandatoryFormCoApplicant();
                            this.updateValdatorsCoApplicant();
                            this.showCoApplicant = true;
                        } else {
                            this.selectedItemClient = client;
                            this.selectedItem.applicant = client;
                            this.selectedItem.applicant.id = client.clientId;
                            this.deleteMandatoryFormCoApplicant();
                            this.updateValdatorsCoApplicant();
                            //Load general info client
                            this.generalInfoClient.nameClient = client.name+' '+client.middleName+' '+client.lastName;
                        }
                    });

                    this.requestCreditForm.controls['expiration'].setValue(null);
                    this.requestCreditForm.controls['bankruptcydate'].setValue(null);
                    this.requestCreditForm.controls['repossessiondate'].setValue(null);
                    this.requestCreditForm.controls['coexpiration'].setValue(null);
                    this.requestCreditForm.controls['cobankruptcydate'].setValue(null);
                    this.requestCreditForm.controls['corepossessiondate'].setValue(null);

                    this.selectedItem.additionalInfo = additionals;
                    additionals.forEach((additional: AdditionalInfo) => {
                        if (additional.coApplicant) {
                            const expirationMoment = additional.idExpDate !== undefined ? moment(additional.idExpDate, 'YYYY-MM-DD') : null;
                            const bankrupcyDateMoment = additional.bankrupcyDate !== undefined ? moment(additional.bankrupcyDate, 'YYYY-MM-DD') : null;
                            const vehicleRepossesionDateMoment = additional.vehicleRepossesionDate !== undefined ? moment(additional.vehicleRepossesionDate, 'YYYY-MM-DD') : null;
                            this.requestCreditForm.controls['coidentification'].setValue(additional.idNumber);
                            this.requestCreditForm.controls['coexpiration'].setValue(expirationMoment);
                            this.requestCreditForm.controls['coidentificationtype'].setValue(additional.idType);
                            this.requestCreditForm.controls['coresidencetype'].setValue(additional.residenceId);
                            this.requestCreditForm.controls['cohowyears'].setValue(additional.yearsResidence);
                            this.requestCreditForm.controls['comaritialstatus'].setValue(additional.fillingId);
                            this.requestCreditForm.controls['codependants'].setValue(additional.dependantsNumber);
                            this.requestCreditForm.controls['coincomefrequency'].setValue(additional.incomeFrecuencyId);
                            this.requestCreditForm.controls['comonthlyincome'].setValue(additional.avgIncome);
                            this.requestCreditForm.controls['comonthlyutilities'].setValue(additional.avgUtilities);
                            this.requestCreditForm.controls['comonthlyrentmortgage'].setValue(additional.avgRent);
                            this.requestCreditForm.controls['cobankruptcyintend'].setValue(additional.bankrupcyIntend);
                            this.requestCreditForm.controls['cobankrupt'].setValue(additional.bankrupcy);
                            this.requestCreditForm.controls['cobankruptcydate'].setValue(bankrupcyDateMoment);
                            this.requestCreditForm.controls['cobankruptreason'].setValue(additional.bankrupcyInfo);
                            this.requestCreditForm.controls['corepossession'].setValue(additional.vehicleRepossesion);
                            this.requestCreditForm.controls['corepossessiondate'].setValue(vehicleRepossesionDateMoment);
                            this.requestCreditForm.controls['corepossessionreason'].setValue(additional.vehicleRepossesionInfo);
                        } else {
                            const expirationMoment = additional.idExpDate !== undefined ? moment(additional.idExpDate, 'YYYY-MM-DD') : null;
                            const bankrupcyDateMoment = additional.bankrupcyDate !== undefined ? moment(additional.bankrupcyDate, 'YYYY-MM-DD') : null;
                            const vehicleRepossesionDateMoment = additional.vehicleRepossesionDate !== undefined ? moment(additional.vehicleRepossesionDate, 'YYYY-MM-DD') : null;
                            this.requestCreditForm.controls['identification'].setValue(additional.idNumber);
                            this.requestCreditForm.controls['expiration'].setValue(expirationMoment);
                            this.requestCreditForm.controls['identificationtype'].setValue(additional.idType);
                            this.requestCreditForm.controls['residencetype'].setValue(additional.residenceId);
                            this.requestCreditForm.controls['howyears'].setValue(additional.yearsResidence);
                            this.requestCreditForm.controls['maritialstatus'].setValue(additional.fillingId);
                            this.requestCreditForm.controls['dependants'].setValue(additional.dependantsNumber);
                            this.requestCreditForm.controls['incomefrequency'].setValue(additional.incomeFrecuencyId);
                            this.requestCreditForm.controls['monthlyincome'].setValue(additional.avgIncome);
                            this.requestCreditForm.controls['monthlyutilities'].setValue(additional.avgUtilities);
                            this.requestCreditForm.controls['monthlyrentmortgage'].setValue(additional.avgRent);
                            this.requestCreditForm.controls['bankruptcyintend'].setValue(additional.bankrupcyIntend);
                            this.requestCreditForm.controls['bankrupt'].setValue(additional.bankrupcy);
                            this.requestCreditForm.controls['bankruptcydate'].setValue(bankrupcyDateMoment);
                            this.requestCreditForm.controls['bankruptreason'].setValue(additional.bankrupcyInfo);
                            this.requestCreditForm.controls['repossession'].setValue(additional.vehicleRepossesion);
                            this.requestCreditForm.controls['repossessiondate'].setValue(vehicleRepossesionDateMoment);
                            this.requestCreditForm.controls['repossessionreason'].setValue(additional.vehicleRepossesionInfo);
                        }
                    });

                    vehicles.forEach((vehicle: AdditionalInfoVehicle) => {
                        this.selectedItem.vehicle = vehicle;
                        this.requestCreditForm.controls['vin'].setValue(vehicle.vin);
                        this.requestCreditForm.controls['make'].setValue(vehicle.make);
                        this.requestCreditForm.controls['model'].setValue(vehicle.model);
                        this.requestCreditForm.controls['year'].setValue(vehicle.year);
                        this.requestCreditForm.controls['mileage'].setValue(vehicle.mileage);
                        this.requestCreditForm.controls['sale'].setValue(vehicle.salesPrice);
                        this.requestCreditForm.controls['stock'].setValue(vehicle.stockNumber);
                        this.requestCreditForm.controls['cleantitle'].setValue(vehicle.titleId);
                        this.requestCreditForm.controls['fuel'].setValue(vehicle.fuelId);
                        this.requestCreditForm.controls['rideShare'].setValue(vehicle.rideShare);
                        this.requestCreditForm.controls['nadaTradeInAvg'].setValue(vehicle.nadaTradeInAvg);
                        if(vehicle.vin && vehicle.vin !== '' && vehicle.vin !== null){
                            this.vinIsValid = true;
                        }
                    });

                    this.selectedItem.address = addresses;
                    addresses.forEach((address: any) => {
                        if (address.coApplicant) {
                            // if(this.requestCreditForm.value.coaddressId !== null
                            //     && this.requestCreditForm.value.coaddressId !== ''
                            //     && this.requestCreditForm.value.coaddressId === '0'){
                            if(address.old === false){
                                    this.requestCreditForm.controls['coaddressId'].setValue(address.id);
                                    this.requestCreditForm.controls['costreet'].setValue(address.street);
                                    this.requestCreditForm.controls['coextnumber'].setValue(address.number);
                                    this.requestCreditForm.controls['cointnumber'].setValue(address.apt);
                                    this.requestCreditForm.controls['costate'].setValue(address.stateId);
                                    this.requestCreditForm.controls['cocity'].setValue(address.city);
                                    this.requestCreditForm.controls['cozipcode'].setValue(address.zip);
                                    this.requestCreditForm.controls['coyearliving'].setValue(address.years);
                                    this.requestCreditForm.controls['comonthliving'].setValue(address.months);
                                    this.requestCreditForm.controls['coold'].setValue(address.old);
                            }else{
                                    this.requestCreditForm.controls['secondcoaddressId'].setValue(address.id);
                                    this.requestCreditForm.controls['secondcostreet'].setValue(address.street);
                                    this.requestCreditForm.controls['secondcoextnumber'].setValue(address.number);
                                    this.requestCreditForm.controls['secondcointnumber'].setValue(address.apt);
                                    this.requestCreditForm.controls['secondcostate'].setValue(address.stateId);
                                    this.requestCreditForm.controls['secondcocity'].setValue(address.city);
                                    this.requestCreditForm.controls['secondcozipcode'].setValue(address.zip);
                                    this.requestCreditForm.controls['secondcoyearliving'].setValue(address.years);
                                    this.requestCreditForm.controls['secondcomonthliving'].setValue(address.months);
                                    this.requestCreditForm.controls['secondcoold'].setValue(address.old);

                                    this.hasSecondcoApplicantAddress = true;
                            }
                        } else {
                            // if(this.requestCreditForm.value.addressId !== null
                            //     && this.requestCreditForm.value.addressId !== ''
                            //     && this.requestCreditForm.value.addressId === '0'){
                            if(address.old === false){
                                    this.requestCreditForm.controls['addressId'].setValue(address.id);
                                    this.requestCreditForm.controls['street'].setValue(address.street);
                                    this.requestCreditForm.controls['extnumber'].setValue(address.number);
                                    this.requestCreditForm.controls['intnumber'].setValue(address.apt);
                                    this.requestCreditForm.controls['state'].setValue(address.stateId);
                                    this.requestCreditForm.controls['city'].setValue(address.city);
                                    this.requestCreditForm.controls['zipcode'].setValue(address.zip);
                                    this.requestCreditForm.controls['yearliving'].setValue(address.years);
                                    this.requestCreditForm.controls['monthliving'].setValue(address.months);
                                    this.requestCreditForm.controls['old'].setValue(address.old);
                            }else{
                                    this.requestCreditForm.controls['secondaddressId'].setValue(address.id);
                                    this.requestCreditForm.controls['secondstreet'].setValue(address.street);
                                    this.requestCreditForm.controls['secondextnumber'].setValue(address.number);
                                    this.requestCreditForm.controls['secondintnumber'].setValue(address.apt);
                                    this.requestCreditForm.controls['secondstate'].setValue(address.stateId);
                                    this.requestCreditForm.controls['secondcity'].setValue(address.city);
                                    this.requestCreditForm.controls['secondzipcode'].setValue(address.zip);
                                    this.requestCreditForm.controls['secondyearliving'].setValue(address.years);
                                    this.requestCreditForm.controls['secondmonthliving'].setValue(address.months);
                                    this.requestCreditForm.controls['secondold'].setValue(address.old);

                                    this.hasSecondApplicantAddress = true;
                            }
                        }
                    });

                    const docsCoApplicant = [];
                    const docsApplicant = [];
                    documents.forEach((doc: any) => {
                        if (doc.coApplicant) {
                            docsCoApplicant.push(doc);
                        } else {
                            docsApplicant.push(doc);
                        }
                    });
                    this.selectedItem.documents = docsApplicant;
                    this.selectedItem.codocuments = docsCoApplicant;

                    commercials.forEach((commercial: any) => {
                        this.selectedItem.bankId = commercial.id;
                        this.selectedItem.bank = commercial.bank;
                        this.selectedItem.branch = commercial.branch;
                        this.selectedItem.accountNumber = commercial.accountNumber;
                        this.requestCreditForm.controls['bank'].setValue(commercial.bank);
                        this.requestCreditForm.controls['branch'].setValue(commercial.branch);
                        this.requestCreditForm.controls['accountNumber'].setValue(commercial.accountNumber);
                        if(commercial.accountTypeId && commercial.accountTypeId !== '' && commercial.accountTypeId !== null){
                            this.selectedItem.accountType = commercial.accountTypeId;
                            this.requestCreditForm.controls['accountType'].setValue(commercial.accountTypeId);
                        }else{
                            this.selectedItem.accountType = '0';
                            this.requestCreditForm.controls['accountType'].setValue(0);
                        }
                    });

                    this.selectedItem.employments = employments;
                    console.log('==========');
                    console.log('RESPUESTA DEL SERVICIO EN EMPLEOS');
                    console.log(employments);
                    console.log('==========');
                    employments.forEach((employment: AdditionalInfoEmployment) => {
                        if (employment.coApplicant) {
                            // if(this.requestCreditForm.value.coemploymentId !== null
                            //     && this.requestCreditForm.value.coemploymentId !== ''
                            //     && this.requestCreditForm.value.coemploymentId === '0'){
                            if(employment.old === false){
                                    this.requestCreditForm.controls['coemploymentId'].setValue(employment.id);
                                    this.requestCreditForm.controls['coemploymentName'].setValue(employment.companyName);
                                    this.requestCreditForm.controls['coemploymentPosition'].setValue(employment.position);
                                    this.requestCreditForm.controls['coemploymentPhone'].setValue(employment.telephone);
                                    this.requestCreditForm.controls['coemploymentYear'].setValue(employment.years);
                                    this.requestCreditForm.controls['coemploymentMonth'].setValue(employment.months);
                                    this.requestCreditForm.controls['coemploymentold'].setValue(employment.old);
                            }else{
                                    this.requestCreditForm.controls['secondcoemploymentId'].setValue(employment.id);
                                    this.requestCreditForm.controls['secondcoemploymentName'].setValue(employment.companyName);
                                    this.requestCreditForm.controls['secondcoemploymentPosition'].setValue(employment.position);
                                    this.requestCreditForm.controls['secondcoemploymentPhone'].setValue(employment.telephone);
                                    this.requestCreditForm.controls['secondcoemploymentYear'].setValue(employment.years);
                                    this.requestCreditForm.controls['secondcoemploymentMonth'].setValue(employment.months);
                                    this.requestCreditForm.controls['secondcoemploymentold'].setValue(employment.old);

                                    this.hasSecondCoApplicantEmployment = true;
                            }
                        } else {
                            // if(this.requestCreditForm.value.employmentId !== null
                            //     && this.requestCreditForm.value.employmentId !== ''
                            //     && this.requestCreditForm.value.employmentId === '0'){
                            if(employment.old === false){
                                    this.requestCreditForm.controls['employmentId'].setValue(employment.id);
                                    this.requestCreditForm.controls['employmentName'].setValue(employment.companyName);
                                    this.requestCreditForm.controls['employmentPosition'].setValue(employment.position);
                                    this.requestCreditForm.controls['employmentPhone'].setValue(employment.telephone);
                                    this.requestCreditForm.controls['employmentYear'].setValue(employment.years);
                                    this.requestCreditForm.controls['employmentMonth'].setValue(employment.months);
                                    this.requestCreditForm.controls['employmentold'].setValue(employment.old);
                            }else{
                                    this.requestCreditForm.controls['secondemploymentId'].setValue(employment.id);
                                    this.requestCreditForm.controls['secondemploymentName'].setValue(employment.companyName);
                                    this.requestCreditForm.controls['secondemploymentPosition'].setValue(employment.position);
                                    this.requestCreditForm.controls['secondemploymentPhone'].setValue(employment.telephone);
                                    this.requestCreditForm.controls['secondemploymentYear'].setValue(employment.years);
                                    this.requestCreditForm.controls['secondemploymentMonth'].setValue(employment.months);
                                    this.requestCreditForm.controls['secondemploymentold'].setValue(employment.old);

                                    this.hasSecondApplicantEmployment = true;
                            }
                        }
                    });

                    this.selectedItem.personals = personals;
                    if(this.selectedItem.personals.length > this.mandatoryPersonalReferences){
                        this.arrayFormPersonalRederences = [];
                        for (let i = 0; i < this.selectedItem.personals.length; i++) {
                            this.arrayFormPersonalRederences.push(i+1);
                            this.addControlFormReference(i+1);
                        }
                        this.mandatoryPersonalReferences = this.selectedItem.personals.length;
                    }

                    personals.forEach((personal: AdditionalInfoPersonal, index) => {
                            this.personalReferencesForm.controls[`personalId${index +1}`].setValue(personal.id);
                            this.personalReferencesForm.controls[`personalName${index +1}`].setValue(personal.name);
                            this.personalReferencesForm.controls[`personalRelationship${index +1}`].setValue(personal.relationship);
                            this.personalReferencesForm.controls[`personalPhone${index +1}`].setValue(personal.telephone);
                            this.personalReferencesForm.controls[`personalWork${index +1}`].setValue(personal.work);
                    });

                    for (const field in this.personalReferencesForm.controls) {
                        if (!this.isAllowUpdatePermission) {
                            this.personalReferencesForm.get(field).disable();
                        } else {
                            this.personalReferencesForm.get(field).enable();
                        }
                    }

                    if (credit != null) {
                        this.selectedItem.creditAppId = credit.id;
                        this.selectedItem.requestedAmount = credit.requestedAmount;
                        this.selectedItem.downPayment = credit.downpayment;
                        this.selectedItem.payTermId = credit.payTermId;
                        this.selectedItem.loanTerm = credit.loanTerm;
                        this.selectedItem.carPayment = credit.termCarPayment;
                        this.selectedItem.firstPayment = credit.firstPayment;
                        this.selectedItem.scheduledContract = credit.scheduledContract;
                        this.selectedItem.statusId = credit.statusId;

                        const firstPaymentMoment = moment(credit.firstPayment, 'YYYY-MM-DD');
                        const scheduledContractMoment = moment(credit.scheduledContract, 'YYYY-MM-DD');

                        this.requestCreditForm.controls['requestAmount'].setValue(credit.requestedAmount);
                        this.requestCreditForm.controls['requestDownpayment'].setValue(credit.downpayment);
                        this.requestCreditForm.controls['requestAPR'].setValue(credit.apr);
                        this.requestCreditForm.controls['requestPeriods'].setValue(credit.payTermId);
                        this.requestCreditForm.controls['requestLoan'].setValue(credit.loanTerm);
                        this.requestCreditForm.controls['requestCar'].setValue(credit.termCarPayment);
                        this.requestCreditForm.controls['firstpayment'].setValue(firstPaymentMoment);
                        this.requestCreditForm.controls['scheduledContract'].setValue(scheduledContractMoment);

                        //Load general info client
                        this.selectedItem.creditAppNumber = credit.number;
                        this.generalInfoClient.requestNumber = credit.number;
                        this.generalInfoClient.dealer = credit.dealer;
                        this.generalInfoClient.dealership = credit.originator;
                    }

                    this.isUpdateCreditApp = true;
                    this.panels.splice(0,1);
                    this.selectedPanel = 'account';
                    this.controlNavigationButton('account');
                    this.screenLoading = false;
                }
                this._changeDetectorRef.markForCheck();
            },
            (err) => {
                this.screenLoading = false;
            }
        );
    }

    changeZip(event: Event, key: string): void{
        this.minZero(event, key);
        if((event.target as HTMLInputElement).value.length > 5){
            const str = ((event.target as HTMLInputElement).value).slice(0, -1);
            this.requestCreditForm.get(key).patchValue(str);
            this._changeDetectorRef.markForCheck();
        }
    }

    changePhone(event: Event, key: string): void{
        this.minZero(event, key);
        if((event.target as HTMLInputElement).value.length > 14){
            const str = ((event.target as HTMLInputElement).value).slice(0, -1);
            this.requestCreditForm.get(key).patchValue(str);
            this._changeDetectorRef.markForCheck();
        }
    }

    changePhoneDynamic(event: Event, idx: any): void{
        if((event.target as HTMLInputElement).value.length > 14){
            const str = ((event.target as HTMLInputElement).value).slice(0, -1);
            this.personalReferencesForm.get(`personalPhone${idx}`).patchValue(str);
            this._changeDetectorRef.markForCheck();
        }
    }

    minZero(event: Event, key: string): void{
        if(Number((event.target as HTMLInputElement).value) < 0){
            this.requestCreditForm.get(key).patchValue('0');
            this._changeDetectorRef.markForCheck();
        }
    }

    maxMonth(event: Event, key: string): void{
        this.minZero(event, key);
        if(Number((event.target as HTMLInputElement).value) > 11){
            const str = ((event.target as HTMLInputElement).value).slice(0, -1);
            this.requestCreditForm.get(key).patchValue(str);
            this._changeDetectorRef.markForCheck();
        }
    }

    minDateExpirationFilter = (m: Moment | null): boolean => {
        const dateFilter = (m || moment()) >= moment().subtract(1, 'days');;
        return dateFilter;
    };

    // functions to control navigation
    controlNavigationButton(panel): void {
        switch(panel){
            case 'search':
                this.needShowBackButton = false;
                this.needShowNextButton = false;
                this.nextPanel = 'account';
                break;
            case 'account':
                this.needShowBackButton = !(this.selectedItem?.creditAppId != null && this.selectedItem.creditAppId !== 0);
                this.needShowNextButton = true;
                this.nextPanel = 'vehicle';
                break;
            case 'vehicle':
                this.needShowBackButton = true;
                this.needShowNextButton = true;
                this.selectedPanel = panel;
                this.nextPanel = 'address';
                break;
            case 'address':
                this.needShowBackButton = true;
                this.needShowNextButton = true;
                this.selectedPanel = panel;
                this.nextPanel = 'documentation';
                break;
            case 'documentation':
                this.needShowBackButton = true;
                this.needShowNextButton = true;
                this.selectedPanel = panel;
                this.nextPanel = 'commercial';
                break;
            case 'commercial':
                this.needShowBackButton = true;
                this.needShowNextButton = true;
                this.selectedPanel = panel;
                this.nextPanel = 'employment';
                break;
            case 'employment':
                this.needShowBackButton = true;
                this.needShowNextButton = true;
                this.selectedPanel = panel;
                this.nextPanel = 'personal';
                break;
            case 'personal':
                this.needShowBackButton = true;
                this.needShowNextButton = true;
                this.selectedPanel = panel;
                this.nextPanel = 'credit';
                break;
            case 'credit':
                this.needShowBackButton = true;
                this.needShowNextButton = this.isAllowUpdatePermission;
                this.selectedPanel = panel;
                this.nextPanel = 'preapproval';
                break;
            case 'preapproval':
                this.needShowBackButton = (this.preApprovedResult.preApproved || this.preApprovedResult.counteroffer);
                this.needShowNextButton = false;
                this.selectedPanel = panel;
                this.nextPanel = '';
                break;
        }
        this._changeDetectorRef.markForCheck();
    }

    onClickNext(panel): void {
        this.checkInitiatedRequest();
        switch (panel) {
            case 'account':
                if(this.selectedItemClient !== null){
                    this.isError = false;
                    this.errorMsg = '';
                    this.requestCreditForm.reset();
                    this.requestCreditForm.controls['bankruptcyintend'].setValue(false);
                    this.requestCreditForm.controls['bankrupt'].setValue(false);
                    this.requestCreditForm.controls['repossession'].setValue(false);
                    this.requestCreditForm.controls['cobankruptcyintend'].setValue(false);
                    this.requestCreditForm.controls['cobankrupt'].setValue(false);
                    this.requestCreditForm.controls['corepossession'].setValue(false);
                    this.requestCreditForm.controls['cleantitle'].setValue(1);//ToDo poner dato por default
                    this.requestCreditForm.controls['rideShare'].setValue(false);
                    this.requestCreditForm.controls['old'].setValue(false);
                    this.requestCreditForm.controls['secondold'].setValue(true);
                    this.requestCreditForm.controls['coold'].setValue(false);
                    this.requestCreditForm.controls['secondcoold'].setValue(true);
                    this.requestCreditForm.controls['employmentold'].setValue(false);
                    this.requestCreditForm.controls['secondemploymentold'].setValue(true);
                    this.requestCreditForm.controls['coemploymentold'].setValue(false);
                    this.requestCreditForm.controls['secondcoemploymentold'].setValue(true);
                    this.personalReferencesForm.reset();
                    this.manageMandatoryFormReference();
                    this.selectedItem.applicant = this.selectedItemClient;
                    this.selectedPanel = panel;
                    this.controlNavigationButton(panel);
                }else{
                    this.isError = true;
                    this.errorMsg = 'Need selected an applicant';
                }
                break;
            case 'vehicle':
                if (this.isAllowUpdatePermission) {
                    //Se modifica esta sección y solo agregar el update
                    this.updateAdditionalInfo();
                } else {
                    this.controlNavigationButton('vehicle');
                }
                break;
            case 'address':
                if (this.isAllowUpdatePermission) {
                    if (this.selectedItem.vehicle?.id != null) {
                        this.updateAdditionalInfoVehicle();
                    } else {
                        this.createAdditionalInfoVehicle();
                    }
                } else {
                    this.controlNavigationButton('address');
                }
                break;
            case 'documentation':
                if (this.isAllowUpdatePermission) {
                    //Se modifica esta sección y solo agregar el update
                    this.updateAdditionalInfoAddress();
                } else {
                    this.controlNavigationButton('documentation');
                }
                break;
            case 'commercial':
                this.controlNavigationButton('commercial');
                break;
            case 'employment':
                if (this.isAllowUpdatePermission) {
                    if (this.isUpdateCreditApp && this.selectedItem.bankId != null) {
                        this.updateAdditionalInfoCommercial();
                    } else {
                        this.createAdditionalInfoCommercial();
                    }
                } else {
                    this.controlNavigationButton('employment');
                }
                break;
            case 'personal':
                if (this.isAllowUpdatePermission) {
                        this.updateAdditionalInfoEmployment();
                } else {
                    this.controlNavigationButton('personal');
                }
                break;
            case 'credit':
                if (this.isAllowUpdatePermission) {
                    this.checkAdditionalInfoPersonal();
                } else {
                    this.controlNavigationButton('credit');
                }
                break;
            case 'preapproval':
                if (this.isAllowUpdatePermission) {
                    this.checkAdditionalInfoCredit();
                } else {
                    this.controlNavigationButton('preapproval');
                }
                break;
        }
    }

    onClickBack(panel): void {
        this.preApprovedResult = null;
        switch (panel) {
            case 'account':
                this.selectedPanel = 'search';
                this.showCoApplicant = false;
                this.selectedItemCoApplicant = null;
                this.clientsList = [];
                for(let i = 0; i < this.clientsListBackup.length; i++){
                    const client = this.clientsListBackup[i];
                    this.clientsList.push(client);
                }
                break;
            case 'vehicle':
                this.selectedPanel = 'account';
                break;
            case 'address':
                this.selectedPanel = 'vehicle';
                break;
            case 'documentation':
                this.selectedPanel = 'address';
                break;
            case 'commercial':
                this.selectedPanel = 'documentation';
                break;
            case 'employment':
                this.selectedPanel = 'commercial';
                break;
            case 'personal':
                this.selectedPanel = 'employment';
                break;
            case 'credit':
                this.selectedPanel = 'personal';
                break;
            case 'preapproval':
                this.selectedPanel = 'credit';
                break;
        }
        this.nextPanel = panel;
        this.controlNavigationButton(this.selectedPanel);
    }

    setupPanels(): void{
        this.panels = [
            { id: 'search', icon: 'heroicons_outline:search', title: 'credit.searchclient' },
            { id: 'account', icon: 'heroicons_outline:identification', title: 'credit.applicant' },
            { id: 'vehicle', icon: 'heroicons_outline:truck', title: 'credit.vehicle' },
            { id: 'address', icon: 'heroicons_outline:location-marker', title: 'credit.address' },
            { id: 'documentation', icon: 'heroicons_outline:folder-open', title: 'credit.documentation' },
            { id: 'commercial', icon: 'heroicons_outline:office-building', title: 'credit.commercial' },
            { id: 'employment', icon: 'heroicons_outline:briefcase', title: 'credit.employment' },
            { id: 'personal', icon: 'heroicons_outline:chat', title: 'credit.personal' },
            { id: 'credit', icon: 'heroicons_outline:clipboard-list', title: 'credit.credit' },
            { id: 'preapproval', icon: 'heroicons_outline:clipboard-check', title: 'credit.preapproval' }
        ];
    }

    clearFormSecondAddressApplicant(): void{
        this.requestCreditForm.get('secondaddressId').patchValue('0');
        this.requestCreditForm.get('secondstreet').patchValue('');
        this.requestCreditForm.get('secondextnumber').patchValue('');
        this.requestCreditForm.get('secondintnumber').patchValue('');
        this.requestCreditForm.get('secondstate').patchValue('');
        this.requestCreditForm.get('secondcity').patchValue('');
        this.requestCreditForm.get('secondzipcode').patchValue('');
        this.requestCreditForm.get('secondyearliving').patchValue('');
        this.requestCreditForm.get('secondmonthliving').patchValue('');
        this.requestCreditForm.get('secondold').patchValue(true);
    }

    clearFormSecondAddressCoApplicant(): void{
        this.requestCreditForm.get('secondcoaddressId').patchValue('0');
        this.requestCreditForm.get('secondcostreet').patchValue('');
        this.requestCreditForm.get('secondcoextnumber').patchValue('');
        this.requestCreditForm.get('secondcointnumber').patchValue('');
        this.requestCreditForm.get('secondcostate').patchValue('');
        this.requestCreditForm.get('secondcocity').patchValue('');
        this.requestCreditForm.get('secondcozipcode').patchValue('');
        this.requestCreditForm.get('secondcoyearliving').patchValue('');
        this.requestCreditForm.get('secondcomonthliving').patchValue('');
        this.requestCreditForm.get('secondcoold').patchValue(true);
    }

    clickSecondAddress(applicant: boolean): void{
        if(applicant){
            if(this.hasSecondApplicantAddress){
                const addressId = this.requestCreditForm.value.secondaddressId;
                if(addressId !== null && addressId !== '' && addressId !== '0'){
                    this.screenLoading = true;
                    this._requestService.updateStatusService(this.selectedItem, 2, addressId).subscribe(
                        (res: any) => {
                            if (res.error) {
                                this.isError = true;
                                this.errorMsg = res.idError + '-' + res.msgError;
                            }
                            this.hasSecondApplicantAddress = !this.hasSecondApplicantAddress;
                            this.clearFormSecondAddressApplicant();
                            this._changeDetectorRef.markForCheck();
                            this.screenLoading = false;
                        },
                        (err) => {
                            this.isError = true;
                            this.errorMsg = this._translocoService.translate('common.error');
                            this.screenLoading = false;
                        });
                }else{
                    this.hasSecondApplicantAddress = !this.hasSecondApplicantAddress;
                    this.clearFormSecondAddressApplicant();
                }
            }else{
                this.hasSecondApplicantAddress = !this.hasSecondApplicantAddress;
                this.clearFormSecondAddressApplicant();
                this._changeDetectorRef.markForCheck();
            }
        }else{
            if(this.hasSecondcoApplicantAddress){
                const addressId = this.requestCreditForm.value.secondcoaddressId;
                if(addressId !== null && addressId !== '' && addressId !== '0'){
                    this.screenLoading = true;
                    this._requestService.updateStatusService(this.selectedItem, 2, addressId).subscribe(
                        (res: any) => {
                            if (res.error) {
                                this.isError = true;
                                this.errorMsg = res.idError + '-' + res.msgError;
                            }
                            this.hasSecondcoApplicantAddress = !this.hasSecondcoApplicantAddress;
                            this.clearFormSecondAddressCoApplicant();
                            this._changeDetectorRef.markForCheck();
                            this.screenLoading = false;
                        },
                        (err) => {
                            this.isError = true;
                            this.errorMsg = this._translocoService.translate('common.error');
                            this.screenLoading = false;
                        });
                }else{
                    this.hasSecondcoApplicantAddress = !this.hasSecondcoApplicantAddress;
                    this.clearFormSecondAddressCoApplicant();
                }
            }else{
                this.hasSecondcoApplicantAddress = !this.hasSecondcoApplicantAddress;
                this.clearFormSecondAddressCoApplicant();
                this._changeDetectorRef.markForCheck();
            }
        }
    }

    clearFormSecondEmploymentApplicant(): void{
        this.requestCreditForm.get('secondemploymentId').patchValue('0');
        this.requestCreditForm.get('secondemploymentName').patchValue('');
        this.requestCreditForm.get('secondemploymentPosition').patchValue('');
        this.requestCreditForm.get('secondemploymentPhone').patchValue('');
        this.requestCreditForm.get('secondemploymentYear').patchValue('');
        this.requestCreditForm.get('secondemploymentMonth').patchValue('');
        this.requestCreditForm.get('secondemploymentold').patchValue(true);
    }

    clearFormSecondEmploymentCoApplicant(): void{
        this.requestCreditForm.get('secondcoemploymentId').patchValue('0');
        this.requestCreditForm.get('secondcoemploymentName').patchValue('');
        this.requestCreditForm.get('secondcoemploymentPosition').patchValue('');
        this.requestCreditForm.get('secondcoemploymentPhone').patchValue('');
        this.requestCreditForm.get('secondcoemploymentYear').patchValue('');
        this.requestCreditForm.get('secondcoemploymentMonth').patchValue('');
        this.requestCreditForm.get('secondcoemploymentold').patchValue(true);
    }

    clickSecondEmployment(applicant: boolean): void{
        if(applicant){
            this.hasSecondApplicantEmployment = !this.hasSecondApplicantEmployment;
        }else{
            this.hasSecondCoApplicantEmployment = !this.hasSecondCoApplicantEmployment;
        }
        this._changeDetectorRef.markForCheck();
    }

    clickDeleteSecondEmployment(applicant: boolean): void{

        if(applicant){
            const employmentId = this.requestCreditForm.value.secondemploymentId;
            if(employmentId !== null && employmentId !== '' && employmentId !== '0'){
                this.screenLoading = true;
                this._requestService.updateStatusService(this.selectedItem, 3, employmentId).subscribe(
                    (res: any) => {
                        if (res.error) {
                            this.isError = true;
                            this.errorMsg = res.idError + '-' + res.msgError;
                        }
                        this.hasSecondApplicantEmployment = !this.hasSecondApplicantEmployment;
                        this.clearFormSecondEmploymentApplicant();
                        this._changeDetectorRef.markForCheck();
                        this.screenLoading = false;
                    },
                    (err) => {
                        this.isError = true;
                        this.errorMsg = this._translocoService.translate('common.error');
                        this.screenLoading = false;
                    });
            }else{
                this.hasSecondApplicantEmployment = !this.hasSecondApplicantEmployment;
                this.clearFormSecondEmploymentApplicant();
            }
        }else{
            const employmentId = this.requestCreditForm.value.secondcoemploymentId;
            if(employmentId !== null && employmentId !== '' && employmentId !== '0'){
                this.screenLoading = true;
                this._requestService.updateStatusService(this.selectedItem, 3, employmentId).subscribe(
                    (res: any) => {
                        if (res.error) {
                            this.isError = true;
                            this.errorMsg = res.idError + '-' + res.msgError;
                        }
                        this.hasSecondCoApplicantEmployment = !this.hasSecondCoApplicantEmployment;
                        this.clearFormSecondEmploymentCoApplicant();
                        this._changeDetectorRef.markForCheck();
                        this.screenLoading = false;
                    },
                    (err) => {
                        this.isError = true;
                        this.errorMsg = this._translocoService.translate('common.error');
                        this.screenLoading = false;
                    });
            }else{
                this.hasSecondCoApplicantEmployment = !this.hasSecondCoApplicantEmployment;
                this.clearFormSecondEmploymentCoApplicant();
            }
        }
    }


    manageMandatoryFormReference(): void{

        this.arrayFormPersonalRederences = [];
        for (let i = 0; i < this.minPersonalReferences + this.minWorkPersonalReferences; i++) {
            this.arrayFormPersonalRederences.push(i+1);
            this.addControlFormReference(i+1);
        }
        this.mandatoryPersonalReferences = this.minPersonalReferences + this.minWorkPersonalReferences;
        this.minmandatoryPersonalReferences = this.minPersonalReferences + this.minWorkPersonalReferences;

        for(let i=1; i <= this.minWorkPersonalReferences; i++){
            this.personalReferencesForm.get(`personalWork${i}`).patchValue(true);
        }
        for (const field in this.personalReferencesForm.controls) {
            if (!this.isAllowUpdatePermission) {
                this.personalReferencesForm.get(field).disable();
            } else {
                this.personalReferencesForm.get(field).enable();
            }
        }
        this._changeDetectorRef.markForCheck();
    }

    clickAddReference(): void {
        this.mandatoryPersonalReferences = this.mandatoryPersonalReferences + 1;
        const maxPerson = this.arrayFormPersonalRederences.reduce((op, item) => op = op > item ? op : item, 0);
        this.arrayFormPersonalRederences.push(maxPerson+1);
        this.addControlFormReference(maxPerson+1);
        this._changeDetectorRef.markForCheck();
    }

    clickDeleteReference(i: number): void {
        const personalId = this.personalReferencesForm.get(`personalId${i}`)?.value;
        if(personalId !== undefined && personalId !== null && personalId !== '' && personalId !== '0'){
            this.screenLoading = true;
            this._requestService.updateStatusService(this.selectedItem, 4, personalId).subscribe(
                (res: any) => {
                    if (res.error) {
                        this.isError = true;
                        this.errorMsg = res.idError + '-' + res.msgError;
                    }

                    this.personalReferencesForm.controls[`personalId${i}`].setValue('0');
                    this.personalReferencesForm.controls[`personalName${i}`].setValue(null);
                    this.personalReferencesForm.controls[`personalRelationship${i}`].setValue(null);
                    this.personalReferencesForm.controls[`personalPhone${i}`].setValue(null);
                    this.personalReferencesForm.controls[`personalWork${i}`].setValue(null);

                    this.selectedItem.personals.splice((i-1), 1);
                    const indexDelete = this.arrayFormPersonalRederences.findIndex(x => x === i);
                    this.arrayFormPersonalRederences.splice(indexDelete, 1);
                    this.removeControlFormReference(i);
                    this.mandatoryPersonalReferences = this.mandatoryPersonalReferences - 1;

                    this._changeDetectorRef.markForCheck();
                    this.screenLoading = false;
                },
                (err) => {
                    this.isError = true;
                    this.errorMsg = this._translocoService.translate('common.error');
                    this.screenLoading = false;
                });
        }else{
            this.mandatoryPersonalReferences = this.mandatoryPersonalReferences - 1;
            const indexDelete = this.arrayFormPersonalRederences.findIndex(x => x === i);
            this.arrayFormPersonalRederences.splice(indexDelete, 1);
            this.removeControlFormReference(i);
        }
    }


    addControlFormReference(key: any): void{
        this.personalReferencesForm.addControl(`personalId${key}`, this._formBuilder.control(''));
        this.personalReferencesForm.addControl(`personalName${key}`, this._formBuilder.control('', Validators.required));
        this.personalReferencesForm.addControl(`personalPhone${key}`, this._formBuilder.control('', [Validators.required, Validators.pattern('[0-9 ]{10}')]));
        this.personalReferencesForm.addControl(`personalRelationship${key}`, this._formBuilder.control('', Validators.required));
        this.personalReferencesForm.addControl(`personalWork${key}`, this._formBuilder.control(false));
        this.personalReferencesForm.controls[`personalWork${key}`].setValue(false);
    }

    removeControlFormReference(key: any): void{
        this.personalReferencesForm.removeControl(`personalId${key}`);
        this.personalReferencesForm.removeControl(`personalName${key}`);
        this.personalReferencesForm.removeControl(`personalPhone${key}`);
        this.personalReferencesForm.removeControl(`personalRelationship${key}`);
        this.personalReferencesForm.removeControl(`personalWork${key}`);
    }

    //renameInput dynamic
    concateInput(str1: string, str2: any): string{
        return str1.concat(str2);
    }

}
