import { Route } from '@angular/router';
import { RequestsComponent } from './requests/requests.component';
import { RequestComponent } from './request/request.component';



export const creditsRouting: Route[] = [
  {
      path: 'requests',
      component: RequestsComponent
  },
  {
    path: 'credit',
    component: RequestComponent
}
];
