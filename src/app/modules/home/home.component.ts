import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    nameUser: string;
    constructor(
        private router: Router,
        private userService: UserService
    ) { }

    ngOnInit(): void {
        //Load NameUser
        this.getUserLogued();
    }



    /**
     * Sign out
     */
    signOut(): void
    {
        this.router.navigate(['/sign-out']);
    }

    /**
     * Function by get User logued
     */
    getUserLogued(): void {
        const userLogued: User = this.userService.userLogued;
        if(userLogued && userLogued.userId){
            this.nameUser = userLogued?.name;
        }
    }

    /**
     * getNameInitials
     */
     getNameInitials(name: string): string {
        let initials = '';
        if(name && name!== ''){
            const arrayName = name.split(' ');
            for (let index = 0; (index <= 1 && index < arrayName.length) ; index++) {
                initials = initials + arrayName[index].substring(0, 1);
            }
        }
        return initials;
     }

}
