import { Route } from '@angular/router';
import { ClientsComponent } from './clients.component';
import { ListComponent } from './list/list.component';

export const clientsRouting: Route[] = [
  {
      path     : '',
      component: ClientsComponent,
      children : [
          {
              path     : '',
              component: ListComponent
          }
      ]
  }
];
