import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, Subject, takeUntil, merge } from 'rxjs';
import { ClientService } from 'app/services/Client.service';
import { Client } from 'app/entities/Client.types';
import { Pagination } from 'app/entities/Pagination.types';
import { FuseAlertType } from '@fuse/components/alert';
import { cloneDeep } from 'lodash-es';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';
import moment from 'moment';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    rowListAll: Client[] = [];
    rowList: Client[] = [];
    // eslint-disable-next-line @typescript-eslint/member-ordering
    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };
    products$: Observable<Client[]>;

    flashMessage: 'success' | 'created' | 'error' | null = null;
    flashMessageMsg: string = '';
    isLoading: boolean = false;
    // Load the gray layer
    screenLoading: boolean = false;
    pagination: Pagination;
    selectedItem: Client | null = null;
    selectedItemForm: FormGroup;
    editMode: boolean = false;
    showFormUserAssociate: boolean = false;
    userAssociate: {list: Client[]; errorMessage: string} = {
        list: [],
        errorMessage: ''
    };

    optionFilter = 0;
    strFilter = '';

    user: User;
    roleAdmin = 1;

    showAlert: boolean = false;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(private _changeDetectorRef: ChangeDetectorRef, private _formBuilder: FormBuilder
        , private _clientService: ClientService, private _userService: UserService,) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.user = this._userService.userLogued;
        // Create the selected product form
        this.selectedItemForm = this._formBuilder.group({
            id: [''],
            email: [''],
            name: ['', [Validators.required]],
            middleName: [''],
            lastName: ['', [Validators.required]],
            dateBirth: ['', [Validators.required]],
            cellphone: ['', [Validators.required]],
            homePhone: [''],
            ssnItin: [''],
            status: [false]
        });
        this.loadClients(true);
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {
        if (this._sort && this._paginator) {
            // Set the initial sort
            this._sort.sort({
                id: 'id',
                start: 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;
                    // Close the details
                    this.closeDetails();
                });
        }
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    inicializeTable(): void {
        if (this._sort && this._paginator) {
            // Set the initial sort
            this._sort.sort({
                id: 'id',
                start: 'asc',
                disableClear: true
            });

            // Mark for check
            this._changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;
                    // Close the details
                    this.closeDetails();
                });

            // Get admins if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).subscribe(() => {
                this.closeDetails();
                this.isLoading = true;
                // eslint-disable-next-line max-len
                let objResult: any;
                if (this._sort && this._paginator) {
                    objResult = this.calculatePagination(this.rowListAll, null, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize);
                } else {
                    objResult = this.calculatePagination(this.rowListAll);
                }
                this.rowList = objResult.list;
                this.pagination = objResult.pagination;
                this.isLoading = false;
                this._changeDetectorRef.markForCheck();
            });
        }
    }

    /**
     * Load clients
     */
    loadClients(inicializeTable: boolean = false): void {
        this.screenLoading = true;
        this.selectedItem = null;
        this.selectedItemForm.reset();
        this._clientService.getObjects().subscribe(
            (res: any) => {
                if (res.error) {
                    this.showFlashMessage('error', 'Error '+res.idError+'!. '+res.msgError);
                    this.rowList = [];
                } else {
                    this.rowListAll = res.clientList;
                    const objResult: any = this.calculatePagination(res.clientList);
                    this.rowList = objResult.list;
                    this.pagination = objResult.pagination;
                }
                this.closeDetails();
                if (inicializeTable) {
                    setTimeout(() => {
                        this.inicializeTable();
                    }, 500);
                }
                this._changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                this.showFlashMessage('error', 'Error! '+ JSON.stringify(err));
                this.screenLoading = false;
            }
        );
    }

    /**
     * Calculate pagination by table
     */
    calculatePagination(list: Client[], search: string = '', sort: any = 'id', order: string = 'asc', page: number = 0, size: number = 10): any {
        // // Clone
        let listClone: Client[] = cloneDeep(list);
        // Sort the listClone
        if (sort === 'id' || sort === 'name' || sort === 'status') {
            listClone.sort((a, b) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldA: any;
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldB: any;
                if (sort === 'id') {
                    fieldA = parseInt(a[sort], 10);
                    fieldB = parseInt(b[sort], 10);
                    // eslint-disable-next-line max-len
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB, undefined, { numeric: 'true' }) : fieldB.toString().localeCompare(fieldA, undefined, { numeric: 'true' });
                } else {
                    fieldA = a[sort].toString().toUpperCase();
                    fieldB = b[sort].toString().toUpperCase();
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB) : fieldB.toString().localeCompare(fieldA);
                }
            });
        }
        else {
            listClone.sort((a, b) => order === 'asc' ? a[sort] - b[sort] : b[sort] - a[sort]);
        }

        // If search exists...
        if (search) {
            // Filter the listClone
            listClone = listClone.filter(contact => contact.name && contact.name.toLowerCase().includes(search.toLowerCase()));
        }

        // Paginate - Start
        const listCloneLength = listClone.length;

        // Calculate pagination details
        const begin = page * size;
        const end = Math.min((size * (page + 1)), listCloneLength);
        const lastPage = Math.max(Math.ceil(listCloneLength / size), 1);

        // Prepare the pagination object
        let pagination = {};

        // If the requested page number is bigger than
        // the last possible page number, return null for
        // adminListClone but also send the last possible page so
        // the app can navigate to there
        if (page > lastPage) {
            listClone = null;
            pagination = {
                lastPage
            };
        }
        else {
            // Paginate the results by size
            listClone = listClone.slice(begin, end);

            // Prepare the pagination mock-api
            pagination = {
                length: listCloneLength,
                size: size,
                page: page,
                lastPage: lastPage,
                startIndex: begin,
                endIndex: end - 1
            };
        }
        return {
            list: listClone,
            pagination: pagination
        };
    }

    /**
     * Toggle item details
     *
     * @param item
     */
    toggleDetails(item: Client): void {
        this.cleanClientAssociate();
        // If the product is already selected...
        if (this.selectedItem && this.selectedItem.id === item.id) {
            // Close the details
            this.closeDetails();
            return;
        }
        this.editMode = true;
        // Set the selected product
        this.selectedItem = item;
        // Fill the form
        console.log(item);
        this.selectedItemForm.patchValue(item);

        const dateBirth = item.dateBirth !== undefined ? moment(item.dateBirth, 'YYYY-MM-DD') : null;
        this.selectedItemForm.controls['dateBirth'].setValue(dateBirth);
        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Close the details
     */
    closeDetails(): void {
        this.selectedItem = null;
        this.cleanClientAssociate();
    }

    /**
     * Create client
     */
     createClient(): void {
        const item: Client = {
            id: 0
            , email: ''
            , name: ''
            , middleName: ''
            , lastName: ''
            , dateBirth: ''
            , cellphone: ''
            , homePhone: ''
            , ssnItin: null
            , status: true
        };

        let elementScroll: any = document.getElementsByClassName('elementScroll');

        this.editMode = false;
        this.rowList.unshift(item);
        this.selectedItem = item;
        this.selectedItemForm.patchValue(item);
        this._changeDetectorRef.markForCheck();

        // Scroll to top
        elementScroll[0].scrollTop = 0;
    }

    /**
     * Update the selected item using the form data
     */
    update(): void {

        if(this.selectedItemForm.invalid){
            return;
        }
        const item = this.selectedItemForm.value;
        item.dateBirth = this.selectedItemForm.value.dateBirth?.format('YYYY-MM-DD');
        this.screenLoading = true;
        if (!this.editMode) {
            const errorListToAssociate = ['36','37','38'];
            this._clientService.create(item).subscribe(
                (res: any) => {
                    if (res.error) {
                        if(res.idError && errorListToAssociate.includes(res.idError)){
                            this.showFormUserAssociate = true;
                            this.userAssociate.list = res.clientList;
                            this.userAssociate.errorMessage = res.msgError;
                        }else{
                            const msg = res.idError + '-' + res.msgError;
                            this.showMessage('warn', msg);
                        }
                        this.screenLoading = false;
                    } else {
                        this.loadClients(false);
                        this.showMessage('success', res.message);
                    }
                },
                (err) => {
                    const msg = JSON.stringify(err);
                    this.showMessage('warn', msg);
                    this.screenLoading = false;
                }
            );
        } else {
            const errorListToAssociate = ['56','57','58'];
            this._clientService.update(item).subscribe(
                (res: any) => {
                    if (res.error) {
                        if(res.idError && errorListToAssociate.includes(res.idError)){
                            this.showFormUserAssociate = true;
                            this.userAssociate.list = res.clientList;
                            this.userAssociate.errorMessage = res.msgError;
                        }else{
                            const dateBirth = item.dateBirth !== undefined ? moment(item.dateBirth, 'YYYY-MM-DD') : null;
                            this.selectedItemForm.controls['dateBirth'].setValue(dateBirth);
                            const msg = res.idError + '-' + res.msgError;
                            this.showMessage('warn', msg);
                        }
                        this.screenLoading = false;
                    } else {
                        this.loadClients(false);
                        this.showMessage('success', res.message);
                    }
                },
                (err) => {
                    const msg = JSON.stringify(err);
                    this.showMessage('warn', msg);
                    this.screenLoading = false;
                }
            );
        }
    }

    /**
     * Function by associate User to dealer
     */
    associateUser(clint: Client): void {
        this._clientService.associateUser(clint).subscribe(
            (res: any) => {
                if (res.error) {
                    this.showFlashMessage('error');
                } else {
                    this.closeDetails();
                    this.showFlashMessage('success');
                    setTimeout(() => {
                        this.loadClients(true);
                    }, 3000);
                }
                this._changeDetectorRef.markForCheck();
            },
            (err) => {
                this.showFlashMessage('error');
            }
        );
    }

    onStatusChange(): void {
        const item = this.selectedItemForm.getRawValue();
        this._clientService.delete(item).subscribe(
            (res: any) => {
                if (res.error) {
                    const msg = res.idError + '-' + res.msgError;
                    this.showMessage('warn', msg);
                } else {
                    this.showMessage('success', res.message);
                    this.loadClients(false);
                }
                this._changeDetectorRef.markForCheck();
            },
            (err) => {
                const msg = JSON.stringify(err);
                this.showMessage('warn', msg);
            }
        );
    }

    /**
     * clean format associate client
     */
    cleanClientAssociate(): void {
        this.showFormUserAssociate = false;
        if(this.rowList.length > 0){
            if(this.rowList[0].id === 0){
                this.rowList.splice(0,1);
                this._changeDetectorRef.markForCheck();
            }
        }
        this.userAssociate = {
            list: [],
            errorMessage: ''
        };
    }


    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'created' | 'error', msg: string = ''): void {
        // Show the message
        this.flashMessage = type;
        this.flashMessageMsg = msg;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {
            this.flashMessage = null;
            this.flashMessageMsg = '';
            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 3000);
    }

    changeCellphone(event: Event): void{
        if((event.target as HTMLInputElement).value.length > 14){
            const str = ((event.target as HTMLInputElement).value).slice(0, -1);
            this.selectedItemForm.get('cellphone').patchValue(str);
            this._changeDetectorRef.markForCheck();
        }
    }

    changeHomephone(event: Event): void{
        if((event.target as HTMLInputElement).value.length > 14){
            const str = ((event.target as HTMLInputElement).value).slice(0, -1);
            this.selectedItemForm.get('homePhone').patchValue(str);
            this._changeDetectorRef.markForCheck();
        }
    }

    changeSSN(event: Event): void{
        if((event.target as HTMLInputElement).value.length > 11){
            const str = ((event.target as HTMLInputElement).value).slice(0, -1);
            this.selectedItemForm.get('ssnItin').patchValue(str);
            this._changeDetectorRef.markForCheck();
        }
    }

    search(): void{
        const item = {
            searchType: this.optionFilter,
            searchParam: this.strFilter
        };
        this._clientService.search(item).subscribe(
            (res: any) => {
                if (res.error) {
                    this.showFlashMessage('error', 'Error '+res.idError+'!. '+res.msgError);
                    this.rowList = [];
                } else {
                    console.log(res);
                    this.rowListAll = res.clientList;
                    const objResult: any = this.calculatePagination(res.clientList);
                    this.rowList = objResult.list;
                    this.pagination = objResult.pagination;
                }
                this._changeDetectorRef.markForCheck();
            },
            (err) => {
                this.showFlashMessage('error', 'Error! '+ JSON.stringify(err));
            }
        );
    }

    cleanFilters(): void {
        this.optionFilter = 0;
        this.strFilter = '';
        this.loadClients(true);
    }

    showMessage(type: FuseAlertType, message: string, _timer: number = 3500): void {
        setTimeout(() => {
            this.showAlert = false;
            this._changeDetectorRef.markForCheck();
        }, _timer);
        this.alert = {
            type   : type,
            message: message
        };
        this.showAlert = true;
    }

}
