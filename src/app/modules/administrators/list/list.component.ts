import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Subject, takeUntil } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { TablePagination } from 'app/entities/InterfacePagination.interface';
import { cloneDeep } from 'lodash-es';

import { AdministratorEntity } from 'app/entities/administrator.entity';
import { AdministratorsService } from '../../../services/administrators.service';
import { FuseAlertType } from '@fuse/components/alert';
import { TranslocoService } from '@ngneat/transloco';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { User } from 'app/core/user/user.entity';
import { UserService } from 'app/core/user/user.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation  : ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations     : fuseAnimations
})
export class ListComponent implements OnInit {
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    rowListAll: AdministratorEntity[] = [];
    rowList: AdministratorEntity[] = [];
    selectedAdmin: AdministratorEntity | null = null;
    selectedAdminForm: FormGroup;
    isLoading: boolean = false;
    // Load the gray layer
    screenLoading: boolean = false;
    pagination: TablePagination;
    flashMessage: 'success' | 'error' | null = null;
    private unsubscribeAll: Subject<any> = new Subject<any>();
    // eslint-disable-next-line @typescript-eslint/member-ordering
    searchInputControl: FormControl = new FormControl();
    // eslint-disable-next-line @typescript-eslint/member-ordering
    proccessNewAdmin: boolean = false;
    // eslint-disable-next-line @typescript-eslint/member-ordering
    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    // eslint-disable-next-line @typescript-eslint/member-ordering
    showAlert: boolean = false;

    // eslint-disable-next-line @typescript-eslint/member-ordering
    user: User;

    /**
     * Constructor
     */
    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private adminService: AdministratorsService,
        private formBuilder: FormBuilder,
        private translocoService: TranslocoService,
        private _userService: UserService
    )
    {
    }

    inicializeTable(): void {
        if ( this._sort && this._paginator )
        {
             // Set the initial sort
             this._sort.sort({
                 id          : 'userId',
                 start       : 'asc',
                 disableClear: true
             });

             // Mark for check
             this.changeDetectorRef.markForCheck();

             // If the user changes the sort order...
             this._sort.sortChange
                 .pipe(takeUntil(this.unsubscribeAll))
                 .subscribe(() => {
                     // Reset back to the first page
                     this._paginator.pageIndex = 0;

                     // Close the details
                     this.closeDetails();
                 });

             // Get admins if sort or page changes
             merge(this._sort.sortChange, this._paginator.page).subscribe(
                 () => {
                     this.closeDetails();
                     this.isLoading = true;
                     // eslint-disable-next-line max-len
                     let objResult: any;
                     if(this._sort && this._paginator){
                        objResult  = this.calculatePagination(this.rowListAll ,null, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize );
                     }else {
                        objResult  = this.calculatePagination(this.rowListAll);
                     }
                     this.rowList            = objResult.list;
                     this.pagination         = objResult.pagination;
                     this.isLoading = false;
                     this.changeDetectorRef.markForCheck();
                 }
             );
        }
    }

    ngOnInit(): void {
        //Logged user load
        this.user = this._userService.userLogued;

        //load list administrator
        this.loadAdmin(true);

        // Create the selected administrator form
        this.selectedAdminForm = this.formBuilder.group({
            userId          : [''],
            name            : ['', [Validators.required]],
            email           : ['', [Validators.required, Validators.email]],
            status          : [true]
        });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges
        .subscribe(
            (query) => {
                this.closeDetails();
                this.isLoading = true;
                let objResult: any;
                if(this._sort && this._paginator){
                    objResult  = this.calculatePagination(this.rowListAll ,query, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize );
                }else {
                    objResult  = this.calculatePagination(this.rowListAll, query);
                    setTimeout(() => {
                        this.inicializeTable();
                    }, 100);
                }
                this.rowList            = objResult.list;
                this.pagination         = objResult.pagination;
                this.isLoading = false;
                this.changeDetectorRef.markForCheck();
            }
        );
    }

    /**
     * Load Administrator
     */
    loadAdmin(inicializeTable: boolean = false): void {
        //Ability button add
        this.proccessNewAdmin = false;
        // Show load screen
        this.screenLoading = true;
        this.adminService.getAdministrators().subscribe(
                (res: any) => {
                    if(res.error){
                         // Set the alert
                        this.showMessage('warn', 'Error '+res.idError+'!. '+res.msgError);
                        this.rowList = [];
                    }else{
                        this.rowListAll         = res.userList;
                        const objResult: any    = this.calculatePagination(res.userList);
                        this.rowList            = objResult.list;
                        this.pagination         = objResult.pagination;
                    }
                    if(inicializeTable){
                        setTimeout(() => {
                            this.inicializeTable();
                        }, 500);
                    }
                    this.changeDetectorRef.markForCheck();
                    // Close load screen when list admin are loaded
                    this.screenLoading = false;
                },
                (err) => {
                    console.log(err);
                     // Set the alert
                    this.showMessage('warn', 'Error! '+ JSON.stringify(err));
                    // Close load screen
                    this.screenLoading = false;
                }
        );
    }

    /**
     * Calculate pagination by table
     */
    calculatePagination(list: AdministratorEntity[], search: string = '', sort: any = 'userId', order: string = 'asc', page: number = 0, size: number = 10 ): any {

        // // Clone the admins
        let adminListClone: AdministratorEntity[] = cloneDeep(list);

        // Sort the adminListClone
        if ( sort === 'userId' || sort === 'name' || sort === 'status' )
        {
            adminListClone.sort((a, b) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldA: any;
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldB: any;
                if(sort === 'userId'){
                    fieldA = parseInt(a[sort], 10);
                    fieldB = parseInt(b[sort], 10);
                    // eslint-disable-next-line max-len
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB, undefined, {numeric:'true'}) : fieldB.toString().localeCompare(fieldA, undefined, {numeric:'true'});
                }else{
                    fieldA = a[sort].toString().toUpperCase();
                    fieldB = b[sort].toString().toUpperCase();
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB) : fieldB.toString().localeCompare(fieldA);
                }
            });
        }
        else
        {
            adminListClone.sort((a, b) => order === 'asc' ? a[sort] - b[sort] : b[sort] - a[sort]);
        }

        // If search exists...
        if ( search )
        {
            // Filter the adminListClone
            adminListClone = adminListClone.filter(contact => contact.name && contact.name.toLowerCase().includes(search.toLowerCase()));
        }

        // Paginate - Start
        const adminListCloneLength = adminListClone.length;

        // Calculate pagination details
        const begin = page * size;
        const end = Math.min((size * (page + 1)), adminListCloneLength);
        const lastPage = Math.max(Math.ceil(adminListCloneLength / size), 1);

        // Prepare the pagination object
        let pagination = {};

        // If the requested page number is bigger than
        // the last possible page number, return null for
        // adminListClone but also send the last possible page so
        // the app can navigate to there
        if ( page > lastPage )
        {
            adminListClone = null;
            pagination = {
                lastPage
            };
        }
        else
        {
            // Paginate the results by size
            adminListClone = adminListClone.slice(begin, end);

            // Prepare the pagination mock-api
            pagination = {
                length    : adminListCloneLength,
                size      : size,
                page      : page,
                lastPage  : lastPage,
                startIndex: begin,
                endIndex  : end - 1
            };
        }
        return {
                list: adminListClone,
                pagination: pagination
        };
    }

    /*
    * New row by new administrator
    */
    newAdmin(): void {
        this.proccessNewAdmin = true;

        const newAdminEnty: AdministratorEntity = {
            status: true,
            email: '',
            name: ''
        };

        const elementScroll: any = document.getElementsByClassName('elementScroll');

        //Add row
        this.rowList.unshift(newAdminEnty);

        // Go to new administrator
        this.selectedAdmin = newAdminEnty;

        // Fill the form
        this.selectedAdminForm.patchValue(newAdminEnty);

        // Mark for check
        this.changeDetectorRef.markForCheck();

        // Scroll to top
        elementScroll[0].scrollTop = 0;
    }

    cancelnewAdmin(): void {
        if(!this.rowList[0].userId){
            this.rowList.splice(0, 1);
        }
        this.proccessNewAdmin = false;
    }

    /*
    * Create administrator
    */
     saveAdministrator(): void
     {
        if (this.selectedAdminForm.valid) {
            // Get the admin object
           const admin: AdministratorEntity = this.selectedAdminForm.getRawValue();
           this.isLoading = true;
            // Create the admin
            this.adminService.createAdministrator(admin).subscribe((res: any) => {
                if(res.error){
                    // Alert error
                    if(res.message && res.message !== ''){
                        this.showMessage('warning', 'Error '+res.idError+'!. '+res.message);
                        //Cargar administradores
                        this.loadAdmin();
                    }else{
                        this.showMessage('warn', 'Error '+res.idError+'!. '+res.msgError);
                    }
                }else{
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Cargar administradores
                    this.loadAdmin();
                }
                this.isLoading = false;
                this.changeDetectorRef.markForCheck();
            });
        } else {
            //set text second by language
            this.translocoService.selectTranslate('forms.errorData').pipe()
            .subscribe((textErrorForm) => {
                    this.showMessage('warn', textErrorForm);
            });
        }
     }

    /**
     * Update the selected admin using the form data
     */
    updateSelectedAdmin(): void
    {

        if (this.selectedAdminForm.dirty && this.selectedAdminForm.valid) {
            // Get the admin object
            const admin: AdministratorEntity = this.selectedAdminForm.getRawValue();
            this.isLoading = true;
            // Show load screen
            this.screenLoading = true;
            // Update the admin on the server
            this.adminService.updateAdministrator(admin).subscribe((res: any) => {
                if(res.error){
                    // Alert error
                    this.showMessage('warn', 'Error '+res.idError+'!. '+res.msgError);
                    // this.showFlashMessage('error');
                }else{
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Update Lists general
                    this.reloadGeneralLists(admin);
                    //Reload list commented
                    // const objResult: any    = this.calculatePagination(this.rowListAll);
                    // this.rowList            = objResult.list;
                    // this.pagination         = objResult.pagination;
                }
                this.isLoading = false;
                this.changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                console.log(err);
                 // Set the alert
                this.showMessage('warn', 'Error! '+ JSON.stringify(err));
                // Close load screen
                this.screenLoading = false;
            });
        } else {
            //set text second by language
            const messageError = (!this.selectedAdminForm.dirty) ? 'forms.errorDirty' : 'forms.errorData';
            this.translocoService.selectTranslate(messageError).pipe()
            .subscribe((textErrorForm) => {
                    this.showMessage('warn', textErrorForm);
            });
        }
    }

    /**
     * Update status of selected admin using the form data
     */
     updateStatusSelectedAdmin(event: MatSlideToggleChange): void
     {

        console.log('toggle', event.checked);
         if (this.selectedAdminForm.dirty && this.selectedAdminForm.valid) {
             // Get the admin object
            const admin: AdministratorEntity = this.selectedAdminForm.getRawValue();
            this.isLoading = true;
             // Update status admin on the server
             this.adminService.updateStatusAdministrator(admin).subscribe((res: any) => {
                 if(res.error){
                     //Return value
                     this.selectedAdminForm.controls['status'].setValue(!event.checked);
                     // Alert error
                     this.showMessage('warn', 'Error '+res.idError+'!. '+res.msgError);
                     // this.showFlashMessage('error');
                 }else{
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Update Lists general
                    this.reloadGeneralLists(admin);
                    //Reload list commented
                    //  const objResult: any    = this.calculatePagination(this.rowListAll);
                    //  this.rowList            = objResult.list;
                    //  this.pagination         = objResult.pagination;
                 }
                 this.isLoading = false;
                 this.changeDetectorRef.markForCheck();
             });
         } else {
             //set text second by language
             const messageError = (!this.selectedAdminForm.dirty) ? 'forms.errorDirty' : 'forms.errorData';
             this.translocoService.selectTranslate(messageError).pipe()
             .subscribe((textErrorForm) => {
                     this.showMessage('warn', textErrorForm);
             });
         }
    }

    /**
     * Function to reload changes in general lists
     */
    reloadGeneralLists(admin: AdministratorEntity): void {
        //Reload List selected
        const indexAdmListSelected: number = this.rowList.findIndex(item => item.userId && item.userId === admin.userId);
        if(indexAdmListSelected>=0){
            this.rowList[indexAdmListSelected] = admin;
        }

        //Reload General List
        const indexAdmListGeneral: number = this.rowListAll.findIndex(item => item.userId && item.userId === admin.userId);
        if(indexAdmListGeneral>=0){
            this.rowListAll[indexAdmListGeneral] = admin;
        }
    }


    /*** Toggle admin details
      * @param administratorEntity
      */
    toggleDetails(admin: AdministratorEntity): void {

        //Cancel new admin
        if(this.proccessNewAdmin && admin?.userId){
            this.cancelnewAdmin();
        }

        // If the admin is already selected...
        if (this.selectedAdmin && this.selectedAdmin.userId === admin.userId) {
            // Close the details
            this.closeDetails();
            return;
        }

        // Go to load admin
        this.selectedAdmin = admin;

        // Fill the form
        this.selectedAdminForm.reset();
        this.selectedAdminForm.patchValue(admin);

        // Mark for check
        this.changeDetectorRef.markForCheck();
    }

    /**
     * Close the details
     */
    closeDetails(): void {
        this.selectedAdmin = null;
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this.changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            this.changeDetectorRef.markForCheck();
        }, 3000);
    }

    showMessage(type: FuseAlertType, message: string, _timer: number = 3500): void {
        setTimeout(() => {
            this.showAlert = false;
            this.changeDetectorRef.markForCheck();
        }, _timer);
        this.alert = {
            type   : type,
            message: message
        };
        this.showAlert = true;
    }
}
