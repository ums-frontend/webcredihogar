import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'app/shared/shared.module';

import { administratorsRouting } from './administrators-routing.module';
import { AdministratorsComponent } from './administrators.component';
import { ListComponent } from './list/list.component';
import { TranslocoModule } from '@ngneat/transloco';
import { FuseAlertModule } from '@fuse/components/alert';



@NgModule({
  declarations: [
    AdministratorsComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(administratorsRouting),
    TranslocoModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatSortModule,
    MatSelectModule,
    MatSlideToggleModule,
    SharedModule,
    FuseAlertModule
  ]
})
export class AdministratorsModule { }
