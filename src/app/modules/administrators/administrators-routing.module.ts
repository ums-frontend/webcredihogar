import { Route } from '@angular/router';
import { AuthGuardService } from 'app/services/auth-guard.service';
import { AdministratorsComponent } from './administrators.component';
import { ListComponent } from './list/list.component';

export const administratorsRouting: Route[] = [
    {
        path     : '',
        component: AdministratorsComponent,
        canActivate:[AuthGuardService],
        children : [
            {
                path     : '',
                component: ListComponent,
                canActivate:[AuthGuardService]
            }
        ]
    }
];
