import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, Subject, takeUntil } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { TablePagination } from 'app/entities/InterfacePagination.interface';
import { DealershipEntity } from 'app/entities/dealership.entity';
import { SettingsEntity } from 'app/entities/settings.entity';
import { DealershipService } from 'app/services/dealership.service';
import { cloneDeep } from 'lodash-es';
import { FuseAlertType } from '@fuse/components/alert';
import { TranslocoService } from '@ngneat/transloco';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';


@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: fuseAnimations
})
export class ListComponent implements OnInit {
    @ViewChild(MatPaginator) private _paginator: MatPaginator;
    @ViewChild(MatSort) private _sort: MatSort;

    rowListAll: DealershipEntity[] = [];
    rowList: DealershipEntity[] = [];
    selectedDealership: DealershipEntity | null = null;
    selectedDealershipForm: FormGroup;
    isLoading: boolean = false;
    // Load the gray layer
    screenLoading: boolean = false;
    pagination: TablePagination;
    flashMessage: 'success' | 'error' | null = null;
    private unsubscribeAll: Subject<any> = new Subject<any>();
    // eslint-disable-next-line @typescript-eslint/member-ordering
    searchInputControl: FormControl = new FormControl();
    // eslint-disable-next-line @typescript-eslint/member-ordering
    listSettings: SettingsEntity[] = [];
    // eslint-disable-next-line @typescript-eslint/member-ordering
    proccessNewDealership: boolean = false;
    // eslint-disable-next-line @typescript-eslint/member-ordering
    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };
    // eslint-disable-next-line @typescript-eslint/member-ordering
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private dealershipService: DealershipService,
        private formBuilder: FormBuilder,
        private translocoService: TranslocoService
    ) { }

    inicializeTable(): void {
        if (this._sort && this._paginator) {
            // Set the initial sort
            this._sort.sort({
                id: 'userId',
                start: 'asc',
                disableClear: true
            });

            // Mark for check
            this.changeDetectorRef.markForCheck();

            // If the user changes the sort order...
            this._sort.sortChange
                .pipe(takeUntil(this.unsubscribeAll))
                .subscribe(() => {
                    // Reset back to the first page
                    this._paginator.pageIndex = 0;

                    // Close the details
                    this.closeDetails();
                });

            // Get dealerships if sort or page changes
            merge(this._sort.sortChange, this._paginator.page).subscribe(
                () => {
                    this.closeDetails();
                    this.isLoading = true;
                    // eslint-disable-next-line max-len
                    let objResult: any;
                    if (this._sort && this._paginator) {
                        objResult = this.calculatePagination(this.rowListAll, null, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize);
                    } else {
                        objResult = this.calculatePagination(this.rowListAll);
                    }
                    this.rowList = objResult.list;
                    this.pagination = objResult.pagination;
                    this.isLoading = false;
                    this.changeDetectorRef.markForCheck();
                }
            );
        }
    }

    ngOnInit(): void {

        //load list dealership
        this.loadDealership(true);

        //Load list settings
        this.loadSettings();

        // Create the selected dealership form
        this.selectedDealershipForm = this.formBuilder.group({
            userId              : [''],
            name                : ['', [Validators.required]],
            email               : ['', [Validators.required]],
            status              : [true],
            address             : ['', [Validators.required]],
            configId            : ['', [Validators.required]],
            acronym             : ['', [Validators.required, Validators.pattern('[A-Za-z0-9_.]{3}')]],
            cellphone           : [''],
            notes               : ['']
        });

        // Subscribe to search input field value changes
        this.searchInputControl.valueChanges.subscribe((query) => {
            this.closeDetails();
            this.isLoading = true;
            let objResult: any;
            if (this._sort && this._paginator) {
                objResult = this.calculatePagination(this.rowListAll, query, this._sort.active, this._sort.direction, this._paginator.pageIndex, this._paginator.pageSize);
            } else {
                objResult = this.calculatePagination(this.rowListAll, query);
                setTimeout(() => {
                    this.inicializeTable();
                }, 100);
            }
            this.rowList = objResult.list;
            this.pagination = objResult.pagination;
            this.isLoading = false;
            this.changeDetectorRef.markForCheck();
        });
    }

    /**
     * Load Dealership
     */
    loadDealership(inicializeTable: boolean = false): void {
        //Ability button add
        this.proccessNewDealership = false;
        this.screenLoading = true;
        this.dealershipService.getDealership().subscribe(
            (res: any) => {
                if (res.error) {
                    // Set the alert
                    this.showMessage('warn', 'Error ' + res.idError + '!. ' + res.msgError);
                    this.rowList = [];
                } else {
                    this.rowListAll = res.userList;
                    const objResult: any = this.calculatePagination(res.userList);
                    this.rowList = objResult.list;
                    this.pagination = objResult.pagination;
                }
                if (inicializeTable) {
                    setTimeout(() => {
                        this.inicializeTable();
                    }, 500);
                }
                this.changeDetectorRef.markForCheck();
                this.screenLoading = false;
            },
            (err) => {
                console.log(err);
                // Set the alert
                this.showMessage('warn', 'Error! ' + JSON.stringify(err));
                this.screenLoading = false;
            }
        );
    }

    /**
     * Calculate pagination by table
     */
    calculatePagination(list: DealershipEntity[], search: string = '', sort: any = 'userId', order: string = 'asc', page: number = 0, size: number = 10): any {

        // // Clone the dealerships
        let dealershipList: DealershipEntity[] = cloneDeep(list);

        // Sort the dealershipList
        if (sort === 'userId' || sort === 'name' || sort === 'address') {
            dealershipList.sort((a, b) => {
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldA: any;
                // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                let fieldB: any;
                if (sort === 'userId') {
                    fieldA = parseInt(a[sort], 10);
                    fieldB = parseInt(b[sort], 10);
                    // eslint-disable-next-line max-len
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB, undefined, { numeric: 'true' }) : fieldB.toString().localeCompare(fieldA, undefined, { numeric: 'true' });
                } else {
                    fieldA = a[sort].toString().toUpperCase();
                    fieldB = b[sort].toString().toUpperCase();
                    return order === 'asc' ? fieldA.toString().localeCompare(fieldB) : fieldB.toString().localeCompare(fieldA);
                }
            });
        }
        else {
            dealershipList.sort((a, b) => order === 'asc' ? a[sort] - b[sort] : b[sort] - a[sort]);
        }

        // If search exists...
        if (search) {
            // Filter the dealershipList
            dealershipList = dealershipList.filter(contact => contact.name && contact.name.toLowerCase().includes(search.toLowerCase()));
        }

        // Paginate - Start
        const dealershipListLength = dealershipList.length;

        // Calculate pagination details
        const begin = page * size;
        const end = Math.min((size * (page + 1)), dealershipListLength);
        const lastPage = Math.max(Math.ceil(dealershipListLength / size), 1);

        // Prepare the pagination object
        let pagination = {};

        // If the requested page number is bigger than
        // the last possible page number, return null for
        // dealershipList but also send the last possible page so
        // the app can navigate to there
        if (page > lastPage) {
            dealershipList = null;
            pagination = {
                lastPage
            };
        }
        else {
            // Paginate the results by size
            dealershipList = dealershipList.slice(begin, end);

            // Prepare the pagination mock-api
            pagination = {
                length: dealershipListLength,
                size: size,
                page: page,
                lastPage: lastPage,
                startIndex: begin,
                endIndex: end - 1
            };
        }
        return {
            list: dealershipList,
            pagination: pagination
        };
    }

    /**
     * Load settings by dropbox
     */
    loadSettings(): any {
        this.dealershipService.getSettings().subscribe((res: any) => {
            if (res.error) {
                // Set the alert
                this.showMessage('warn', 'Error ' + res.idError + '!. ' + res.msgError);
                this.listSettings = [];
            } else {
                this.listSettings = res.catalog;
            }
            this.changeDetectorRef.markForCheck();
        }, (err) => {
            // Set the alert
            this.showMessage('warn', 'Error! ' + JSON.stringify(err));
        });
    }

    /*
    * New row by new dealership
    */
    newDealership(): void {
        this.proccessNewDealership = true;

        const newDealershipEnty: DealershipEntity = {
            address: '',
            configId: undefined,
            email: '',
            name: '',
            acronym: '',
            cellphone: '',
            notes: ''
        };

        const elementScroll: any = document.getElementsByClassName('elementScroll');

        //Add row
        this.rowList.unshift(newDealershipEnty);

        // Go to new dealership
        this.selectedDealership = newDealershipEnty;

        // Fill the form
        this.selectedDealershipForm.patchValue(newDealershipEnty);

        // Mark for check
        this.changeDetectorRef.markForCheck();

        // Scroll to top
        elementScroll[0].scrollTop = 0;
    }

    cancelNewDealership(): void {
        if (!this.rowList[0].userId) {
            this.rowList.splice(0, 1);
        }
        this.proccessNewDealership = false;
    }

    /*
    * Create dealership
    */
    saveDealership(): void {
        if (this.selectedDealershipForm.valid) {
            // Get the dealership object
            // eslint-disable-next-line @typescript-eslint/no-shadow
            const dealership: DealershipEntity = this.selectedDealershipForm.getRawValue();
            this.isLoading = true;
            // Create the dealer
            this.dealershipService.createDealership(dealership).subscribe((res: any) => {
                if (res.error) {
                    // Alert error
                    if (res.message && res.message !== '') {
                        this.showMessage('warning', 'Error ' + res.idError + '!. ' + res.message);
                        //Cargar dealeristradores
                        this.loadDealership();
                    } else {
                        this.showMessage('warn', 'Error ' + res.idError + '!. ' + res.msgError);
                    }
                } else {
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Cargar administradores
                    this.loadDealership();
                }
                this.isLoading = false;
                this.changeDetectorRef.markForCheck();
            });
        } else {
            //set text second by language
            this.translocoService.selectTranslate('forms.errorData').pipe()
                .subscribe((textErrorForm) => {
                    this.showMessage('warn', textErrorForm);
                });
        }
    }

    /**
     * Update the selected dealership using the form data
     */
    updateSelectedDealership(): void {
        if (this.selectedDealershipForm.dirty && this.selectedDealershipForm.valid) {
            // Get the dealership object
            // eslint-disable-next-line @typescript-eslint/no-shadow
            const dealership: DealershipEntity = this.selectedDealershipForm.getRawValue();
            this.isLoading = true;
            this.screenLoading = true;
            // Update the dealer on the server
            this.dealershipService.updateDealership(dealership).subscribe((res: any) => {
                if (res.error) {
                    // Alert error
                    this.showMessage('warn', 'Error ' + res.idError + '!. ' + res.msgError);
                } else {
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Update Lists general
                    this.reloadGeneralLists(dealership);
                }
                this.isLoading = false;
                this.screenLoading = false;
                this.changeDetectorRef.markForCheck();
            },
            (err) => {
                console.log(err);
                // Set the alert
                this.showMessage('warn', 'Error! ' + JSON.stringify(err));
                this.screenLoading = false;
            });

        } else {
            //set text second by language
            const messageError = (!this.selectedDealershipForm.dirty) ? 'forms.errorDirty' : 'forms.errorData';
            this.translocoService.selectTranslate(messageError).pipe()
                .subscribe((textErrorForm) => {
                    this.showMessage('warn', textErrorForm);
                });
        }
    }

    /**
     * Update status of selected Dealership using the form data
     */
    updateStatusSelectedDealership(event: MatSlideToggleChange): void {

        console.log('toggle', event.checked);
        if (this.selectedDealershipForm.dirty && this.selectedDealershipForm.valid) {
            // Get the admin object
            const dealership: DealershipEntity = this.selectedDealershipForm.getRawValue();
            this.isLoading = true;
            // Update status admin on the server
            this.dealershipService.updateStatusDealership(dealership).subscribe((res: any) => {
                if (res.error) {
                    //Return value
                    this.selectedDealershipForm.controls['status'].setValue(!event.checked);
                    // Alert error
                    this.showMessage('warn', 'Error ' + res.idError + '!. ' + res.msgError);
                } else {
                    //Success new admin
                    this.showMessage('success', res.message);
                    //Update Lists general
                    this.reloadGeneralLists(dealership);
                }
                this.isLoading = false;
                this.changeDetectorRef.markForCheck();
            });
        } else {
            //set text second by language
            const messageError = (!this.selectedDealershipForm.dirty) ? 'forms.errorDirty' : 'forms.errorData';
            this.translocoService.selectTranslate(messageError).pipe()
                .subscribe((textErrorForm) => {
                    this.showMessage('warn', textErrorForm);
                });
        }
    }

    /**
     * Function to reload changes in general lists
     */
    reloadGeneralLists(dealership: DealershipEntity): void {
        //Reload List selected
        const indexDealershipListSelected: number = this.rowList.findIndex(item => item.userId && item.userId === dealership.userId);
        if (indexDealershipListSelected >= 0) {
            this.rowList[indexDealershipListSelected] = dealership;
        }

        //Reload General List
        const indexDeralershipListGeneral: number = this.rowListAll.findIndex(item => item.userId && item.userId === dealership.userId);
        if (indexDeralershipListGeneral >= 0) {
            this.rowListAll[indexDeralershipListGeneral] = dealership;
        }
    }


    /*** Toggle dealership details
      * @param dealershipId
      */
    toggleDetails(dealership: DealershipEntity): void {

        //Cancel new dealership
        if (this.proccessNewDealership && dealership?.userId) {
            this.cancelNewDealership();
        }

        // If the dealership is already selected...
        if (this.selectedDealership && this.selectedDealership.userId === dealership.userId) {
            // Close the details
            this.closeDetails();
            return;
        }

        // Go to load dealership
        this.selectedDealership = dealership;

        // Fill the form
        this.selectedDealershipForm.reset();
        this.selectedDealershipForm.patchValue(dealership);

        // Mark for check
        this.changeDetectorRef.markForCheck();
    }

    /**
     * Close the details
     */
    closeDetails(): void {
        this.selectedDealership = null;
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this.changeDetectorRef.markForCheck();

        // Hide it after 3 seconds
        setTimeout(() => {

            this.flashMessage = null;

            // Mark for check
            this.changeDetectorRef.markForCheck();
        }, 3000);
    }

    showMessage(type: FuseAlertType, message: string, _timer: number = 3500): void {
        setTimeout(() => {
            this.showAlert = false;
            this.changeDetectorRef.markForCheck();
        }, _timer);
        this.alert = {
            type: type,
            message: message
        };
        this.showAlert = true;
    }
}
