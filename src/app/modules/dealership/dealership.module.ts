import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { dealershipRoutingModule } from './dealership-routing.module';
import { DealershipComponent } from './dealership.component';
import { ListComponent } from './list/list.component';
import { TranslocoModule } from '@ngneat/transloco';

import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from 'app/shared/shared.module';
import { FuseAlertModule } from '@fuse/components/alert';
import { NgxMaskModule, IConfig } from 'ngx-mask';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    DealershipComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(dealershipRoutingModule),
    TranslocoModule,
    MatButtonModule,
    // MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    // MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    // MatRippleModule,
    MatSortModule,
    MatSelectModule,
    MatSlideToggleModule,
    // MatTooltipModule,
    SharedModule,
    FuseAlertModule,
    //Mask
    NgxMaskModule.forRoot()
  ]
})
export class DealershipModule { }
