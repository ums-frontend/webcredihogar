import { Route } from '@angular/router';
import { AuthGuardService } from 'app/services/auth-guard.service';
import { DealershipComponent } from './dealership.component';
import { ListComponent } from './list/list.component';

export const dealershipRoutingModule: Route[] = [
    {
        path     : '',
        component: DealershipComponent,
        canActivate:[AuthGuardService],
        children : [
            {
                path     : '',
                component: ListComponent,
                canActivate:[AuthGuardService]
            }
        ]
    }
];
