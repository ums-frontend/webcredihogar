import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, of, Subject, switchMap, takeUntil, throwError } from 'rxjs';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { UserService } from 'app/core/user/user.service';

import { environment } from 'environments/environment';
import { ResponseSessionsEntity, ResponseChangePasswordEntity } from 'app/entities/responseSessions';
import { TranslocoService } from '@ngneat/transloco';
import { User } from '../user/user.entity';

const END_POINT = `${environment.SERVICES_URL}`;

@Injectable()
export class AuthService
{
    private _authenticated: boolean = false;
    private readonly secretKey: string = `${environment.keyToken}`;


    /**
     * Constructor
     */
    constructor(
        private http: HttpClient,
        private _userService: UserService,
        private _translocoService: TranslocoService,
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for access token
     */
    set accessToken(user: any)
    {
        localStorage.setItem('accessToken', AuthUtils.generateToken(user));
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    get accessToken(): string
    {
        return localStorage.getItem('accessToken') ?? '';
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Forgot password
     *
     * @param email
     */
    forgotPassword(email: string): Observable<any>
    {
        const loginRequest: any = {
            'email': email,
            'language': this._translocoService.getActiveLang().toUpperCase()
        };
        const body = JSON.stringify(loginRequest);
        return this.http.post<ResponseChangePasswordEntity>(`${END_POINT}/recoverPassword`, body);
    }

    /**
     * Reset password
     *
     * @param password
     */
    resetPassword(password: string, idUser: number): Observable<ResponseChangePasswordEntity>
    {

        const loginRequest: any = {
            'userId': idUser,
            'pass': password,
            'language': this._translocoService.getActiveLang().toUpperCase()
        };
        const body = JSON.stringify(loginRequest);
        return this.http.post<ResponseChangePasswordEntity>(`${END_POINT}/changePassword`, body);
    }

    /**
     * Sign in
     *
     * @param credentials
     */
    signIn(credentials: { email: string; password: string }): Observable<any>
    {
        // Throw error, if the user is already logged in
        if ( this._authenticated )
        {
            return throwError('User is already logged in.');
        }

        const loginRequest: any = {
            'user': credentials.email,
            'pass': credentials.password,
            'language': this._translocoService.getActiveLang().toUpperCase()
        };
        const body = JSON.stringify(loginRequest);
        return this.http.post<ResponseSessionsEntity>(`${END_POINT}/login`, body).pipe(
                switchMap((resp: ResponseSessionsEntity) => {
                    this._userService.user = null;
                    if(!resp.error){
                        // Set the authenticated flag to true
                        if(resp.user?.reset === true){
                            this._authenticated = false;
                        }else{
                            this._authenticated = true;
                            this.accessToken = resp.user;
                        }
                        // Store the user on the user service
                        this._userService.user = resp.user;
                        console.log(resp.user);
                    }else{
                        // Set the authenticated flag to true
                        this._authenticated = false;
                        this._userService.user = null;
                    }

                    // Return a new observable with the response
                    return of(resp);
                }
        ));
    }

    /**
     * Sign in using the access token
     */
    signInUsingToken(): Observable<any>
    {

        const user: User = AuthUtils.getUserByToken(localStorage.getItem('accessToken') ?? '');
        this._authenticated = true;
        this.accessToken = user;
        // Store the user on the user service
        this._userService.user = user;
        return of(true);
    }

    /**
     * Sign out
     */
    signOut(): Observable<any>
    {
        // Remove the access token from the local storage
        localStorage.removeItem('accessToken');

        // Set the authenticated flag to false
        this._authenticated = false;

        // Return the observable
        return of(true);
    }

    /**
     * Sign up
     *
     * @param user
     */
    signUp(): any
    {
    }

    /**
     * Unlock session
     *
     * @param credentials
     */
    unlockSession(): any
    {
    }

    /**
     * Check the authentication status
     */
    check(): Observable<boolean>
    {
        // Check if the user is logged in
        if ( this._authenticated )
        {
            return of(true);
        }

        // Check the access token availability
        if ( !this.accessToken )
        {
            return of(false);
        }

        // Check the access token expire date
        if ( AuthUtils.isTokenExpired(this.accessToken) )
        {
            return of(false);
        }

        // return of(true);
        // If the access token exists and it didn't expire, sign in using it
        return this.signInUsingToken();
    }
}
