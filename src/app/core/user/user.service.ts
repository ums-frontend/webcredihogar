import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable, of, ReplaySubject, tap } from 'rxjs';
import { User } from 'app/core/user/user.entity';
import { AuthUtils } from 'app/core/auth/auth.utils';


@Injectable({
    providedIn: 'root'
})
export class UserService
{
    private _user: User;

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for user
     *
     * @param value
     */
    set user(value: User)
    {
        // Store the value
        this._user = value;
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    get user$(): Observable<User>
    {
        return of(this._user);
    }

    // eslint-disable-next-line @typescript-eslint/member-ordering
    get userLogued(): User
    {
        return this._user;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get the current logged in user data
     */
    get(): Observable<User>
    {
        // const userToken: string =
        const user: User = AuthUtils.getUserByToken(localStorage.getItem('accessToken') ?? '');

        return of(user);
    }
}
