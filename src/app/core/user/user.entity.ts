export interface User
{
    userId: number;
    name: string;
    email: string;
    address: string;
    reset?: boolean;
    roleId?: number;
    role?: string;
    managment?: boolean;
    minPersonalReferences?: number;
    minWorkReferences?: number;
    maxReferences?: number;
}

